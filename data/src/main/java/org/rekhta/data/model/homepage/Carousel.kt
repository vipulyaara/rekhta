package org.rekhta.data.model.homepage

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Carousel {

    @SerializedName("I")
    @Expose
    var i: String? = null
    @SerializedName("BI")
    @Expose
    var bi: String? = null
    @SerializedName("BE")
    @Expose
    var be: String? = null
    @SerializedName("BH")
    @Expose
    var bh: String? = null
    @SerializedName("BU")
    @Expose
    var bu: String? = null
    @SerializedName("LE")
    @Expose
    var le: Any? = null
    @SerializedName("LH")
    @Expose
    var lh: Any? = null
    @SerializedName("LU")
    @Expose
    var lu: Any? = null
    @SerializedName("BLE")
    @Expose
    var ble: String? = null
    @SerializedName("BLH")
    @Expose
    var blh: String? = null
    @SerializedName("BLU")
    @Expose
    var blu: String? = null
    @SerializedName("FD")
    @Expose
    var fd: Any? = null
    @SerializedName("TD")
    @Expose
    var td: Any? = null
    @SerializedName("TI")
    @Expose
    var ti: Any? = null
    @SerializedName("TU")
    @Expose
    var tu: Any? = null
    @SerializedName("CI")
    @Expose
    var ci: String? = null
    @SerializedName("IT")
    @Expose
    var it: Int? = null
    @SerializedName("S")
    @Expose
    var s: Int? = null
    @SerializedName("BT")
    @Expose
    var bt: Int? = null
    @SerializedName("TIN")
    @Expose
    var tin: Any? = null
    @SerializedName("MP")
    @Expose
    var mp: Boolean? = null
    @SerializedName("LB")
    @Expose
    var lb: Any? = null
    @SerializedName("LC")
    @Expose
    var lc: Any? = null
    @SerializedName("PNE")
    @Expose
    var pne: Any? = null
    @SerializedName("PNH")
    @Expose
    var pnh: Any? = null
    @SerializedName("PNU")
    @Expose
    var pnu: Any? = null

}
