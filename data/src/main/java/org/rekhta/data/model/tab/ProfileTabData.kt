package org.rekhta.data.model.tab

/**
 * Authored by vipulkumar on 14/12/17.
 */

enum class ProfileTabData(var value: String?, var contentTypeId: String?) {
    PROFILE("Profile", ""),
    GHAZAL("Ghazal", "43d60a15-0b49-4caf-8b74-0fcdddeb9f83"),
    NAZM("Nazm", "c54c4d8b-7e18-4f70-8312-e1c2cc028b0b"),
    SHER("Sher", "f722d5dc-45da-41ec-a439-900df702a3d6"),
//    AUDIO("Audio", "")
}
