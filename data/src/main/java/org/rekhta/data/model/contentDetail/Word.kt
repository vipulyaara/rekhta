package org.rekhta.data.model.contentDetail

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

import java.io.Serializable

class Word : Serializable {

    @SerializedName("M")
    @Expose
    var m: String? = null
    @SerializedName("W")
    @Expose
    var w: String? = null
    @SerializedName("S")
    @Expose
    var s: String? = null

}
