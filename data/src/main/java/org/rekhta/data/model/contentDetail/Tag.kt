package org.rekhta.data.model.contentDetail

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Authored by vipulkumar on 29/12/17.
 */

class Tag {
    @SerializedName("TN")
    @Expose
    var tn: String? = null
    @SerializedName("TS")
    @Expose
    var ts: String? = null
}
