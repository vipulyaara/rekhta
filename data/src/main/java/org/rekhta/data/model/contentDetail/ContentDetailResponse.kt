package org.rekhta.data.model.contentDetail

import androidx.annotation.NonNull
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.google.gson.Gson
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import org.rekhta.data.db.RekhtaTypeConverters

@Entity
class ContentDetailResponse {

    @SerializedName("NF")
    @Expose
    var nf: Boolean? = null
    @SerializedName("AS")
    @Expose
    var `as`: String? = null
    @SerializedName("ATS")
    @Expose
    var ats: String? = null
    @SerializedName("EC")
    @Expose
    var ec: Boolean? = null
    @SerializedName("PC")
    @Expose
    var pc: Boolean? = null
    @PrimaryKey
    @NonNull
    @SerializedName("I")
    @Expose
    var i: String = ""
    @SerializedName("SI")
    @Expose
    var si: Int? = null
    @SerializedName("CS")
    @Expose
    var cs: String? = null
    @SerializedName("CT")
    @Expose
    var ct: String? = null
    @SerializedName("ST")
    @Expose
    var st: String? = null
    @SerializedName("FT")
    @Expose
    var ft: String? = null
    @SerializedName("HN")
    @Expose
    var hn: Boolean? = null
    @SerializedName("HH")
    @Expose
    var hh: Boolean? = null
    @SerializedName("HU")
    @Expose
    var hu: Boolean? = null
    @SerializedName("HT")
    @Expose
    var ht: Boolean? = null
    @SerializedName("RF")
    @Expose
    var rf: Int? = null
    @SerializedName("RA")
    @Expose
    var ra: Int? = null
    @SerializedName("TS")
    @Expose
    var ts: String? = null
    @SerializedName("CR")
    @Expose
    var cr: String? = null
    @SerializedName("RFP")
    @Expose
    var rfp: String? = null
    @SerializedName("CTS")
    @Expose
    var cts: String? = null
    @SerializedName("TD")
    @Expose
    var td: String? = null
    @SerializedName("TN")
    @Expose
    var tn: String? = null
    @SerializedName("PT")
    @Expose
    var pt: String? = null
    @SerializedName("PS")
    @Expose
    var ps: String? = null
    @SerializedName("PTS")
    @Expose
    var pts: String? = null
    @SerializedName("Videos")
    @Expose
    @Ignore
    var videos: List<Video>? = null
    @SerializedName("Audios")
    @Expose
    @Ignore
    var audios: List<Audio>? = null
    @SerializedName("FPMappings")
    @Expose
    @Ignore
    var fpMappings: List<Any>? = null
    @SerializedName("FPParaMappings")
    @Expose
    @Ignore
    var fpParaMappings: List<Any>? = null
    @SerializedName("Tags")
    @Expose
    @Ignore
    var tags: List<Tag>? = null
    @SerializedName("Poet")
    @Expose
    @TypeConverters(RekhtaTypeConverters.ContentDetailPoetTypeConverter::class)
    var poet: Poet? = null
    @SerializedName("ParaInfo")
    @Expose
    @Ignore
    var paraInfo: List<Any>? = null

    var idWithLang: String? = null

    fun getContentToRender() : ContentToRender {
        return Gson().fromJson<ContentToRender>(cr, ContentToRender::class.java)
    }
}
