package org.rekhta.data.model.dictionary

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class WordMeaningResponseContainer {
    @SerializedName("S")
    @Expose
    var s: Int? = null
    @SerializedName("R")
    @Expose
    var wordMeaningResponse: WordMeaningResponse? = null
    @SerializedName("T")
    @Expose
    var t: String? = null

}
