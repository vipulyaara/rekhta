package org.rekhta.data.model.contentDetail

import androidx.room.Ignore
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Poet {

    @SerializedName("CS")
    @Expose
    var cs: String? = null
    @SerializedName("DS")
    @Expose
    @Ignore
    var ds: String? = null
    @SerializedName("PN")
    @Expose
    var pn: String? = null
    @SerializedName("IU")
    @Expose
    var iu: String? = null
    @SerializedName("LI")
    @Expose
    var li: Boolean? = null

}
