package org.rekhta.data.model.tab

import java.io.Serializable

/**
 * Authored by vipulkumar on 15/12/17.
 */

class TabParcelable: Serializable {
    var tabId: String? = null
    var poetId: String = ""
    var contentTypeId: String? = null
    var currentPage: Int = 0
    var ghazalCount: Int = 0
    var nazmCount: Int = 0
}
