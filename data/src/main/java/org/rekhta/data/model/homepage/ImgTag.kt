package org.rekhta.data.model.homepage

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ImgTag {

    @SerializedName("TagId")
    @Expose
    var tagId: Any? = null
    @SerializedName("TagName")
    @Expose
    var tagName: String? = null
    @SerializedName("TagSlug")
    @Expose
    var tagSlug: String? = null
    @SerializedName("CoupletId")
    @Expose
    var coupletId: String? = null

}
