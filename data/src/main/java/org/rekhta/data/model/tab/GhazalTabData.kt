package org.rekhta.data.model.tab

/**
 * Authored by vipulkumar on 14/12/17.
 */

enum class GhazalTabData private constructor(var value: String?, var targetId: String?) {
    TOP_100("Top 100", "60e03626-6a76-481c-b4d6-cdd6173da417"),
    HUMOUR_SATIRE("Humour/Satire", "7DF9047A-D6B7-4CB0-8180-C29F9AD3B1B7"),
    EDITORS_CHOICE("Editor's Choice", "13ywdpLJ9iEA93BtCwez2w8zXFPWxoDota")
}
