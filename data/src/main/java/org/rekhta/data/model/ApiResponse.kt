package org.rekhta.data.model


import androidx.collection.ArrayMap
import java.io.IOException
import java.util.regex.Pattern

import retrofit2.Response
import timber.log.Timber

/**
 * Common class used by REMOTE responses.
 *
 * @param <T>
</T> */
class ApiResponse<T> {
    val code: Int
    val body: T?
    val errorMessage: String?
    private val links: MutableMap<String, String>

    val isSuccessful: Boolean
        get() {
            val isSuccessfulFromNetwork = code in 200..299
            var isSuccessfulFromApi = true
            if (this.body != null && this.body is org.rekhta.data.model.Response) {
                val resCode = (this.body as org.rekhta.data.model.Response).resCode
                isSuccessfulFromApi = resCode in 200..299
            }
            return isSuccessfulFromNetwork && isSuccessfulFromApi
        }

    constructor(error: Throwable) {
        code = 500
        body = null
        errorMessage = error.message
        links = mutableMapOf()
    }

    constructor(response: Response<T>) {
        code = response.code()
        body = response.body()
        if (isSuccessful) {
            errorMessage = null
        } else {
            var message: String? = null
            if (response.body() != null && response.body() is org.rekhta.data.model.Response) {
                message = (response.body() as org.rekhta.data.model.Response).resMessage
            }
            if (message == null && response.errorBody() != null) {
                try {
                    message = response.errorBody()?.string()
                } catch (ignored: IOException) {
                    Timber.e(ignored, "error while parsing response")
                }

            }
            if (message == null || message.trim { it <= ' ' }.isEmpty()) {
                message = response.message()
            }
            errorMessage = message
        }
        val linkHeader = response.headers().get("link")
        if (linkHeader == null) {
            links = mutableMapOf()
        } else {
            links = ArrayMap()
            val matcher = LINK_PATTERN.matcher(linkHeader)

            while (matcher.find()) {
                val count = matcher.groupCount()
                if (count == 2) {
                    links[matcher.group(2)] = matcher.group(1)
                }
            }
        }
    }

    companion object {
        private val LINK_PATTERN = Pattern
                .compile("<([^>]*)>[\\s]*;[\\s]*rel=\"([a-zA-Z0-9]+)\"")
    }
}
