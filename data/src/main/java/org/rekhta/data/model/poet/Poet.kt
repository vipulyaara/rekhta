package org.rekhta.data.model.poet

import androidx.room.Entity
import androidx.room.PrimaryKey
import android.content.Context
import androidx.annotation.NonNull
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import org.rekhta.data.ImageUtil
import org.rekhta.data.Environment
import org.rekhta.data.R
import org.rekhta.data.model.Language
import org.rekhta.data.model.PagedModel

@Entity
class Poet : PagedModel() {

    @NonNull
    @PrimaryKey
    @SerializedName("I")
    @Expose
    var id: String = ""
    @SerializedName("NE")
    @Expose
    var nameInEnglish: String? = null
    @SerializedName("NH")
    @Expose
    var nameInHindi: String? = null
    @SerializedName("NU")
    @Expose
    var nameInUrdu: String? = null
    @SerializedName("P")
    @Expose
    var isP: Boolean = false
    @SerializedName("SL")
    @Expose
    var profileImageUrl: String? = null
    @SerializedName("HI")
    @Expose
    var isHI: Boolean = false
    @SerializedName("DF")
    @Expose
    var df: String? = null
    @SerializedName("DT")
    @Expose
    var dt: String? = null
    @SerializedName("Le")
    @Expose
    var le: String? = null
    @SerializedName("Lh")
    @Expose
    var lh: String? = null
    @SerializedName("Lu")
    @Expose
    var lu: String? = null
    @SerializedName("GC")
    @Expose
    var ghazalCount: Int = 0
    @SerializedName("NC")
    @Expose
    var nazmCount: Int = 0
    @SerializedName("SC")
    @Expose
    var sherCount: Int = 0
    @SerializedName("DE")
    @Expose
    var descInEnglish: String? = null
    @SerializedName("DH")
    @Expose
    var descInHindi: String? = null
    @SerializedName("DU")
    @Expose
    var descInUrdu: String? = null
    @SerializedName("N")
    @Expose
    var isNew: Boolean = false

    var targetId: String? = null

    public fun getPoetName(): String? {
        return when (Environment.language.value) {
            Language.ENGLISH -> nameInEnglish
            Language.HINDI -> nameInHindi
            Language.URDU -> nameInUrdu
            else -> ""
        }
    }

    fun getFormattedCounts(context: Context): String? {
        val builder = StringBuilder()
        if (ghazalCount > 0) {
            builder.append("$ghazalCount " + context.resources.getQuantityString(R.plurals.ghazal_count, ghazalCount))
        }
        if (nazmCount > 0) {
            builder.append(" • $nazmCount " + context.resources.getQuantityString(R.plurals.nazm_count, nazmCount))
        }
        if (sherCount > 0) {
            builder.append(" • $sherCount " + context.resources.getQuantityString(R.plurals.sher_count, sherCount))
        }
        return builder.toString()
    }

    fun getFormattedImageUrl(): String {
        return if (profileImageUrl != null) ImageUtil.buildImageUrl(profileImageUrl ?: "") else ""
    }
}
