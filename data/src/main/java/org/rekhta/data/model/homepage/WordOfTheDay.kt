package org.rekhta.data.model.homepage

import com.google.gson.Gson
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import org.rekhta.data.model.contentDetail.ContentToRender

class WordOfTheDay {

    @SerializedName("RenderingFormat")
    @Expose
    var renderingFormat: Any? = null
    @SerializedName("Word_En")
    @Expose
    var wordEn: String? = null
    @SerializedName("Word_Hi")
    @Expose
    var wordHi: String? = null
    @SerializedName("Word_Ur")
    @Expose
    var wordUr: String? = null
    @SerializedName("Meaning")
    @Expose
    var meaning: String? = null
    @SerializedName("Title")
    @Expose
    var title: String? = null
    @SerializedName("PoetName")
    @Expose
    var poetName: String? = null
    @SerializedName("PoetSlug")
    @Expose
    var poetSlug: String? = null
    @SerializedName("ParentTitle")
    @Expose
    var parentTitle: String? = null
    @SerializedName("ParentSlug")
    @Expose
    var parentSlug: String? = null

    fun getFormattedWordOfTheDay(): String {
        return "$wordHi • $wordUr"
    }

    fun getContentToRender() : ContentToRender? {
        return Gson().fromJson<ContentToRender?>(title, ContentToRender::class.java)
    }
}
