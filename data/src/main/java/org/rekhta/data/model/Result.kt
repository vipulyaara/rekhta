package org.rekhta.data.model

sealed class Result<T>

data class Success<T>(val data: T, val responseModified: Boolean = true) : Result<T>()

data class ErrorResult<T>(val exception: Exception) : Result<T>()
