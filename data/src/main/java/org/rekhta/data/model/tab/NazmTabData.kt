package org.rekhta.data.model.tab

/**
 * Authored by vipulkumar on 14/12/17.
 */

enum class NazmTabData(var value: String?, var targetId: String?) {
    TOP_50("Top 100", "0DBC9550-9833-4E30-A695-A63679966C69"),
    BEGINNERS("Beginners", "BFCE2895-551E-4C07-B9D7-F7FF0DA9F9F5"),
    EDITORS_CHOICE("Editor's Choice", "18BE4E82-1A8D-4750-B1FF-47B7E95BD458"),
    HUMOR_SATIRE("Humour/Satire", "7DF9047A-D6B7-4CB0-8180-C29F9AD3B1B7")
}
