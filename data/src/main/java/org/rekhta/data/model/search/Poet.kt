package org.rekhta.data.model.search

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import org.rekhta.data.ImageUtil

class Poet {

    @SerializedName("EntityId")
    @Expose
    var entityId: String? = null
    @SerializedName("Name")
    @Expose
    var name: String? = null
    @SerializedName("GazalCount")
    @Expose
    var ghazalCount: Int = 0
    @SerializedName("IsNew")
    @Expose
    var isNew: Boolean? = null
    @SerializedName("NazmCount")
    @Expose
    var nazmCount: Int = 0
    @SerializedName("SEO_Slug")
    @Expose
    var seoSlug: String? = null
    @SerializedName("ImageUrl")
    @Expose
    var imageUrl: String? = null

    fun getFormattedImageUrl(): String {
        return when {
            imageUrl != null -> imageUrl!!
            entityId != null -> ImageUtil.buildImageUrl(entityId!!)
            else -> ""
        }
    }
}
