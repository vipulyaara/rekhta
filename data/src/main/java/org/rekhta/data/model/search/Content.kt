package org.rekhta.data.model.search

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Content {

    @SerializedName("Body")
    @Expose
    var body: String? = null
    @SerializedName("Id")
    @Expose
    var id: String? = null
    @SerializedName("TypeId")
    @Expose
    var typeId: String? = null
    @SerializedName("ContentSlug")
    @Expose
    var contentSlug: String? = null
    @SerializedName("PoetId")
    @Expose
    var poetId: String? = null
    @SerializedName("PoetSlug")
    @Expose
    var poetSlug: String? = null
    @SerializedName("PoetName")
    @Expose
    var poetName: String? = null
    @SerializedName("ImageUrl")
    @Expose
    var imageUrl: String? = null
    @SerializedName("ContentUrl")
    @Expose
    var contentUrl: String? = null
    @SerializedName("AudioCount")
    @Expose
    var audioCount: Int? = null
    @SerializedName("VideoCount")
    @Expose
    var videoCount: Int? = null
    @SerializedName("EditorChoice")
    @Expose
    var editorChoice: Boolean? = null
    @SerializedName("PopularChoice")
    @Expose
    var popularChoice: Boolean? = null
    @SerializedName("ShortUrlIndex")
    @Expose
    var shortUrlIndex: Int? = null
    @SerializedName("Title")
    @Expose
    var title: String? = null

}
