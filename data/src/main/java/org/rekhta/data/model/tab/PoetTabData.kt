package org.rekhta.data.model.tab

/**
 * Authored by vipulkumar on 14/12/17.
 */

enum class PoetTabData private constructor(var value: String?, var targetId: String?) {
    ALL("All", "B16B3E22-7881-4FE3-B2BD-94D45CC47139"),
    TOP_READ("Top Read", "3AB1ACC4-E080-4B77-BFC1-41E2908E6C2D"),
    WOMEN_POETS("Women Poets", "9F15FCDD-C531-4EB9-A25E-F2F7DB8C7CF3"),
//    YOUNG_POETS("Young Poets", ""),
    CLASSICAL_POETS("Classical Poets", "517093F6-D295-4389-AA02-AEA52E738B97")
}
