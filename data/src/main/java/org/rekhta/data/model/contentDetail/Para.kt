package org.rekhta.data.model.contentDetail

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class Para : Serializable {

    @SerializedName("L")
    @Expose
    var line: List<Line>? = null
    @SerializedName("T")
    @Expose
    var t: Any? = null

}
