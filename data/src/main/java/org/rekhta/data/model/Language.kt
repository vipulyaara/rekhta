package org.rekhta.data.model

enum class Language(var value: String?, var code: Int) {
    ENGLISH("English", 1),
    HINDI("Hindi", 2),
    URDU("Urdu", 3);
}
