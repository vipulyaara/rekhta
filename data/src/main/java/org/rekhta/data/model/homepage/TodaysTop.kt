package org.rekhta.data.model.homepage

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class TodaysTop {

    @SerializedName("ContentId")
    @Expose
    var contentId: String? = null
    @SerializedName("Slug")
    @Expose
    var slug: String? = null
    @SerializedName("ShortUrlIndex")
    @Expose
    var shortUrlIndex: Int? = null
    @SerializedName("Title")
    @Expose
    var title: String? = null
    @SerializedName("Translation")
    @Expose
    var translation: Any? = null
    @SerializedName("Alignment")
    @Expose
    var alignment: Int? = null
    @SerializedName("RenderingFormat")
    @Expose
    var renderingFormat: Int? = null
    @SerializedName("TypeSlug")
    @Expose
    var typeSlug: String? = null
    @SerializedName("TranslatorName")
    @Expose
    var translatorName: Any? = null
    @SerializedName("PoetName")
    @Expose
    var poetName: String? = null
    @SerializedName("PoetSlug")
    @Expose
    var poetSlug: String? = null
    @SerializedName("PoetDPSlug")
    @Expose
    var poetDPSlug: String? = null
    @SerializedName("ParentSlug")
    @Expose
    var parentSlug: Any? = null
    @SerializedName("ParentTypeSlug")
    @Expose
    var parentTypeSlug: Any? = null
    @SerializedName("Tags")
    @Expose
    var tags: List<Tag>? = null

}
