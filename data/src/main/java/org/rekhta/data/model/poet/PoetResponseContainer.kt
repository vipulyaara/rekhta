package org.rekhta.data.model.poet

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class PoetResponseContainer {

    @SerializedName("S")
    @Expose
    var s: Int = 0
    @SerializedName("Me")
    @Expose
    var me: Any? = null
    @SerializedName("Mh")
    @Expose
    var mh: Any? = null
    @SerializedName("Mu")
    @Expose
    var mu: Any? = null
    @SerializedName("R")
    @Expose
    var response: PoetListResponse? = null
    @SerializedName("T")
    @Expose
    var t: String? = null

}
