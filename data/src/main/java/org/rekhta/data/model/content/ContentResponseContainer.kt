package org.rekhta.data.model.content

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ContentResponseContainer {

    @SerializedName("S")
    @Expose
    var s: Int? = null
    @SerializedName("Me")
    @Expose
    var me: Any? = null
    @SerializedName("Mh")
    @Expose
    var mh: Any? = null
    @SerializedName("Mu")
    @Expose
    var mu: Any? = null
    @SerializedName("R")
    @Expose
    var contentResponse: ContentResponse? = null
    @SerializedName("T")
    @Expose
    var t: String? = null

}
