package org.rekhta.data.model.audio

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import org.rekhta.data.Environment
import org.rekhta.data.model.Language
import org.rekhta.data.model.content.Content
import org.rekhta.data.util.StringUtil

class Audio {

    @SerializedName("I")
    @Expose
    var i: String? = null
    @SerializedName("PI")
    @Expose
    var pi: String? = null
    @SerializedName("NE")
    @Expose
    var ne: String? = null
    @SerializedName("NH")
    @Expose
    var nh: String? = null
    @SerializedName("NU")
    @Expose
    var nu: String? = null
    @SerializedName("PS")
    @Expose
    var ps: String? = null
    @SerializedName("CI")
    @Expose
    var ci: String? = null
    @SerializedName("TE")
    @Expose
    var te: String? = null
    @SerializedName("TH")
    @Expose
    var th: String? = null
    @SerializedName("TU")
    @Expose
    var tu: String? = null
    @SerializedName("CS")
    @Expose
    var cs: String? = null
    @SerializedName("AI")
    @Expose
    var ai: String? = null
    @SerializedName("AE")
    @Expose
    var ae: String? = null
    @SerializedName("AH")
    @Expose
    var ah: String? = null
    @SerializedName("AU")
    @Expose
    var au: String? = null
    @SerializedName("AS")
    @Expose
    var `as`: String? = null
    @SerializedName("TI")
    @Expose
    var ti: String? = null
    @SerializedName("PSN")
    @Expose
    var psn: String? = null
    @SerializedName("ASN")
    @Expose
    var asn: String? = null
    @SerializedName("HA")
    @Expose
    var ha: Boolean? = null
    @SerializedName("HP")
    @Expose
    var hp: Boolean? = null
    @SerializedName("AD")
    @Expose
    var ad: String? = null

    fun getContentText(): String? {
        return StringUtil.capitalizeFirstCharacter(getContentTextRaw()?.trim())
    }

    fun getContentTextRaw(): String? {
        return when (Environment.language.value) {
            Language.ENGLISH -> te
            Language.HINDI -> th
            Language.URDU -> tu
            else -> ""
        }
    }

    override fun equals(other: Any?): Boolean {
        return if (other is Content) {
            getContentTextRaw().equals(other.getContentTitleRaw())
        } else {
            super.equals(other)
        }
    }

}
