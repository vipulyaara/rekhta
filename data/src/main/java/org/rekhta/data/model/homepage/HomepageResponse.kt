package org.rekhta.data.model.homepage

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import org.rekhta.data.db.RekhtaTypeConverters
import org.rekhta.data.model.search.Poet

@Entity
class HomepageResponse {

    @PrimaryKey(autoGenerate = true)
    var id: Int? = 0
    @SerializedName("Carousels")
    @Expose
    @Ignore
    var carousels: List<Carousel>? = null
    @SerializedName("TopPoets")
    @Expose
    @TypeConverters(RekhtaTypeConverters.SearchPoetListTypeConverter::class)
    var topPoets: List<Poet>? = null
    @SerializedName("WordOfTheDay")
    @Expose
    var wordOfTheDay: WordOfTheDay? = null
    @SerializedName("Video")
    @Expose
    @TypeConverters(RekhtaTypeConverters.VideoTypeConverter::class)
    var video: Video? = null
    @SerializedName("Featured")
    @Expose
    @TypeConverters(RekhtaTypeConverters.FeaturedListTypeConverter::class)
    var featured: List<Featured>? = null
    @SerializedName("Cards")
    @Expose
    @TypeConverters(RekhtaTypeConverters.CardListTypeConverter::class)
    var cards: List<Card>? = null
    @SerializedName("TodaysTop")
    @Expose
    @Ignore
    var todaysTop: List<TodaysTop>? = null
    @SerializedName("ImgShayari")
    @Expose
    @TypeConverters(RekhtaTypeConverters.ImageShayriListTypeConverter::class)
    var imgShayari: List<ImgShayari>? = null
    @SerializedName("T20Series")
    @Expose
    @Ignore
    var t20Series: List<T20Series>? = null


}
