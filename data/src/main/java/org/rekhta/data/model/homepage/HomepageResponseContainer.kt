package org.rekhta.data.model.homepage

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class HomepageResponseContainer {

    @SerializedName("S")
    @Expose
    var s: Int? = null
    @SerializedName("Me")
    @Expose
    var me: Any? = null
    @SerializedName("Mh")
    @Expose
    var mh: Any? = null
    @SerializedName("Mu")
    @Expose
    var mu: Any? = null
    @SerializedName("R")
    @Expose
    var response: HomepageResponse? = null
    @SerializedName("T")
    @Expose
    var t: String? = null
}
