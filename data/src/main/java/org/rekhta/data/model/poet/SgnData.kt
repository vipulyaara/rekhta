package org.rekhta.data.model.poet

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class SgnData {

    @SerializedName("I")
    @Expose
    var i: String? = null
    @SerializedName("S")
    @Expose
    var s: Int? = null
    @SerializedName("C")
    @Expose
    var c: Int? = null

}


