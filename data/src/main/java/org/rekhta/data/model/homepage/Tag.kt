package org.rekhta.data.model.homepage

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Tag {
    @SerializedName("TagId")
    @Expose
    var tagId: String? = null
    @SerializedName("TagName")
    @Expose
    var tagName: String? = null
    @SerializedName("TagSlug")
    @Expose
    var tagSlug: String? = null
    @SerializedName("ContentId")
    @Expose
    var contentId: String? = null
}
