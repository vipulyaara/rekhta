package org.rekhta.data.model.homepage

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import org.rekhta.data.ImageUtil

class Video {

    @SerializedName("Youtube_Id")
    @Expose
    var youtubeId: String? = null
    @SerializedName("EntityName")
    @Expose
    var entityName: String? = null
    @SerializedName("EntityUrl")
    @Expose
    var entityUrl: String? = null
    @SerializedName("VideoTitle")
    @Expose
    var videoTitle: String? = null
    @SerializedName("ImageUrl")
    @Expose
    var imageUrl: String? = null


    fun getFormattedImageUrl(): String? {
        return ImageUtil.buildVideoUrl(imageUrl ?: "")
    }
}
