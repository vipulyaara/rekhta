package org.rekhta.data.model.poet

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import androidx.annotation.NonNull
import android.text.Spanned
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import org.rekhta.data.Environment
import org.rekhta.data.model.Language
import org.rekhta.data.util.StringUtil

@Entity
class PoetProfile {
    @NonNull
    @PrimaryKey
    @SerializedName("I")
    @Expose
    var poetId: String = ""
    @SerializedName("NE")
    @Expose
    var nameInEnglish: String? = null
    @SerializedName("NH")
    @Expose
    var nameInHindi: String? = null
    @SerializedName("NU")
    @Expose
    var nameInUrdu: String? = null
    @SerializedName("H")
    @Expose
    var h: Boolean? = null
    @SerializedName("PE")
    @Expose
    var descInEnglish: String? = null
    @SerializedName("PH")
    @Expose
    var descInHindi: String? = null
    @SerializedName("PU")
    @Expose
    var descInUrdu: String? = null
    @SerializedName("U")
    @Expose
    var u: String? = null
    @SerializedName("Be")
    @Expose
    var birthPlaceInEnglish: String? = null
    @SerializedName("Bh")
    @Expose
    var birthPlaceInHindi: String? = null
    @SerializedName("Bu")
    @Expose
    var birthPlaceInUrdu: String? = null
    @SerializedName("DOe")
    @Expose
    var deathPlaceInEnglish: String? = null
    @SerializedName("DOh")
    @Expose
    var deathPlaceInHindi: String? = null
    @SerializedName("DOu")
    @Expose
    var deathPlaceInUrdu: String? = null
    @SerializedName("RNe")
    @Expose
    var realNameInEnglish: String? = null
    @SerializedName("RNh")
    @Expose
    var realNameInHindi: String? = null
    @SerializedName("RNu")
    @Expose
    var realNameInUrdu: String? = null
    @SerializedName("dob")
    @Expose
    var dateOfBirth: String? = null
    @SerializedName("dod")
    @Expose
    var dateOfDeath: String? = null
    @SerializedName("dobd")
    @Expose
    var dobd: String? = null
    @SerializedName("dodd")
    @Expose
    var dodd: String? = null
    @SerializedName("AC")
    @Expose
    var noOfAudios: Int? = null
    @SerializedName("VC")
    @Expose
    var noOfVideos: Int? = null
    @SerializedName("EC")
    @Expose
    var noOfEditorChoice: Int? = null
    @SerializedName("C")
    @Expose
    @Ignore
    var c: List<SgnData>? = null

    fun getProfileDesc(): Spanned? {
        return StringUtil.getSpannableFromHtml(getProfileDescRaw())
    }

    fun getProfileDescRaw(): String? {
        return when (Environment.language.value) {
            Language.ENGLISH -> descInEnglish
            Language.HINDI -> descInHindi
            Language.URDU -> descInUrdu
            else -> ""
        }
    }

    fun getPoetName(): String? {
        return when (Environment.language.value) {
            Language.ENGLISH -> nameInEnglish
            Language.HINDI -> nameInHindi
            Language.URDU -> nameInUrdu
            else -> ""
        }
    }

    fun getDeathPlace(): String? {
        return when (Environment.language.value) {
            Language.ENGLISH -> deathPlaceInEnglish
            Language.HINDI -> deathPlaceInHindi
            Language.URDU -> deathPlaceInUrdu
            else -> ""
        }
    }

    fun getShortInfo(): String {
        return dateOfBirth + " - " + (dateOfDeath ?: "") + "  •  " + getDeathPlace()
    }

}
