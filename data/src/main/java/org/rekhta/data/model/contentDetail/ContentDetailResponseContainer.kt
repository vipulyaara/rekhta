package org.rekhta.data.model.contentDetail

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ContentDetailResponseContainer {

    @SerializedName("S")
    @Expose
    var s: Int? = null
    @SerializedName("Me")
    @Expose
    private var me: String? = null
    @SerializedName("Mh")
    @Expose
    var mh: String? = null
    @SerializedName("Mu")
    @Expose
    var mu: String? = null
    @SerializedName("R")
    @Expose
    var contentDetailResponse: ContentDetailResponse? = null
    @SerializedName("T")
    @Expose
    var t: String? = null

}
