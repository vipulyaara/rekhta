package org.rekhta.data.model.tab

import androidx.fragment.app.Fragment
import java.io.Serializable

/**
 * Authored by vipulkumar on 15/12/17.
 */

class BaseTabData : Serializable {
    lateinit var fragments: List<Fragment?>
    lateinit var titles: List<String?>

    companion object {
        val TAB_POET_LIST  = "tab_poet_list"
        val TAB_PROFILE  = "tab_profile"
        val TAB_GHAZALS  = "tab_ghazals"
        val TAB_NAZMS = "tab_nazms"
        val CONTENT_TYPE_ID_GHAZALS = "43d60a15-0b49-4caf-8b74-0fcdddeb9f83"
        val CONTENT_TYPE_ID_NAZMS = "c54c4d8b-7e18-4f70-8312-e1c2cc028b0b"
    }
}
