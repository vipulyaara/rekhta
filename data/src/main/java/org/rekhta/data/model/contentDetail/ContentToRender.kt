package org.rekhta.data.model.contentDetail

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

import java.io.Serializable

class ContentToRender : Serializable {
    @SerializedName("P")
    @Expose
    var para: List<Para>? = null
}
