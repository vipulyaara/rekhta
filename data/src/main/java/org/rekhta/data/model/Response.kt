package org.rekhta.data.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Response {

    @SerializedName("resCode")
    @Expose
    var resCode: Int? = null
    @SerializedName("resMessage")
    @Expose
    var resMessage: String? = null
    @SerializedName("interactionId")
    @Expose
    var interactionId: String? = null
    @SerializedName("interationType")
    @Expose
    var interationType: String? = null
    @SerializedName("sessionId")
    @Expose
    var sessionId: String? = null
}
