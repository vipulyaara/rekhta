package org.rekhta.data.model.contentDetail

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Audio {

    @SerializedName("I")
    @Expose
    var i: String? = null
    @SerializedName("SQ")
    @Expose
    var sq: Int? = null
    @SerializedName("AN")
    @Expose
    var an: String? = null
    @SerializedName("HI")
    @Expose
    var hi: Boolean? = null
    @SerializedName("ASS")
    @Expose
    var ass: String? = null
    @SerializedName("ADS")
    @Expose
    var ads: String? = null
    @SerializedName("IU")
    @Expose
    var iu: String? = null
    @SerializedName("AU")
    @Expose
    var au: String? = null
    @SerializedName("AT")
    @Expose
    var at: String? = null

}
