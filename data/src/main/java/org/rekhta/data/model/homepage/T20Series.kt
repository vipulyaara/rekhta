package org.rekhta.data.model.homepage

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class T20Series {

    @SerializedName("Id")
    @Expose
    var id: String? = null
    @SerializedName("Name_En")
    @Expose
    var nameEn: String? = null
    @SerializedName("Name_Hi")
    @Expose
    var nameHi: String? = null
    @SerializedName("Name_Ur")
    @Expose
    var nameUr: String? = null
    @SerializedName("Description_En")
    @Expose
    var descriptionEn: String? = null
    @SerializedName("Description_Hi")
    @Expose
    var descriptionHi: String? = null
    @SerializedName("Description_Ur")
    @Expose
    var descriptionUr: String? = null
    @SerializedName("LaunchDate")
    @Expose
    var launchDate: String? = null
    @SerializedName("IsPoetBased")
    @Expose
    var isPoetBased: Boolean? = null
    @SerializedName("ListId")
    @Expose
    var listId: String? = null
    @SerializedName("SEO_Slug")
    @Expose
    var seoSlug: String? = null
    @SerializedName("Title_En")
    @Expose
    var titleEn: String? = null
    @SerializedName("Title_Hi")
    @Expose
    var titleHi: String? = null
    @SerializedName("Title_Ur")
    @Expose
    var titleUr: String? = null
    @SerializedName("PoetId")
    @Expose
    var poetId: String? = null
    @SerializedName("MediumImageId")
    @Expose
    var mediumImageId: Any? = null

}
