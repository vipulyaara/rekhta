package org.rekhta.data.model.search

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import androidx.annotation.NonNull
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import org.rekhta.data.db.RekhtaTypeConverters

@Entity
class SearchResponse {

    @NonNull
    @PrimaryKey(autoGenerate = true)
    var id: Int? = null
    @SerializedName("Poets")
    @Expose
    @TypeConverters(RekhtaTypeConverters.SearchPoetListTypeConverter::class)
    var poets: List<Poet>? = null
    @SerializedName("PoetsTotal")
    @Expose
    var poetsTotal: Int? = null
    @SerializedName("GhazalsTotal")
    @Expose
    var ghazalsTotal: Int? = null
    @SerializedName("CoupletsTotal")
    @Expose
    var coupletsTotal: Int? = null
    @SerializedName("NazmsTotal")
    @Expose
    var nazmsTotal: Int? = null
    @SerializedName("Content")
    @Expose
    @Ignore
    var content: List<Content>? = null
    @SerializedName("ContentTotal")
    @Expose
    var contentTotal: Int? = null
    @SerializedName("Dictionary")
    @Expose
    @TypeConverters(RekhtaTypeConverters.DictionaryPoetListTypeConverter::class)
    var dictionary: List<Dictionary>? = null
    @SerializedName("Tags")
    @Expose
    @TypeConverters(RekhtaTypeConverters::class)
    var tags: List<String>? = null
    @SerializedName("AppPages")
    @Expose
    @TypeConverters(RekhtaTypeConverters::class)
    var appPages: List<String>? = null
    @SerializedName("Banners")
    @Expose
    @TypeConverters(RekhtaTypeConverters::class)
    var banners: List<String>? = null
    @SerializedName("T20")
    @Expose
    @TypeConverters(RekhtaTypeConverters::class)
    var t20: List<String>? = null
    @SerializedName("Message")
    @Expose
    var message: String? = null

}
