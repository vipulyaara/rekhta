package org.rekhta.data.model.contentDetail

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Video {

    @SerializedName("YI")
    @Expose
    var yi: String? = null
    @SerializedName("ST")
    @Expose
    var st: String? = null
    @SerializedName("SQ")
    @Expose
    var sq: Int? = null
    @SerializedName("VT")
    @Expose
    var vt: String? = null
    @SerializedName("AN")
    @Expose
    var an: String? = null
    @SerializedName("HI")
    @Expose
    var hi: Boolean? = null
    @SerializedName("ASS")
    @Expose
    var ass: String? = null
    @SerializedName("ADS")
    @Expose
    var ads: Any? = null
    @SerializedName("AU")
    @Expose
    var au: String? = null
    @SerializedName("IU")
    @Expose
    var iu: String? = null

}
