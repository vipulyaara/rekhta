package org.rekhta.data.model.dictionary

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by VipulKumar on 2/15/18.
 */
@Entity
class WordMeaningResponse {
    @SerializedName("m")
    @Expose
    var m: String? = null
    @SerializedName("A")
    @Expose
    var a: Boolean? = null
    @SerializedName("I")
    @Expose
    @PrimaryKey
    var id: String = ""
    @SerializedName("E")
    @Expose
    var wordInEnglish: String? = null
    @SerializedName("H")
    @Expose
    var wordInHindi: String? = null
    @SerializedName("U")
    @Expose
    var wordInUrdu: String? = null
    @SerializedName("M1E")
    @Expose
    var meaningInEnglish1: String? = null
    @SerializedName("M2E")
    @Expose
    var meaningInEnglish2: String? = null
    @SerializedName("M3E")
    @Expose
    var meaningInEnglish3: String? = null
    @SerializedName("M1H")
    @Expose
    var meaningInHindi1: String? = null
    @SerializedName("M2H")
    @Expose
    var meaningInHindi2: String? = null
    @SerializedName("M3H")
    @Expose
    var meaningInHindi3: String? = null
    @SerializedName("M1U")
    @Expose
    var meaningInUrdu1: String? = null
    @SerializedName("M2U")
    @Expose
    var meaningInUrdu2: String? = null
    @SerializedName("M3U")
    @Expose
    var meaningInUrdu3: String? = null

    fun formattedWordInHindiEnglish(): String {
        return wordInHindi?.trim() + " • " + wordInUrdu?.trim()
    }
}
