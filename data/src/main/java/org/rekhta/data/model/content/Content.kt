package org.rekhta.data.model.content

import androidx.annotation.NonNull
import androidx.room.Entity
import androidx.room.PrimaryKey

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import org.rekhta.data.Environment
import org.rekhta.data.model.Language
import org.rekhta.data.model.PagedModel
import org.rekhta.data.util.StringUtil

@Entity
class Content : PagedModel() {

    @NonNull
    @PrimaryKey
    @SerializedName("I")
    @Expose
    var i: String = ""
    @SerializedName("T")
    @Expose
    var t: String? = null
    @SerializedName("PI")
    @Expose
    var pi: String? = null
    @SerializedName("PE")
    @Expose
    var pe: String? = null
    @SerializedName("PH")
    @Expose
    var ph: String? = null
    @SerializedName("PU")
    @Expose
    var pu: String? = null
    @SerializedName("P")
    @Expose
    var p: String? = null
    @SerializedName("TE")
    @Expose
    var te: String? = null
    @SerializedName("TH")
    @Expose
    var th: String? = null
    @SerializedName("TU")
    @Expose
    var tu: String? = null
    @SerializedName("SE")
    @Expose
    var se: String? = null
    @SerializedName("SH")
    @Expose
    var sh: String? = null
    @SerializedName("SU")
    @Expose
    var su: String? = null
    @SerializedName("R")
    @Expose
    var r: String? = null
    @SerializedName("S")
    @Expose
    var s: String? = null
    @SerializedName("SI")
    @Expose
    var si: Int? = null
    @SerializedName("N")
    @Expose
    var n: Boolean? = null
    @SerializedName("EC")
    @Expose
    var ec: Boolean? = null
    @SerializedName("PC")
    @Expose
    var pc: Boolean? = null
    @SerializedName("AU")
    @Expose
    var au: Boolean? = null
    @SerializedName("VI")
    @Expose
    var vi: Boolean? = null
    @SerializedName("AC")
    @Expose
    var ac: Int? = null
    @SerializedName("VC")
    @Expose
    var vc: Int? = null
    @SerializedName("UE")
    @Expose
    var ue: String? = null
    @SerializedName("UH")
    @Expose
    var uh: String? = null
    @SerializedName("UU")
    @Expose
    var uu: String? = null
    @SerializedName("FC")
    @Expose
    var fc: Int? = null
    @SerializedName("SC")
    @Expose
    var sc: Int? = null

    var targetId: String? = null
    var poetId: String? = null
    var contentTypeId: String? = null

    fun getPoetName(): String? {
        return when (Environment.language.value) {
            Language.ENGLISH -> pe
            Language.HINDI -> ph
            Language.URDU -> pu
            else -> ""
        }
    }

    fun getContentTitle(): String? {
        return StringUtil.capitalizeFirstCharacter(getContentTitleRaw()?.trim() ?: "")
    }

    fun getContentText(): String? {
        return StringUtil.capitalizeFirstCharacter(getContentTextRaw()?.trim() ?: "")
    }

    fun getContentTitleRaw(): String? {
        return when (Environment.language.value) {
            Language.ENGLISH -> te
            Language.HINDI -> th
            Language.URDU -> tu
            else -> ""
        }
    }

    fun getContentTextRaw(): String? {
        return when (Environment.language.value) {
            Language.ENGLISH -> se
            Language.HINDI -> sh
            Language.URDU -> su
            else -> ""
        }
    }

    override fun equals(other: Any?): Boolean {
        return if (other is Content) {
            getContentTitleRaw().equals(other.getContentTitleRaw())
        } else {
            super.equals(other)
        }
    }
}
