package org.rekhta.data.model.search

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Dictionary {

    @SerializedName("Id")
    @Expose
    var id: String? = null
    @SerializedName("Urdu")
    @Expose
    var urdu: String? = null
    @SerializedName("Hindi")
    @Expose
    var hindi: String? = null
    @SerializedName("English")
    @Expose
    var english: String? = null
    @SerializedName("Meaning1_En")
    @Expose
    var meaning1En: String? = null
    @SerializedName("Meaning2_En")
    @Expose
    var meaning2En: String? = null
    @SerializedName("Meaning3_En")
    @Expose
    var meaning3En: String? = null
    @SerializedName("Meaning1_Hi")
    @Expose
    var meaning1Hi: String? = null
    @SerializedName("Meaning2_Hi")
    @Expose
    var meaning2Hi: String? = null
    @SerializedName("Meaning3_Hi")
    @Expose
    var meaning3Hi: String? = null
    @SerializedName("Meaning1_Ur")
    @Expose
    var meaning1Ur: String? = null
    @SerializedName("Meaning2_Ur")
    @Expose
    var meaning2Ur: String? = null
    @SerializedName("Meaning3_Ur")
    @Expose
    var meaning3Ur: String? = null

}
