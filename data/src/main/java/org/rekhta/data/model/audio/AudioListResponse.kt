package org.rekhta.data.model.audio

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class AudioListResponse {

    @SerializedName("TC")
    @Expose
    var tc: Int? = null
    @SerializedName("A")
    @Expose
    var audios: List<Audio>? = null

}
