package org.rekhta.data.model.homepage

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import org.rekhta.data.ImageUtil

class Card {

    @SerializedName("TextHeading")
    @Expose
    var textHeading: String? = null
    @SerializedName("CardId")
    @Expose
    var cardId: String = ""
    @SerializedName("Link")
    @Expose
    var link: String? = null
    @SerializedName("Name")
    @Expose
    var name: String? = null
    @SerializedName("TextContent")
    @Expose
    var textContent: String? = null

    fun getImageUrl(): String {
        return ImageUtil.buildCardImageUrl(cardId)
    }
}
