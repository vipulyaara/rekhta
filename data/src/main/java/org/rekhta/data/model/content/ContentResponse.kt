package org.rekhta.data.model.content

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ContentResponse {

    @SerializedName("TC")
    @Expose
    var tc: Int? = null
    @SerializedName("CS")
    @Expose
    var cs: List<Content>? = null

}
