package org.rekhta.data.model.poet

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import org.rekhta.data.model.poet.Poet

class PoetListResponse {

    @SerializedName("TC")
    @Expose
    var tc: Int = 0
    @SerializedName("P")
    @Expose
    var poets: List<Poet>? = null

}
