package org.rekhta.data.model.homepage

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ImgShayari {

    @SerializedName("Id")
    @Expose
    var id: String? = null
    @SerializedName("PoetId")
    @Expose
    var poetId: String? = null
    @SerializedName("Name")
    @Expose
    var name: String? = null
    @SerializedName("CoupletId")
    @Expose
    var coupletId: String? = null
    @SerializedName("IsSher")
    @Expose
    var isSher: Boolean? = null
    @SerializedName("Tag1")
    @Expose
    var tag1: String? = null
    @SerializedName("SEO_Slug")
    @Expose
    var seoSlug: String? = null
    @SerializedName("ShortUrlIndex")
    @Expose
    var shortUrlIndex: Int? = null
    @SerializedName("ShortUrl_En")
    @Expose
    var shortUrlEn: String? = null
    @SerializedName("ShortUrl_Hi")
    @Expose
    var shortUrlHi: String? = null
    @SerializedName("ShortUrl_Ur")
    @Expose
    var shortUrlUr: String? = null
    @SerializedName("imgTags")
    @Expose
    var imgTags: List<ImgTag>? = null

}
