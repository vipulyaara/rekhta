package org.rekhta.data.model.poet

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class PoetProfileResponseContainer {
    @SerializedName("S")
    @Expose
    var s: Int? = null
    @SerializedName("Me")
    @Expose
    var me: String? = null
    @SerializedName("Mh")
    @Expose
    var mh: String? = null
    @SerializedName("Mu")
    @Expose
    var mu: String? = null

    @SerializedName("R")
    @Expose
    var response: PoetProfile? = null
    @SerializedName("T")
    @Expose
    var t: String? = null

}
