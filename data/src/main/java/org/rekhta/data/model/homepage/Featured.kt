package org.rekhta.data.model.homepage

import com.google.gson.Gson
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import org.rekhta.data.ImageUtil
import org.rekhta.data.model.contentDetail.ContentToRender

class Featured {

    @SerializedName("DayType")
    @Expose
    var dayType: Int? = null
    @SerializedName("EntityUrl")
    @Expose
    var entityUrl: String? = null
    @SerializedName("EntityName")
    @Expose
    var entityName: String? = null
    @SerializedName("ImageUrl")
    @Expose
    var imageUrl: String = ""
    @SerializedName("Title")
    @Expose
    var title: String? = null
    @SerializedName("FromDate")
    @Expose
    var fromDate: String? = null
    @SerializedName("ToDate")
    @Expose
    var toDate: String? = null
    @SerializedName("Remarks")
    @Expose
    var remarks: String? = null
    @SerializedName("DestinationUrl")
    @Expose
    var destinationUrl: String? = null
    @SerializedName("AnchorText")
    @Expose
    var anchorText: String? = null
    @SerializedName("RenderingFormat")
    @Expose
    var renderingFormat: Int? = null

    fun getFormattedImageUrl(): String {
        return ImageUtil.buildImageUrl(imageUrl)
    }

    fun getTitleText() : ContentToRender? {
        return Gson().fromJson<ContentToRender>(title, ContentToRender::class.java)
    }

}
