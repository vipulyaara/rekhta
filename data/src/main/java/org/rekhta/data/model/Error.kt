package org.rekhta.data.model

/**
 * Created by b0203949 on 28/03/18.
 */

class Error(val errorCode: Int, val message: String?, val resolution: String? = "") {

    companion object {
        const val RESOLUTION_PLEASE_RETRY = "Please retry"
        fun build(errorValue: ErrorValue, code: Int = errorValue.code,
                  message: String? = errorValue.message, resolution: String? = errorValue.resolution): Error {
            return Error(code, message, resolution)
        }
    }

    enum class ErrorValue(val code: Int, val message: String?, val resolution: String?) {
        ERROR_NETWORK_ERROR(0, "", RESOLUTION_PLEASE_RETRY),
        ERROR_MIGRATION_FAILED(1000, "Migration Failed", RESOLUTION_PLEASE_RETRY),
        OTP_GENERATION_FAILED(1001, "Otp generation failed from server", RESOLUTION_PLEASE_RETRY),
        OTP_VERIFICATION_FAILED(1002, "OTP verification failed from server", RESOLUTION_PLEASE_RETRY),
        PERMISSIONS_NOT_GRANTED(1003, "Mandatory permissions are not granted", RESOLUTION_PLEASE_RETRY)
    }
}
