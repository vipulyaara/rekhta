package org.rekhta.data.model.contentDetail

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

import java.io.Serializable

class Line : Serializable {

    @SerializedName("W")
    @Expose
    var word: List<Word>? = null

}
