package org.rekhta.data

import android.content.Context
import androidx.lifecycle.MutableLiveData
import org.rekhta.data.model.Language
import org.rekhta.data.data.SharedPrefUtil

/**
 * Authored by vipulkumar on 15/12/17.
 */

object Environment {
    var showNetworkLogs = true
    var showSnackbarAsDefaultFeedback = false


    var language: MutableLiveData<Language> = MutableLiveData()

    fun setLanguage(context: Context?, language: Language) {
        SharedPrefUtil.putString(context!!, "language", language.name)
        Environment.language.value = language
    }

    fun getLanguage(context: Context?): Language {
        val languageVal: String = SharedPrefUtil.getString(context!!, "language", "ENGLISH")
        return Language.valueOf(languageVal)
    }
}

fun langCode(context: Context?): Int = Environment.getLanguage(context).code
