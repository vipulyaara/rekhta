package org.rekhta.data

/**
 * Created by VipulKumar on 09/09/18.
 */
class FavoritePoetsBuilder {
    val mostVisitedPoets = arrayListOf<VisitedPoet>()

    fun getMostVisitedPoets() {
        val enoughPoets =  mostVisitedPoets.size > 6
        val enoughFrequency = mostVisitedPoets.subList(0, 4)
    }

    fun addToVisitedPoets(poetId: String) {

    }

    data class VisitedPoet(var poetId: String = "", var frequency: Int = 0)
}
