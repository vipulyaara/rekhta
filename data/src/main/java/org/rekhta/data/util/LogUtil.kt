package org.rekhta.data.util

import timber.log.Timber

/**
 * Created by VipulKumar
 */
object LogUtil {

    fun d(message: String, vararg args: Any) {
        Timber.d(message, *args)
    }

    fun i(message: String, vararg args: Any) {
        Timber.i(message, *args)
    }

    fun w(message: String, vararg args: Any) {
        Timber.w(message, *args)
    }

    fun v(message: String, vararg args: Any) {
        Timber.v(message, *args)
    }

    fun e(message: String, vararg args: Any) {
        Timber.e(message, *args)
    }
}
