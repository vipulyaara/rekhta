package org.rekhta.data.util

import android.net.Uri
import android.text.Html
import android.text.Spanned
import android.text.TextUtils

/**
 * Authored by vipulkumar on 23/09/17.
 */

object StringUtil {
    fun capitalizeFirstCharacterOfEveryWord(string: String): String {
        var string = string
        if (TextUtils.isEmpty(string)) {
            return ""
        } else {
            string = string.toLowerCase()
            val res = StringBuilder()
            if (TextUtils.isEmpty(string))
                return ""
            val strArr = string.split(" ".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
            for (str in strArr) {
                val stringArray = str.trim { it <= ' ' }.toCharArray()
                stringArray[0] = Character.toUpperCase(stringArray[0])
                val strs = String(stringArray)

                res.append(strs).append(" ")
            }
            return res.toString().trim { it <= ' ' }
        }
    }

    fun capitalizeFirstCharacter(string: String?): String {
        return if (TextUtils.isEmpty(string)) {
            ""
        } else {
            string?.substring(0, 1)?.toUpperCase() + string?.substring(1)
        }
    }

    fun removeSpacesFromUrl(url: String): String {
        val uri = Uri.parse(url.replace(" ", "%20"))
        return uri.toString()
    }

    fun getSpannableFromHtml(text: String?): Spanned? {
        return if (text == null) null else Html.fromHtml(text)
    }

    fun isEmpty(input: String): Boolean {
        return TextUtils.isEmpty(input)
    }

    fun isEmpty(input: CharSequence): Boolean {
        return TextUtils.isEmpty(input)
    }
}
