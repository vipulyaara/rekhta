package org.rekhta.data.util

import android.app.Activity
import android.content.Context
import android.os.Build
import android.view.View
import android.view.inputmethod.InputMethodManager

/**
 * Authored by vipulkumar on 27/09/17.
 */

object DeviceUtil {
    val isPostLollipop: Boolean
        get() = Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP

    val isPostMarshmallow: Boolean
        get() = Build.VERSION.SDK_INT >= Build.VERSION_CODES.M

    fun hideSoftKeyboard(activity: Activity) {
        hideSoftKeyboard(activity.window.decorView)
    }

    fun hideSoftKeyboard(view: View) {
        val keyboard = view.context
                .getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        // Not using getCurrentFocus as that sometimes is null, but the keyboard is still up.
        keyboard?.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun showSoftKeyboard(view: View) {
        val keyboard = view.context
                .getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        keyboard?.toggleSoftInput(0, 0)
    }

    fun dpToPx(context: Context, dps: Int): Int {
        return Math.round(context.resources.displayMetrics.density * dps)
    }
}
