package org.rekhta.data.util

import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction

/**
 * Created by VipulKumar on 2/7/18.
 * helpful extension functions for presentation layer.
 */

/** Get children of a [ViewGroup] */
val ViewGroup.children: List<*>
    get() = (0 until childCount).map { getChildAt(it) }

/** Inflate a [ViewGroup] */
fun ViewGroup.inflate(layoutId: Int, attachToRoot: Boolean = false): View {
    return LayoutInflater.from(context).inflate(layoutId, this, attachToRoot)
}

/** Inflate a [ViewGroup] with DataBinding */
fun ViewGroup.inflateWithDataBinding(layoutId: Int, attachToRoot: Boolean = false): ViewDataBinding? {
    return DataBindingUtil
            .inflate(LayoutInflater.from(context),
                    layoutId, this, attachToRoot)
}

/** Apply [FragmentTransaction]s */
inline fun FragmentManager.inTransaction(func: FragmentTransaction.() -> FragmentTransaction) {
    beginTransaction().func().commit()
}

/** Check [Collection] for null or empty */
fun Collection<*>?.isNotNullOrEmpty(): Boolean {
    return this != null && this.isNotEmpty()
}

/** Check [Collection] for null or empty */
fun Collection<*>?.isNullOrEmpty(): Boolean {
    return this == null || this.isEmpty()
}
