package org.rekhta.data.util

import android.content.Context

/**
 * Authored by vipulkumar on 10/10/17.
 */

object ResourceUtil {
    fun getResourceByName(context: Context, resourceName: String): Int {
        return context.resources.getIdentifier(resourceName, "drawable", context.packageName)
    }
}
