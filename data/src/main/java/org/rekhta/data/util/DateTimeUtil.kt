package org.rekhta.data.util

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

/**
 * Authored by vipulkumar on 23/11/17.
 */

object DateTimeUtil {
    fun convertDateFormats(date: String, inputFormat: String, outputFormat: String): String {
        if (StringUtil.isEmpty(date)) {
            return ""
        }
        val inputDateFormat = SimpleDateFormat(inputFormat, Locale.getDefault())
        val outputDateFormat = SimpleDateFormat(outputFormat, Locale.getDefault())
        val parsed: Date
        try {
            parsed = inputDateFormat.parse(date)
        } catch (e: ParseException) {
            e.printStackTrace()
            return ""
        }

        return outputDateFormat.format(parsed)
    }
}
