package org.rekhta.data.util

import android.content.Context
import android.os.Handler
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils

/**
 * Authored by vipulkumar on 29/09/17.
 */

object AnimationUtil {
    fun animationIn(view: View?, animation: Int, delayTime: Int, context: Context) {
        val handler = Handler()
        handler.postDelayed({
            val inAnimation = AnimationUtils.loadAnimation(
                    context.applicationContext, animation)
            view?.animation = inAnimation
            view?.visibility = View.VISIBLE
        }, delayTime.toLong())
    }

    fun animationOut(view: View?, animation: Int, delayTime: Int,
                     isViewGone: Boolean, context: Context) {
        view?.visibility = View.VISIBLE
        val handler = Handler()
        handler.postDelayed({
            val outAnimation = AnimationUtils.loadAnimation(
                    context.applicationContext, animation)
            view?.animation = outAnimation
            if (isViewGone)
                view?.visibility = View.GONE
            else
                view?.visibility = View.INVISIBLE
        }, delayTime.toLong())
    }
}
