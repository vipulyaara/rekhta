package org.rekhta.data.extensions

import kotlin.reflect.KMutableProperty0

fun <T> updateProperty(entityVar: KMutableProperty0<T?>, updateVal: T?, updateAlways: Boolean = true) {
    if (updateVal != null && (updateAlways || entityVar.get() == null)) {
        entityVar.set(updateVal)
    }
}

fun updateProperty(entityVar: KMutableProperty0<String?>, updateVal: String?) {
    if (updateVal.isLongerThan(entityVar.get())) {
        entityVar.set(updateVal)
    }
}
