package org.rekhta.data.extensions

import kotlinx.coroutines.experimental.DefaultDispatcher
import kotlinx.coroutines.experimental.async
import kotlin.coroutines.experimental.CoroutineContext

suspend fun <A, B> Collection<A>.parallelMap(
    context: CoroutineContext = DefaultDispatcher,
    block: suspend (A) -> B
) = map {
    async(context) { block(it) }
}.map { it.await() }

suspend fun <A, B> Collection<A>.parallelForEach(
    context: CoroutineContext = DefaultDispatcher,
    block: suspend (A) -> B
) = map {
    async(context) { block(it) }
}.forEach { it.await() }
