package org.rekhta.data.data

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import com.cantrowitz.rxbroadcast.RxBroadcast
import io.reactivex.Observable
import io.reactivex.Single

class AndroidNetworkDetector constructor(private val context: Context) : NetworkDetector {
    @SuppressLint("NewApi")
    private val connectivityManager = context.getSystemService(ConnectivityManager::class.java)

    companion object {
        val intentFilter = IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
    }

    private fun getConnectivityStatus(): Boolean {
        return connectivityManager.activeNetworkInfo?.isConnected == true
    }

    override fun observe(): Observable<Boolean> {
        return RxBroadcast.fromBroadcast(context, intentFilter)
                .startWith(Intent())
                .map { getConnectivityStatus() }
                .distinctUntilChanged()
    }

    override fun waitForConnection(): Single<Boolean> {
        return observe()
                .filter { it }
                .firstOrError()
    }
}
