package org.rekhta.data.data

import android.content.Context
import android.content.SharedPreferences

/**
 * Authored by vipulkumar on 15/12/17.
 */

object SharedPrefUtil {
    public fun getSharedPrefs(context: Context): SharedPreferences {
        return context.getSharedPreferences("app_info", Context.MODE_PRIVATE)
    }

    public fun putBoolean(context: Context, key: String?, value: Boolean) {
        getSharedPrefs(context).edit().putBoolean(key, value).apply()
    }

    public fun putString(context: Context, key: String?, value: String?) {
        getSharedPrefs(context).edit().putString(key, value).apply()
    }

    public fun putInt(context: Context, key: String?, value: Int) {
        getSharedPrefs(context).edit().putInt(key, value).apply()
    }

    public fun getBoolean(context: Context, key: String?, defValue: Boolean): Boolean {
        return getSharedPrefs(context).getBoolean(key, defValue)
    }

    public fun getString(context: Context, key: String?, defValue: String?): String {
        return getSharedPrefs(context).getString(key, defValue)
    }

    public fun getInt(context: Context, key: String?, defValue: Int): Int {
        return getSharedPrefs(context).getInt(key, defValue)
    }
}
