package org.rekhta.data.data

/**
 * Authored by vipulkumar on 19/09/17.
 */

interface CommonConstants {
    companion object {
        val baseUrl = "http://world-staging.rekhta.org/api/v4/shayari/"
        val tempToken = "b1f6e9eb-afc7-4985-bb20-2e560de0f9fe"
        val targetId = "B16B3E22-7881-4FE3-B2BD-94D45CC47139"
    }
}
