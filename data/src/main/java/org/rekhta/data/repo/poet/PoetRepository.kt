package org.rekhta.data.repo.poet

import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import androidx.paging.PagedList
import org.rekhta.data.api.RekhtaService
import org.rekhta.data.db.PoetDao
import org.rekhta.data.kodeinInstance

import org.rekhta.data.livedata.NetworkBoundResource
import org.rekhta.data.model.ApiResponse
import org.rekhta.data.model.AppExecutors
import org.rekhta.data.model.Resource
import org.rekhta.data.model.poet.Poet
import org.rekhta.data.model.poet.PoetProfile
import org.rekhta.data.model.poet.PoetProfileResponseContainer
import org.rekhta.data.model.poet.PoetResponseContainer
import org.rekhta.data.repo.BaseRepository
import org.rekhta.data.repo.PaginatedResource
import org.rekhta.data.repo.pagination.Result
import org.rekhta.data.data.RateLimiter
import org.kodein.di.generic.instance
import retrofit2.Call
import java.util.concurrent.TimeUnit

/**
 * Poet repository to access data from REMOTE as well as Room.
 */

class PoetRepository : BaseRepository() {
    private val appExecutors: AppExecutors by kodeinInstance.instance()
    private val rekhtaService: RekhtaService by kodeinInstance.instance()
    private val poetDao: PoetDao by kodeinInstance.instance()
    val poetListRateLimiter = RateLimiter<String>(10, TimeUnit.MINUTES)

    fun loadPoets(targetId: String): Result<PagedList<Poet>> {
        return object : PaginatedResource<Poet, PoetResponseContainer>(appExecutors) {
            override fun shouldFetch(page: Int): Boolean {
                return true
            }

            override fun saveCallResult(entity: PoetResponseContainer?) {
                entity?.response?.poets?.forEachIndexed { index, poet ->
                    poet.targetId = targetId
                    val start = poetDao.getNextIndexInPoet(targetId)
                    poet.indexInResponse = (start + index)
                }
                poetDao.insertAll(entity?.response?.poets)
            }

            override fun loadFromDb(): DataSource.Factory<Int, Poet> {
                return poetDao.loadAllPoetsWithTargetId(targetId)
            }

            override fun createCall(page: Int): Call<PoetResponseContainer> {
                return rekhtaService
                        .getPoetsListWithPaging("", targetId, "", page)
            }
        }.asPagedResult()
    }

    fun loadPoetProfile(poetId: String): LiveData<Resource<PoetProfile>> {
        return object : NetworkBoundResource<PoetProfile,
                PoetProfileResponseContainer>(appExecutors) {
            override fun saveCallResult(entity: PoetProfileResponseContainer) {
                if (entity.response != null) {
                    poetDao.insertAll(entity.response!!)
                }
            }

            override fun shouldFetch(data: PoetProfile?): Boolean {
                return true
            }

            override fun loadFromDb(): LiveData<PoetProfile> {
                return poetDao.loadPoetDetail(poetId)
            }

            override fun createCall(): LiveData<ApiResponse<PoetProfileResponseContainer>> {
                return rekhtaService.getPoetProfile(poetId)
            }
        }.asLiveData()
    }
}
