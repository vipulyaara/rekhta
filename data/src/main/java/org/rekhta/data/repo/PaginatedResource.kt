package org.rekhta.data.repo

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.annotation.MainThread
import androidx.annotation.WorkerThread
import androidx.paging.DataSource
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import org.rekhta.data.model.AppExecutors
import org.rekhta.data.model.PagedModel
import org.rekhta.data.repo.pagination.GenericBoundaryCallback
import org.rekhta.data.repo.pagination.NetworkState
import org.rekhta.data.repo.pagination.PagingRequestHelper
import org.rekhta.data.repo.pagination.Result
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by VipulKumar on 04/09/18.
 * Generic resource to fetch paginated data.
 */

const val pageSize = 50

abstract class PaginatedResource<ResultType : PagedModel, RequestType>
constructor(private val appExecutors: AppExecutors) {

    private var boundaryCallback: GenericBoundaryCallback<ResultType, RequestType> = object :
            GenericBoundaryCallback<ResultType, RequestType>(
                    { body: RequestType -> this.saveCallResult(body) }, appExecutors) {
        override fun fetchItemsWithPaging(item: ResultType?,
                                          callback: PagingRequestHelper.Request.Callback) {
                var page = ((item?.indexInResponse ?: 0) / pageSize)
                page = if (page == 0) 1 else page
            if (shouldFetch(page)) {
                createCall(page).enqueue(createWebserviceCallback(callback))
            }
        }
    }

    fun asPagedResult(): Result<PagedList<ResultType>> {
        val dataSourceFactory = loadFromDb()
        val builder =
                LivePagedListBuilder<Int, ResultType>(dataSourceFactory, pageSize)
                        .setBoundaryCallback(boundaryCallback)

        val refreshTrigger = MutableLiveData<Void>()
        val refreshState = Transformations
                .switchMap(refreshTrigger) { refresh() }

        return Result(builder.build(), boundaryCallback.networkState,
                refreshState,
                { refreshTrigger.value = null },
                { boundaryCallback.helper.retryAllFailed() })
    }

    private fun refresh(): LiveData<NetworkState> {
        val networkState = MutableLiveData<NetworkState>()
        networkState.value = NetworkState.LOADING
        createCall(0 / pageSize).enqueue(
                object : Callback<RequestType> {
                    override fun onResponse(call: Call<RequestType>,
                                            response: Response<RequestType>) {
                        appExecutors.diskIO().execute {
                            saveCallResult(processResponse(response))
                            // since we are in bg thread now, post the result.
                            networkState.postValue(NetworkState.SUCCESS)
                        }
                    }

                    override fun onFailure(call: Call<RequestType>, t: Throwable) {
                        networkState.value = NetworkState.error(t.localizedMessage)
                    }
                })
        return networkState
    }

    @WorkerThread
    private fun processResponse(response: Response<RequestType>?): RequestType? {
        return response?.body()
    }

    @MainThread
    protected abstract fun shouldFetch(page: Int): Boolean

    @WorkerThread
    protected abstract fun saveCallResult(entity: RequestType?)

    @MainThread
    protected abstract fun loadFromDb(): DataSource.Factory<Int, ResultType>

    @MainThread
    abstract fun createCall(page: Int): Call<RequestType>

}
