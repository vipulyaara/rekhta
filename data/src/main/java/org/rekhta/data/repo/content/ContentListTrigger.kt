package org.rekhta.data.repo.content

data class ContentListTrigger(var poetId: String, var contentTypeId: String, var targetId: String)
