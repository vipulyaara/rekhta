package org.rekhta.data.repo.audio

import androidx.lifecycle.LiveData
import org.rekhta.data.api.RekhtaService
import org.rekhta.data.db.ContentDao
import org.rekhta.data.kodeinInstance
import org.rekhta.data.livedata.AbsentLiveData
import org.rekhta.data.livedata.NetworkBoundResource
import org.rekhta.data.model.ApiResponse
import org.rekhta.data.model.AppExecutors
import org.rekhta.data.model.Resource
import org.rekhta.data.model.audio.Audio
import org.rekhta.data.model.audio.AudioListContainer
import org.rekhta.data.repo.BaseRepository
import org.kodein.di.generic.instance

/**
 * Hotel repository to access data from REMOTE as well as Room.
 */

class AudioRepository : BaseRepository() {
    private val appExecutors: AppExecutors by kodeinInstance.instance()
    private val rekhtaService: RekhtaService by kodeinInstance.instance()
    private val contentDao: ContentDao by kodeinInstance.instance()

    fun loadAudioList(audioListTrigger: AudioListTrigger): LiveData<Resource<List<Audio>>> {
        return object : NetworkBoundResource<List<Audio>, AudioListContainer>(appExecutors) {
            override fun saveCallResult(entity: AudioListContainer) {
            }

            override fun shouldFetch(data: List<Audio>?): Boolean {
                return true
            }

            override fun loadFromDb(): LiveData<List<Audio>> {
                return AbsentLiveData.create()
            }

            override fun createCall(): LiveData<ApiResponse<AudioListContainer>> {
                return rekhtaService.getAudioList(audioListTrigger.poetId, audioListTrigger.lang, "")
            }

        }.asLiveData()
    }
}
