package org.rekhta.data.repo.pagination

import org.rekhta.data.model.Status

class NetworkState {
    var status: Status
    lateinit var msg: String

    constructor(status: Status) {
        this.status = status
    }

    constructor(status: Status, msg: String) {
        this.status = status
        this.msg = msg
    }

    companion object {
        var SUCCESS = NetworkState(Status.SUCCESS)
        var LOADING = NetworkState(Status.LOADING_MORE)
        fun error(msg: String): NetworkState {
            return NetworkState(Status.ERROR, msg)
        }
    }
}
