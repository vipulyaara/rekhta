package org.rekhta.data.repo.content

import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import androidx.paging.PagedList
import org.rekhta.data.api.RekhtaService
import org.rekhta.data.db.ContentDao
import org.rekhta.data.kodeinInstance
import org.rekhta.data.livedata.NetworkBoundResource
import org.rekhta.data.model.ApiResponse
import org.rekhta.data.model.AppExecutors
import org.rekhta.data.model.Resource
import org.rekhta.data.model.content.Content
import org.rekhta.data.model.content.ContentResponseContainer
import org.rekhta.data.model.contentDetail.ContentDetailResponse
import org.rekhta.data.model.contentDetail.ContentDetailResponseContainer
import org.rekhta.data.model.dictionary.WordMeaningResponse
import org.rekhta.data.model.dictionary.WordMeaningResponseContainer
import org.rekhta.data.repo.BaseRepository
import org.rekhta.data.repo.PaginatedResource
import org.rekhta.data.data.RateLimiter
import org.rekhta.data.repo.pagination.Result
import org.kodein.di.generic.instance
import retrofit2.Call
import java.util.concurrent.TimeUnit

/**
 * Content repository to access data from REMOTE as well as Room.
 */

class ContentRepository : BaseRepository() {
    private val appExecutors: AppExecutors by kodeinInstance.instance()
    private val rekhtaService: RekhtaService by kodeinInstance.instance()
    private val contentDao: ContentDao by kodeinInstance.instance()

    val contentDetailRateLimiter = RateLimiter<String>(10, TimeUnit.MINUTES)
    val meaningRateLimiter = RateLimiter<String>(30, TimeUnit.MINUTES)

    fun loadContent(contentListTrigger: ContentListTrigger): Result<PagedList<Content>> {
        return object : PaginatedResource<Content, ContentResponseContainer>(appExecutors) {

            override fun shouldFetch(page: Int): Boolean {
                return true
            }

            override fun saveCallResult(entity: ContentResponseContainer?) {
                entity?.contentResponse?.cs?.forEachIndexed { index, it ->
                    it.apply {
                        val start = contentDao.getNextIndex(contentListTrigger.targetId,
                                contentListTrigger.poetId, contentListTrigger.contentTypeId)
                        indexInResponse = (start + index)
                        targetId = contentListTrigger.targetId
                        poetId = contentListTrigger.poetId
                        contentTypeId = contentListTrigger.contentTypeId
                    }
                }
                entity?.contentResponse?.cs?.let {
                    contentDao.insertAll(it)
                }
            }

            override fun loadFromDb(): DataSource.Factory<Int, Content> {
                return contentDao.loadAllContent(contentListTrigger.targetId,
                        contentListTrigger.poetId, contentListTrigger.contentTypeId)
            }

            override fun createCall(page: Int): Call<ContentResponseContainer> {
                return rekhtaService.getContentListWithPaging(contentListTrigger.poetId,
                        contentListTrigger.contentTypeId,
                        contentListTrigger.targetId, "", page)
            }
        }.asPagedResult()
    }

    fun loadWordMeaning(wordId: String): LiveData<Resource<WordMeaningResponse>>? {
        return object : NetworkBoundResource<WordMeaningResponse,
                WordMeaningResponseContainer>(appExecutors) {
            override fun saveCallResult(entity: WordMeaningResponseContainer) {
                if (entity.wordMeaningResponse != null) {
                    val response = entity.wordMeaningResponse
                    response?.m = wordId
                    contentDao.insertWordMeaningResponse(response)
                }
            }

            override fun shouldFetch(data: WordMeaningResponse?): Boolean {
                return data == null || meaningRateLimiter.shouldFetch(wordId)
            }

            override fun loadFromDb(): LiveData<WordMeaningResponse> {
                return contentDao.loadWordMeaningResponse(wordId)
            }

            override fun createCall(): LiveData<ApiResponse<WordMeaningResponseContainer>> {
                return rekhtaService.getWordMeaning(wordId)
            }

        }.asLiveData()
    }

    fun loadContentDetail(contentTrigger: ContentDetailTrigger):
            LiveData<Resource<ContentDetailResponse>>? {
        return object : NetworkBoundResource<ContentDetailResponse,
                ContentDetailResponseContainer>(appExecutors) {
            override fun saveCallResult(entity: ContentDetailResponseContainer) {
                if (entity.contentDetailResponse != null) {
                    val response = entity.contentDetailResponse
                    response?.idWithLang = contentTrigger.contentId + contentTrigger.lang
                    contentDao.insertContentDetailResponse(response)
                }
            }

            override fun shouldFetch(data: ContentDetailResponse?): Boolean {
                return data == null || contentDetailRateLimiter
                        .shouldFetch(contentTrigger.toString())
            }

            override fun loadFromDb(): LiveData<ContentDetailResponse> {
                return contentDao.loadContentDetailResponse(
                        contentTrigger.contentId + contentTrigger.lang)
            }

            override fun createCall(): LiveData<ApiResponse<ContentDetailResponseContainer>> {
                return rekhtaService.getContentById(contentTrigger.lang, contentTrigger.contentId)
            }

        }.asLiveData()
    }
}
