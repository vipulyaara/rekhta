package org.rekhta.data.repo.home

import androidx.lifecycle.LiveData
import org.rekhta.data.api.RekhtaService
import org.rekhta.data.db.HomepageDao
import org.rekhta.data.kodeinInstance
import org.rekhta.data.livedata.NetworkBoundResource

import org.rekhta.data.model.ApiResponse
import org.rekhta.data.model.AppExecutors
import org.rekhta.data.model.Resource
import org.rekhta.data.model.homepage.HomepageResponse
import org.rekhta.data.model.homepage.HomepageResponseContainer
import org.rekhta.data.repo.BaseRepository
import org.kodein.di.generic.instance


/**
 * Hotel repository to access data from REMOTE as well as Room.
 */

class HomepageRepository : BaseRepository() {
    private val appExecutors: AppExecutors by kodeinInstance.instance()
    private val rekhtaService: RekhtaService by kodeinInstance.instance()
    private val homepageDao: HomepageDao by kodeinInstance.instance()

    fun loadHomepageResponse(trigger: HomeTrigger): LiveData<Resource<HomepageResponse>> {
        return object : NetworkBoundResource<HomepageResponse, HomepageResponseContainer>(appExecutors) {
            override fun saveCallResult(container: HomepageResponseContainer) {
                if (container.response != null) {
                    homepageDao.insertAll(container.response!!)
                }
            }

            override fun shouldFetch(data: HomepageResponse?): Boolean {
                return true
            }

            override fun loadFromDb(): LiveData<HomepageResponse> {
                return homepageDao.loadHomepageResponse()
            }

            override fun createCall(): LiveData<ApiResponse<HomepageResponseContainer>> {
                return rekhtaService.getHomePageCollection(trigger.languageCode, "4-6-2018")
            }
        }.asLiveData()
    }
}
