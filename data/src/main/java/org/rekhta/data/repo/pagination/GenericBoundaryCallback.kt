package org.rekhta.data.repo.pagination

import androidx.lifecycle.LiveData
import androidx.annotation.MainThread
import androidx.paging.PagedList
import org.rekhta.data.model.AppExecutors
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by VipulKumar on 17/08/18.
 * A generic implementation of [PagedList.BoundaryCallback].
 *
 */
abstract class GenericBoundaryCallback<ItemType, ResponseType>
protected constructor(private val handleResponse: Function1<ResponseType, Unit>,
                      private val appExecutors: AppExecutors) : PagedList.BoundaryCallback<ItemType>() {

    val helper: PagingRequestHelper = PagingRequestHelper(this.appExecutors.diskIO())
    val networkState: LiveData<NetworkState>

    init {
        this.networkState = this.helper.createStatusLiveData()
    }

    @MainThread
    override fun onZeroItemsLoaded() {
        this.helper.runIfNotRunning(PagingRequestHelper.RequestType.INITIAL
        ) { callback -> appExecutors.networkIO().execute { fetchItemsWithPaging(null, callback) } }
    }

    @MainThread
    override fun onItemAtEndLoaded(itemAtEnd: ItemType) {
        this.helper.runIfNotRunning(PagingRequestHelper.RequestType.AFTER
        ) { callback -> appExecutors.networkIO().execute { fetchItemsWithPaging(itemAtEnd, callback) } }
    }

    abstract fun fetchItemsWithPaging(item: ItemType?, callback: PagingRequestHelper.Request.Callback)

    private fun insertItemsIntoDb(response: Response<ResponseType>,
                                  it: PagingRequestHelper.Request.Callback) {
        this.appExecutors.diskIO().execute {
            response.body()?.let { body -> handleResponse.invoke(body) }
            it.recordSuccess()
        }
    }

    override fun onItemAtFrontLoaded(itemAtFront: ItemType) {}

    protected fun createWebserviceCallback(it: PagingRequestHelper.Request.Callback): Callback<ResponseType> {
        return object : Callback<ResponseType> {
            override fun onFailure(call: Call<ResponseType>, t: Throwable) {
                it.recordFailure(t)
            }

            override fun onResponse(call: Call<ResponseType>, response: Response<ResponseType>) {
                insertItemsIntoDb(response, it)
            }
        }
    }
}
