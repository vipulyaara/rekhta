package org.rekhta.data.repo.pagination

import androidx.lifecycle.LiveData
import org.rekhta.data.model.Resource

/**
 * Data class that is necessary for a UI to show a listing and interact w/ the rest of the system
 */
data class Result<T>(
        // the LiveData of T for the UI to observe
        val pagedList: LiveData<T>,
        // represents the network request status to show to the user
        val networkState: LiveData<NetworkState>,
        // represents the refresh status to show to the user. Separate from networkState, this
        // value is importantly only when refresh is requested.
        val refreshState: LiveData<NetworkState>,
        // refreshes the whole data and fetches it from scratch.
        val refresh: () -> Unit,
        // retries any failed requests.
        val retry: () -> Unit)
