package org.rekhta.data.repo.search

import androidx.lifecycle.LiveData
import org.rekhta.data.api.RekhtaService
import org.rekhta.data.db.SearchDao
import org.rekhta.data.kodeinInstance

import org.rekhta.data.livedata.NetworkBoundResource
import org.rekhta.data.model.ApiResponse
import org.rekhta.data.model.AppExecutors
import org.rekhta.data.model.Resource
import org.rekhta.data.model.search.SearchResponse
import org.rekhta.data.model.search.SearchTrigger
import org.rekhta.data.repo.BaseRepository
import org.kodein.di.generic.instance

/**
 * Hotel repository to access data from REMOTE as well as Room.
 */

class SearchRepository : BaseRepository() {
    private val appExecutors: AppExecutors by kodeinInstance.instance()
    private val rekhtaService: RekhtaService by kodeinInstance.instance()
    private val searchDao: SearchDao by kodeinInstance.instance()

    fun loadSearchResponse(trigger: SearchTrigger): LiveData<Resource<SearchResponse>> {
        return object : NetworkBoundResource<SearchResponse, SearchResponse>(appExecutors) {
            override fun saveCallResult(entity: SearchResponse) {
                searchDao.clearSearchResponse()
                searchDao.insertSearchResponse(entity)
            }

            override fun shouldFetch(data: SearchResponse?): Boolean {
                return true
            }

            override fun loadFromDb(): LiveData<SearchResponse> {
                return searchDao.loadSearchResponse()
            }

            override fun createCall(): LiveData<ApiResponse<SearchResponse>> {
                return rekhtaService.getSearchResponse(trigger.languageCode, trigger.keyword)
            }
        }.asLiveData()
    }
}
