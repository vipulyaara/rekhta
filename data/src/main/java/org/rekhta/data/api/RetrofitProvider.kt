package org.rekhta.data.api

import org.rekhta.data.Environment
import org.rekhta.data.livedata.LiveDataCallAdapterFactory
import org.rekhta.data.data.CommonConstants

import java.util.concurrent.TimeUnit

import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Authored by vipulkumar on 21/09/17.
 */

object RetrofitProvider {

    private val headerInterceptor: Interceptor
        get() = Interceptor { chain ->
            val original = chain.request()

            val request = original.newBuilder()
                    .header("Content-Type", "application/json")
                    .header("tempToken", CommonConstants.tempToken)
                    .method(original.method(), original.body())
                    .build()

            chain.proceed(request)
        }

    fun provideDefaultRetrofit(): Retrofit {
        return Retrofit.Builder()
                .baseUrl(CommonConstants.baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(LiveDataCallAdapterFactory())
                .client(provideOkHttpClient())
                .build()
    }

    fun provideDefaultRetrofit(baseUrl: String): Retrofit {
        return Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(LiveDataCallAdapterFactory())
                .client(provideOkHttpClient())
                .build()
    }

    private fun provideOkHttpClient(): OkHttpClient {
        val builder = OkHttpClient.Builder()
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
        if (Environment.showNetworkLogs) {
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY
            builder.addInterceptor(interceptor)
        }
        builder.addInterceptor(headerInterceptor)
        return builder.build()
    }
}
