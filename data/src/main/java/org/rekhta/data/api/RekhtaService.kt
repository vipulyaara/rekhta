package org.rekhta.data.api

import androidx.lifecycle.LiveData
import org.rekhta.data.model.ApiResponse
import org.rekhta.data.model.audio.AudioListContainer
import org.rekhta.data.model.content.ContentResponseContainer
import org.rekhta.data.model.contentDetail.ContentDetailResponseContainer
import org.rekhta.data.model.dictionary.WordMeaningResponseContainer
import org.rekhta.data.model.homepage.HomepageResponseContainer
import org.rekhta.data.model.poet.PoetProfileResponseContainer
import org.rekhta.data.model.poet.PoetResponseContainer
import org.rekhta.data.model.search.SearchResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

/**
 * Interface with all the methods to interact with API.
 */

interface RekhtaService {
    @POST("GetPoetsListWithPaging")
    fun getPoetsListWithPaging(
            @Query("lastFetchDate") lastFetchDate: String?,
            @Query("targetId") targetId: String?,
            @Query("keyword") keyword: String?, @Query("pageIndex") pageIndex: Int): Call<PoetResponseContainer>

    @POST("GetContentListWithPaging")
    fun getContentListWithPaging(
            @Query("poetId") poetId: String?,
            @Query("contentTypeId") contentTypeId: String,
            @Query("targetId") targetId: String?,
            @Query("keyword") keyword: String?, @Query("pageIndex") pageIndex: Int):
            Call<ContentResponseContainer>

    @GET("GetContentById")
    fun getContentById(
            @Query("lang") poetId: Int,
            @Query("contentId") contentId: String?): LiveData<ApiResponse<ContentDetailResponseContainer>>

    @POST("GetPoetProfile")
    fun getPoetProfile(@Query("poetId") poetId: String?): LiveData<ApiResponse<PoetProfileResponseContainer>>

    @POST("GetHomePageCollection")
    fun getHomePageCollection(@Query("lang") poetId: Int, @Query("lastFetchDate") lastFetchDate: String?): LiveData<ApiResponse<HomepageResponseContainer>>

    @POST("SearchAll")
    fun getSearchResponse(@Query("lang") lang: Int,
                          @Query("keyword") keyword: String?): LiveData<ApiResponse<SearchResponse>>

    @POST("GetAudioListByPoetIdWithPaging")
    fun getAudioList(@Query("poetId") poetId: String?, @Query("pageIndex") lang: Int,
                     @Query("keyword") keyword: String?): LiveData<ApiResponse<AudioListContainer>>

    @POST("GetWordMeaning")
    fun getWordMeaning(@Query("word") keyword: String?): LiveData<ApiResponse<WordMeaningResponseContainer>>
}
