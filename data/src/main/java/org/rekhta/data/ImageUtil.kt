package org.rekhta.data

import androidx.fragment.app.Fragment
import org.rekhta.data.model.tab.*

/**
 * Authored by vipulkumar on 13/12/17.
 */

object ImageUtil {
    private const val URL_IMAGE_REKHTA_CDN = "https://rekhtacdn.azureedge.net"
    private const val URL_IMAGE_SHAYAR_CDN = "/Images/Shayar/"
    private const val URL_IMAGE_CARD_CDN = "/Images/Cms/Cards/"

    fun buildImageUrl(imageUrl: String): String {
        return when {
            imageUrl.contains(URL_IMAGE_SHAYAR_CDN) -> URL_IMAGE_REKHTA_CDN + imageUrl
            else -> "$URL_IMAGE_REKHTA_CDN$URL_IMAGE_SHAYAR_CDN$imageUrl.png"
        }
    }

    fun buildCardImageUrl(imageUrl: String): String {
        return "$URL_IMAGE_REKHTA_CDN$URL_IMAGE_CARD_CDN$imageUrl.jpg"
    }

    fun buildVideoUrl(imageUrl: String): String {
        return URL_IMAGE_REKHTA_CDN + imageUrl
    }

}
