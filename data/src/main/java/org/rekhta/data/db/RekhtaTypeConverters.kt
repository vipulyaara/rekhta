package org.rekhta.data.db

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import org.rekhta.data.model.homepage.*
import org.rekhta.data.model.search.Dictionary
import org.rekhta.data.model.search.Poet
import java.util.*

/**
 * Type converters to be used for database entities.
 */

class RekhtaTypeConverters {
    /* Ignore warning about public modifiers. TypeConverters need to be public.*/

    @TypeConverter
    fun stringToStringList(data: String): List<String>? {
        val listType = object : TypeToken<ArrayList<String>>() {

        }.type
        return Gson().fromJson<List<String>>(data, listType)
    }

    @TypeConverter
    fun stringListToString(stringList: List<String?>?): String {
        val gson = Gson()
        return gson.toJson(stringList)
    }

    @TypeConverter
    fun stringToWord(data: String): WordOfTheDay? {
        val listType = object : TypeToken<WordOfTheDay?>() {

        }.type
        return Gson().fromJson<WordOfTheDay>(data, listType)
    }

    @TypeConverter
    fun wordToString(stringList: WordOfTheDay?): String {
        val gson = Gson()
        return gson.toJson(stringList)
    }

    class SearchPoetListTypeConverter {
        @TypeConverter
        fun stringToList(data: String): List<Poet>? {
            val listType = object : TypeToken<ArrayList<Poet>>() {
            }.type
            return Gson().fromJson<List<Poet>>(data, listType)
        }

        @TypeConverter
        fun listToString(stringList: List<Poet>): String {
            val gson = Gson()
            return gson.toJson(stringList)
        }
    }

    class FeaturedListTypeConverter {
        @TypeConverter
        fun stringToList(data: String): List<Featured>? {
            val listType = object : TypeToken<ArrayList<Featured>>() {
            }.type
            return Gson().fromJson<List<Featured>>(data, listType)
        }

        @TypeConverter
        fun listToString(stringList: List<Featured>): String {
            val gson = Gson()
            return gson.toJson(stringList)
        }
    }

    class CardListTypeConverter {
        @TypeConverter
        fun stringToList(data: String): List<Card>? {
            val listType = object : TypeToken<ArrayList<Card>?>() {
            }.type
            return Gson().fromJson(data, listType)
        }

        @TypeConverter
        fun listToString(stringList: List<Card>?): String {
            val gson = Gson()
            return gson.toJson(stringList)
        }
    }

    class DictionaryPoetListTypeConverter {
        @TypeConverter
        fun stringToList(data: String): List<Dictionary>? {
            val listType = object : TypeToken<ArrayList<Dictionary>>() {
            }.type
            return Gson().fromJson<List<Dictionary>>(data, listType)
        }

        @TypeConverter
        fun listToString(stringList: List<Dictionary>): String {
            val gson = Gson()
            return gson.toJson(stringList)
        }
    }

    class VideoTypeConverter {
        @TypeConverter
        fun stringToList(data: String): Video? {
            val listType = object : TypeToken<Video>() {
            }.type
            return Gson().fromJson<Video>(data, listType)
        }

        @TypeConverter
        fun listToString(stringList: Video?): String {
            val gson = Gson()
            return gson.toJson(stringList)
        }
    }

    class ContentDetailPoetTypeConverter {
        @TypeConverter
        fun stringToList(data: String): org.rekhta.data.model.contentDetail.Poet? {
            return Gson().fromJson<org.rekhta.data.model.contentDetail.Poet>(data, org.rekhta.data.model.contentDetail.Poet::class.java)
        }

        @TypeConverter
        fun listToString(stringList: org.rekhta.data.model.contentDetail.Poet): String {
            val gson = Gson()
            return gson.toJson(stringList)
        }
    }


    class ImageShayriListTypeConverter {
        @TypeConverter
        fun stringToList(data: String): List<ImgShayari>? {
            val listType = object : TypeToken<ArrayList<ImgShayari>?>() {
            }.type
            return Gson().fromJson(data, listType)
        }

        @TypeConverter
        fun listToString(stringList: List<ImgShayari>?): String {
            val gson = Gson()
            return gson.toJson(stringList)
        }
    }
}
