package org.rekhta.data.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import org.rekhta.data.model.homepage.HomepageResponse

/**
 * Interface to access database for Hotel related operations.
 */
@Dao
interface HomepageDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(homepageResponse: HomepageResponse)

    @Query("select * from homepageResponse")
    fun loadHomepageResponse() : LiveData<HomepageResponse>

    @Query("delete from homepageResponse")
    fun clearHomepageResponse()
}
