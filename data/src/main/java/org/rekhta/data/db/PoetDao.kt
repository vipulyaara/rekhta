package org.rekhta.data.db

import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import org.rekhta.data.model.poet.Poet
import org.rekhta.data.model.poet.PoetProfile

/**
 * Interface to access database for Hotel related operations.
 */
@Dao
interface PoetDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(poets: List<Poet>?)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(poetProfile: PoetProfile)

    @Query("select * from poet")
    fun loadAllPoets() : LiveData<List<Poet>>

    @Query("select * from poet where targetId = :targetId ORDER BY indexInResponse")
    fun loadAllPoetsWithTargetId(targetId: String) : DataSource.Factory<Int, Poet>

    @Query("delete from poet")
    fun clearAllPoets()

    @Query("select * from poetProfile where poetId = :id")
    fun loadPoetDetail(id: String) : LiveData<PoetProfile>

    @Query("SELECT MAX(indexInResponse) + 1 FROM Poet where targetId = :targetId ORDER BY indexInResponse")
    fun getNextIndexInPoet(targetId: String): Int
}
