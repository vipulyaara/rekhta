package org.rekhta.data.db


import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import org.rekhta.data.model.content.Content
import org.rekhta.data.model.contentDetail.ContentDetailResponse
import org.rekhta.data.model.dictionary.WordMeaningResponse
import org.rekhta.data.model.homepage.HomepageResponse
import org.rekhta.data.model.poet.Poet
import org.rekhta.data.model.poet.PoetProfile
import org.rekhta.data.model.search.SearchResponse

/**
 * Hotel database description.
 */
@Database(entities = arrayOf(Poet::class,
        PoetProfile::class, Content::class,
        SearchResponse::class, ContentDetailResponse::class,
        HomepageResponse::class, WordMeaningResponse::class), version = 1)
@TypeConverters(RekhtaTypeConverters::class)
abstract class RekhtaDb : RoomDatabase() {

    abstract fun poetDao(): PoetDao
    abstract fun contentDao(): ContentDao
    abstract fun searchDao(): SearchDao
    abstract fun homepageDao(): HomepageDao
}
