package org.rekhta.data.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import org.rekhta.data.model.search.SearchResponse

/**
 * Interface to access database for Hotel related operations.
 */
@Dao
interface SearchDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertSearchResponse(contents: SearchResponse)

    @Query("delete from searchResponse")
    fun clearSearchResponse()

    @Query("select * from searchResponse")
    fun loadSearchResponse(): LiveData<SearchResponse>
}
