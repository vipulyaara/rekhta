package org.rekhta.data.db

import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import org.rekhta.data.model.content.Content
import org.rekhta.data.model.contentDetail.ContentDetailResponse
import org.rekhta.data.model.dictionary.WordMeaningResponse

/**
 * Interface to access database for Hotel related operations.
 */
@Dao
interface ContentDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(contents: List<Content>?)

    @Query("select * from content")
    fun loadAllContent(): LiveData<List<Content>>

    @Query("select * from content where targetId = :targetId AND poetId = :poetId AND contentTypeId = :contentTypeId ORDER BY indexInResponse")
    fun loadAllContent(targetId: String?, poetId: String?, contentTypeId: String?):
            DataSource.Factory<Int, Content>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertContentDetailResponse(contentDetail: ContentDetailResponse?)

    @Query("select * from contentDetailResponse where idWithLang = :id")
    fun loadContentDetailResponse(id: String): LiveData<ContentDetailResponse>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertWordMeaningResponse(wordMeaningResponse: WordMeaningResponse?)

    @Query("select * from wordMeaningResponse where m = :id")
    fun loadWordMeaningResponse(id: String): LiveData<WordMeaningResponse>

    @Query("SELECT MAX(indexInResponse) + 1 FROM Content where targetId = :targetId AND poetId = :poetId AND contentTypeId = :contentTypeId ")
    fun getNextIndex(targetId: String?, poetId: String?, contentTypeId: String?): Int
}
