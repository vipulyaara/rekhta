package org.rekhta.user.data.di

import androidx.room.Room
import org.rekhta.data.model.AppExecutors
import org.rekhta.data.api.RetrofitProvider
import org.rekhta.data.data.AndroidNetworkDetector
import org.rekhta.data.util.CommonConstants
import org.rekhta.data.util.Logger
import org.rekhta.data.util.TimberLogger
import org.kodein.di.Kodein
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton
import org.rekhta.data.api.RekhtaService
import org.rekhta.data.db.*
import org.rekhta.data.repo.audio.AudioRepository
import org.rekhta.data.repo.content.ContentRepository
import org.rekhta.data.repo.home.HomepageRepository
import org.rekhta.data.repo.poet.PoetRepository
import org.rekhta.data.repo.search.SearchRepository
import org.rekhta.user.config.initializers.AppInitializer
import org.rekhta.user.config.initializers.AppInitializers
import org.rekhta.user.config.initializers.StethoInitializer
import org.rekhta.user.config.log.TimberInitializer

/**
 * DI module that provides objects which will live during the application lifecycle.
 */

val appModule = Kodein.Module("AppModule") {
    bind<Logger>() with singleton {
        TimberLogger()
    }

    bind<AndroidNetworkDetector>() with singleton {
        AndroidNetworkDetector(instance())
    }

    bind<AppExecutors>() with singleton {
        AppExecutors()
    }

    bind<AudioRepository>() with singleton {
        AudioRepository()
    }

    bind<ContentRepository>() with singleton {
        ContentRepository()
    }

    bind<HomepageRepository>() with singleton {
        HomepageRepository()
    }

    bind<PoetRepository>() with singleton {
        PoetRepository()
    }

    bind<SearchRepository>() with singleton {
        SearchRepository()
    }

    bind<RekhtaService>() with singleton {
        RetrofitProvider
                .provideDefaultRetrofit(CommonConstants.baseUrl)
                .create(RekhtaService::class.java)
    }

    bind<RekhtaDb>() with singleton {
        Room.databaseBuilder(instance(), RekhtaDb::class.java, "rekhta.db").build()
    }

    bind<PoetDao>() with singleton {
        instance<RekhtaDb>().poetDao()
    }

    bind<ContentDao>() with singleton {
        instance<RekhtaDb>().contentDao()
    }

    bind<SearchDao>() with singleton {
        instance<RekhtaDb>().searchDao()
    }

    bind<HomepageDao>() with singleton {
        instance<RekhtaDb>().homepageDao()
    }

    bind<AppInitializer>("TimberInitializer") with singleton {
        TimberInitializer(instance())
    }

    bind<AppInitializer>("StethoInitializer") with singleton {
        StethoInitializer()
    }

    bind<AppInitializers>() with singleton {
        AppInitializers()
    }
}
