package org.rekhta.user.util

import android.content.Context
import android.text.TextUtils
import android.widget.ImageView

import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import org.rekhta.data.util.StringUtil
import org.rekhta.user.R

/**
 * Authored by vipulkumar on 27/09/17.
 */

object ImageLoader {
    fun loadImage(context: Context, url: String, imageView: ImageView) {
        if (!TextUtils.isEmpty(url)) {
            Glide.with(context)
                    .load(StringUtil.removeSpacesFromUrl(url))
                    .apply(RequestOptions().placeholder(R.color.icon_tint_light))
                    .into(imageView)
        }
    }

    fun loadImage(context: Context, url: String, imageView: ImageView, placeholder: Int) {
        if (!TextUtils.isEmpty(url)) {
            Glide.with(context)
                    .load(StringUtil.removeSpacesFromUrl(url))
                    //                    .placeholder(placeholder)
                    .into(imageView)
        }
    }
}
