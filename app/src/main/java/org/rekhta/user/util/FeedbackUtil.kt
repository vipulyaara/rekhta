package org.rekhta.user.util

import android.content.Context
import org.rekhta.data.Environment
import org.rekhta.user.ui.widget.SaneToast

/**
 * Authored by vipulkumar on 23/09/17.
 */

object FeedbackUtil {
    fun showMessage(context: Context?, message: String) {
        if (context != null) {
            if (Environment.showSnackbarAsDefaultFeedback) {
                // TODO: Show Snackbar
            } else {
                SaneToast.getToast(context, message).show()
            }
        }
    }
}
