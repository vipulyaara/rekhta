package org.rekhta.user.util

import android.graphics.Paint
import android.view.View
import android.widget.TextView

import com.like.LikeButton
import org.rekhta.user.ui.widget.RekhtaSearchView

/**
 * Authored by vipulkumar on 14/12/17.
 */

object WidgetUtil {
    fun setLike(view: View) {
        if (view is LikeButton) {
            view.isLiked = true

            val searchView: RekhtaSearchView
        }
    }

    fun getPositionOnScreenX(view: View): Int {
        val location = IntArray(2)
        view.getLocationOnScreen(location)
        return location[0]
    }

    fun getPositionOnScreenY(view: View): Int {
        val location = IntArray(2)
        view.getLocationOnScreen(location)
        return location[1]
    }

    fun enableStrikeOffToTextView(textView: TextView) {
        textView.paintFlags = textView.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
    }
}
