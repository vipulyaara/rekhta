package org.rekhta.user.util

import androidx.fragment.app.Fragment
import org.rekhta.data.model.tab.*
import org.rekhta.user.feature.content.ContentListFragment
import org.rekhta.user.feature.poet.PoetListFragment
import org.rekhta.user.feature.profile.PoetProfileDescFragment

/**
 * Created by VipulKumar on 09/09/18.
 */
object DataUtil {

    fun prepareTabParcelableForPoetList(): TabParcelable {
        val tabParcelable = TabParcelable()
        tabParcelable.tabId = BaseTabData.TAB_POET_LIST
        return tabParcelable
    }

    fun prepareTabParcelableForGhazals(): TabParcelable {
        val tabParcelable = TabParcelable()
        tabParcelable.tabId = BaseTabData.TAB_GHAZALS
        return tabParcelable
    }

    fun prepareTabParcelableForNazms(): TabParcelable {
        val tabParcelable = TabParcelable()
        tabParcelable.tabId = BaseTabData.TAB_NAZMS
        return tabParcelable
    }

    fun prepareTabParcelableForProfile(
        poetId: String,
        ghazalCount: Int,
        nazmCount: Int
    ): TabParcelable {
        val tabParcelable = TabParcelable()
        tabParcelable.tabId = BaseTabData.TAB_PROFILE
        tabParcelable.currentPage = 1
        tabParcelable.poetId = poetId
        tabParcelable.ghazalCount = ghazalCount
        tabParcelable.nazmCount = nazmCount
        return tabParcelable
    }

    fun preparePoetsTabData(): BaseTabData {
        val tabData = BaseTabData()
        val titles: ArrayList<String?> = ArrayList()
        val fragments: ArrayList<Fragment?> = ArrayList()
        PoetTabData.values().forEach { titles.add(it.value) }
        PoetTabData.values().forEach { fragments.add(PoetListFragment.newInstance(it.targetId)) }

        tabData.titles = titles
        tabData.fragments = fragments
        return tabData
    }

    fun prepareProfileTabData(poetId: String, ghazalCount: Int, nazmCount: Int): BaseTabData {
        val tabData = BaseTabData()
        val titles: ArrayList<String?> = ArrayList()
        val fragments: ArrayList<Fragment?> = ArrayList()

        titles.add(ProfileTabData.PROFILE.value)
        fragments.add(PoetProfileDescFragment.newInstance())

        if (ghazalCount != 0) {
            titles.add(ProfileTabData.GHAZAL.value)
            fragments.add(ContentListFragment
                    .newInstance(poetId, ProfileTabData.GHAZAL.contentTypeId, ""))
        }

        if (nazmCount != 0) {
            titles.add(ProfileTabData.NAZM.value)
            fragments.add(ContentListFragment
                    .newInstance(poetId, ProfileTabData.NAZM.contentTypeId, ""))
        }

        titles.add(ProfileTabData.SHER.value)
        fragments.add(ContentListFragment
                .newInstance(poetId, ProfileTabData.SHER.contentTypeId, ""))
//        fragments.add(AudioListFragment
//                .newInstance(poetId))

        tabData.titles = titles
        tabData.fragments = fragments
        return tabData
    }

    fun prepareGhazalTabData(): BaseTabData {
        val tabData = BaseTabData()
        val titles: ArrayList<String?> = ArrayList()
        val fragments: ArrayList<Fragment?> = ArrayList()
        GhazalTabData.values().forEach { titles.add(it.value) }
        GhazalTabData.values().forEach {
            fragments.add(ContentListFragment
                    .newInstance("", BaseTabData.CONTENT_TYPE_ID_GHAZALS, it.targetId))
        }

        tabData.titles = titles
        tabData.fragments = fragments
        return tabData
    }

    fun prepareNazmTabData(): BaseTabData {
        val tabData = BaseTabData()
        val titles: ArrayList<String?> = ArrayList()
        val fragments: ArrayList<Fragment?> = ArrayList()
        NazmTabData.values().forEach { titles.add(it.value) }
        NazmTabData.values().forEach {
            fragments.add(ContentListFragment
                    .newInstance("", BaseTabData.CONTENT_TYPE_ID_NAZMS, it.targetId))
        }

        tabData.titles = titles
        tabData.fragments = fragments
        return tabData
    }
}
