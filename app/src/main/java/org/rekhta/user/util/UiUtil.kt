package org.rekhta.user.util

import androidx.core.widget.NestedScrollView
import androidx.appcompat.widget.Toolbar
import android.widget.TextView

/**
 * Authored by vipulkumar on 27/09/17.
 */

object UiUtil {
    fun showTitleOnScroll(toolbar: Toolbar, nestedScrollView: NestedScrollView, titleView: TextView) {
        val title = titleView.text.toString()
        nestedScrollView.setOnScrollChangeListener(NestedScrollView.OnScrollChangeListener { v, scrollX, scrollY, oldScrollX, oldScrollY ->
            /**
             * Called when the scroll position of a view changes.
             *
             * @param v The view whose scroll position has changed.
             * @param scrollX Current horizontal scroll origin.
             * @param scrollY Current vertical scroll origin.
             * @param oldScrollX Previous horizontal scroll origin.
             * @param oldScrollY Previous vertical scroll origin.
             */
            if (scrollY > oldScrollY) {
            }
        })
    }
}
