package org.rekhta.user.util

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.app.Activity
import android.content.Context
import android.os.Build
import androidx.annotation.RequiresApi
import android.transition.Transition
import android.transition.TransitionInflater
import android.view.View
import android.view.ViewTreeObserver
import org.rekhta.data.util.DeviceUtil
import org.rekhta.user.R

/**
 * Authored by vipulkumar on 27/09/17.
 */

object TransitionUtil {
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    fun setupSharedEnterTransition(activity: Activity) {
        val transition = TransitionInflater.from(activity)
                .inflateTransition(R.transition.change_bounds_with_arc)
        transition.duration = 250
        activity.window.sharedElementEnterTransition = transition
        transition.addListener(object : Transition.TransitionListener {
            override fun onTransitionStart(transition: Transition) {
            }

            override fun onTransitionEnd(transition: Transition) {}

            override fun onTransitionCancel(transition: Transition) {
            }

            override fun onTransitionPause(transition: Transition) {
            }

            override fun onTransitionResume(transition: Transition) {
            }
        })
    }

    fun initPostponedSharedElementTransition(activity: Activity, view: View) {
        view.viewTreeObserver
                .addOnPreDrawListener(object : ViewTreeObserver.OnPreDrawListener {
                    override fun onPreDraw(): Boolean {
                        view.viewTreeObserver.removeOnPreDrawListener(this)
                        if (DeviceUtil.isPostLollipop) {
                            activity.startPostponedEnterTransition()
                        }
                        return true
                    }
                })
    }

    fun revealFragmentWithCircularReveal(
        context: Context,
        actionView: View,
        revealView: View,
        isShown: Boolean,
        animatorListenerAdapter: AnimatorListenerAdapter
    ) {
        val cx = 930
        //                    actionView.getLeft() + 84;
        val cy = 1610
        //                    actionView.getBottom() - 84;
        val radius = Math.max(revealView.width, revealView.height) + 400
        if (!isShown) {
            val anim: Animator
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                anim = android.view.ViewAnimationUtils.createCircularReveal(revealView, cx, cy, 0f, radius.toFloat())
                anim.addListener(animatorListenerAdapter)
                //                    anim.setDuration(320);
                anim.start()
            }
            revealView.visibility = View.VISIBLE
        } else {
            val anim: Animator
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                anim = android.view.ViewAnimationUtils.createCircularReveal(revealView, cx, cy, radius.toFloat(), 0f)
                anim.addListener(animatorListenerAdapter)
                //                    anim.setDuration(320);
                anim.start()
            } else {
                revealView.visibility = View.GONE
            }
        }
    }
}
