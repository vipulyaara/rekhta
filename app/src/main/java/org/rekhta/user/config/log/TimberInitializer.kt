package org.rekhta.user.config.log

import android.app.Application
import org.rekhta.data.util.Logger
import org.rekhta.data.util.TimberLogger
import org.rekhta.user.BuildConfig
import org.rekhta.user.config.initializers.AppInitializer

class TimberInitializer constructor(private val timberLogger: Logger) : AppInitializer {
    override fun init(application: Application) {
        if (timberLogger is TimberLogger) {
            timberLogger.setup(BuildConfig.DEBUG)
        }
    }
}
