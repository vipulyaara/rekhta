package org.rekhta.user.config.initializers

import android.app.Application
import com.facebook.stetho.Stetho
import org.rekhta.user.config.initializers.AppInitializer

class StethoInitializer : AppInitializer {
    override fun init(application: Application) =
            Stetho.initializeWithDefaults(application)
}
