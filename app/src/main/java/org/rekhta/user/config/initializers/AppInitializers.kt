package org.rekhta.user.config.initializers

import android.app.Application
import org.kodein.di.generic.instance
import org.rekhta.data.kodeinInstance

class AppInitializers {
    private val initializers: MutableSet<@JvmSuppressWildcards AppInitializer> = hashSetOf()

    init {
        val timber: AppInitializer by kodeinInstance.instance("TimberInitializer")
        val stetho: AppInitializer by kodeinInstance.instance("StethoInitializer")
        initializers.add(timber)
        initializers.add(stetho)
    }

    fun init(application: Application) {
        initializers.forEach {
            it.init(application)
        }
    }
}
