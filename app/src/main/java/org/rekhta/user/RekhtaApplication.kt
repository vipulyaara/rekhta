package org.rekhta.user

import android.app.Application
import androidx.appcompat.app.AppCompatDelegate
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.androidModule
import org.kodein.di.generic.instance
import org.rekhta.data.kodeinInstance
import org.rekhta.user.config.initializers.AppInitializers
import org.rekhta.user.data.di.appModule

/**
 * Authored by vipulkumar on 18/09/17.
 */

class RekhtaApplication : Application(), KodeinAware {
    override val kodein = Kodein.lazy {
        import(androidModule(this@RekhtaApplication))
        import(appModule)
    }

    // initializes Timber, Stetho, FrameMetrics
//    private val initializers: AppInitializers by kodein.instance()

    override fun onCreate() {
        super.onCreate()
        kodeinInstance = kodein
//        initializers.init(this)
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
    }
}
