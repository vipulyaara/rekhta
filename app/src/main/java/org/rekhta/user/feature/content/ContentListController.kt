package org.rekhta.user.feature.content

import androidx.paging.PagedList
import com.airbnb.epoxy.paging.PagingEpoxyController
import com.like.LikeButton
import com.like.OnLikeListener
import org.rekhta.user.ItemContentBindingModel_
import org.rekhta.user.ItemContentNazmBindingModel_
import org.rekhta.user.ItemLoaderBindingModel_
import org.rekhta.data.model.content.Content
import org.rekhta.data.model.tab.BaseTabData
import org.rekhta.data.repo.pageSize
import java.util.*

/**
 * Epoxy needs a controller to build and add models to it.
 * See https://github.com/airbnb/epoxy for more info.
 */
class ContentListController(
    var contentListViewModel: ContentListViewModel,
    var contentClickListener: ContentClickListener
) :
        PagingEpoxyController<Content>() {

    override fun buildModels(contentList: List<Content>) {
        if (contentList != null && contentList.isNotEmpty()) {
            for (content in contentList) {
                val likeListener = object : OnLikeListener {
                    override fun liked(likeButton: LikeButton?) {
                        if (likeButton != null) {
                            contentClickListener.onLikeClicked(content, likeButton, true)
                        }
                    }

                    override fun unLiked(likeButton: LikeButton?) {
                        if (likeButton != null) {
                            contentClickListener.onLikeClicked(content, likeButton, false)
                        }
                    }
                }

                if (content != null) {
                    if (content.contentTypeId == BaseTabData.CONTENT_TYPE_ID_GHAZALS) {
                        ItemContentBindingModel_()
                                .id(content.i + content.getPoetName())
                                .content(content)
                                .clickListener { model, parentView, clickedView, position ->
                                    contentClickListener.onContentItemClicked(content)
                                }
                                .viewModel(contentListViewModel)
                                .onLikeListener(likeListener)
                                .addTo(this)
                    } else if (content.contentTypeId == BaseTabData.CONTENT_TYPE_ID_NAZMS) {
                        ItemContentNazmBindingModel_()
                                .id(content.i + content.getPoetName())
                                .content(content)
                                .clickListener { model, parentView, clickedView, position ->
                                    contentClickListener.onContentItemClicked(content)
                                }
                                .viewModel(contentListViewModel)
                                .onLikeListener(likeListener)
                                .addTo(this)
                    }
                }
            }
            if (contentList.size == pageSize) {
                ItemLoaderBindingModel_()
                        .id(UUID.randomUUID().toString())
                        .addTo(this)
            }
        }
    }

    fun replaceData(poets: PagedList<Content>?) {
        setList(poets)
    }

    interface ContentClickListener {
        fun onContentItemClicked(content: Content)
        fun onLikeClicked(content: Content, clickedView: LikeButton, isLiked: Boolean)
    }
}
