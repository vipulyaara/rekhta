package org.rekhta.user.feature.audio

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import org.rekhta.data.livedata.AbsentLiveData
import org.rekhta.data.model.Resource
import org.kodein.di.generic.instance
import org.rekhta.data.kodeinInstance
import org.rekhta.data.model.audio.Audio
import org.rekhta.data.repo.audio.AudioListTrigger
import org.rekhta.data.repo.audio.AudioRepository
import org.rekhta.user.feature.common.BaseViewModel

/**
 * Authored by vipulkumar on 13/12/17.
 */

class AudioViewModel : BaseViewModel() {
    private val audioRepository: AudioRepository by kodeinInstance.instance()
    private val contentListTrigger = MutableLiveData<AudioListTrigger>()
    val audioList: LiveData<Resource<List<Audio>>>

    init {
        audioList = Transformations.switchMap(contentListTrigger) { contentTrigger ->
            if (contentTrigger != null) {
                audioRepository.loadAudioList(contentTrigger)
            } else {
                AbsentLiveData.create<Resource<List<Audio>>>()
            }
        }
    }

    fun setContentTrigger(audioListTrigger: AudioListTrigger) {
        this.contentListTrigger.value = audioListTrigger
    }
}
