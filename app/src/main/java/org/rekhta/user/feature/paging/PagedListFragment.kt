package org.rekhta.user.feature.paging

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.snackbar.Snackbar
import org.rekhta.data.model.Status
import kotlinx.android.synthetic.main.fragment_poet_list.*
import org.rekhta.user.databinding.FragmentPoetListBinding
import org.rekhta.data.extensions.observeK
import org.rekhta.user.feature.common.BaseFragment
import org.rekhta.data.model.PagedModel
import org.rekhta.user.ui.SpacingItemDecorator

/**
 * Created by VipulKumar on 05/09/18.
 */
abstract class PagedListFragment<LI : PagedModel, VM : PagedListViewModel<LI>>(
    private val vmClass: Class<VM>
) : BaseFragment() {
    private lateinit var binding: FragmentPoetListBinding
    private lateinit var controller: PagedEpoxyController<LI>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentPoetListBinding.inflate(inflater, container, false)
        return binding.root
    }

    protected lateinit var viewModel: VM

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(vmClass)
        controller = createController()
        controller.callbacks = object : PagedEpoxyController.Callbacks<LI> {
            override fun onItemClicked(item: LI) {
                this@PagedListFragment.onItemClicked(item)
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        root.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN

        recyclerView.apply {
            // We set the item animator to null since it can interfere with the enter/shared element
            // transitions
            itemAnimator = null

            setController(controller)
            addItemDecoration(SpacingItemDecorator(paddingLeft))
        }

        viewModel.liveList.observeK(this) {
            controller.setList(it)
        }

        viewModel.viewState.observeK(this) { viewState ->
            viewState?.uiResource?.let {
                when (it.status) {
                    Status.SUCCESS -> {
                        controller.isLoading = false
                    }
                    Status.ERROR -> {
                        controller.isLoading = false
                        Snackbar.make(recyclerView, it.message ?: "EMPTY", Snackbar.LENGTH_SHORT).show()
                    }
                    Status.REFRESHING -> {}
                    Status.LOADING_MORE -> controller.isLoading = true
                }
            }
        }
    }

    abstract fun onItemClicked(item: LI)

    abstract fun createController(): PagedEpoxyController<LI>

    override fun onLanguageChanged() {
    }
}
