/*
 * Copyright 2017 Google, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.rekhta.user.feature.paging

import com.airbnb.epoxy.EpoxyModel
import com.airbnb.epoxy.paging.PagingEpoxyController
import org.rekhta.data.model.PagedModel

abstract class PagedEpoxyController<LI : PagedModel> : PagingEpoxyController<LI>() {
    internal var callbacks: Callbacks<LI>? = null

    var isLoading = false
        set(value) {
            if (value != field) {
                field = value
                requestModelBuild()
            }
        }

    interface Callbacks<in LI> {
        fun onItemClicked(item: LI)
    }

    override fun buildModels(items: MutableList<LI?>) {
//        if (!items.isEmpty()) {
//            items.forEachIndexed { index, item ->
//                when {
//                    item != null -> buildItemModel(item)
//                    else -> buildItemPlaceholder(index)
//                }.addTo(this)
//            }
//        } else {
//            emptyState {
//                id("item_placeholder")
//                spanSizeOverride(TotalSpanOverride)
//            }
//        }
//
//        if (isLoading) {
//            infiniteLoading {
//                id("loading_view")
//                spanSizeOverride(TotalSpanOverride)
//            }
//        }
    }

    protected abstract fun buildItemModel(item: LI): EpoxyModel<Any>

    protected abstract fun buildItemPlaceholder(index: Int): EpoxyModel<Any>
}
