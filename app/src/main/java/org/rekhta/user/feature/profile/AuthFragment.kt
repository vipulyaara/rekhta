package org.rekhta.user.feature.profile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import org.rekhta.user.R
import org.rekhta.user.feature.poet.PoetListFragment
import org.rekhta.data.model.tab.PoetTabData
import org.rekhta.user.feature.common.BaseActivity
import org.rekhta.user.feature.common.BaseFragment

class AuthFragment : BaseFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_poets, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as BaseActivity).getToolbar().title = ""
        childFragmentManager.beginTransaction()
                .replace(R.id.fragmentContainer, PoetListFragment.newInstance(PoetTabData.TOP_READ.targetId))
                .commit()
    }

    override fun onLanguageChanged() {
    }

    companion object {
        fun newInstance(): AuthFragment {
            return AuthFragment()
        }
    }
}
