package org.rekhta.user.feature.content

import com.airbnb.epoxy.TypedEpoxyController
import com.like.LikeButton
import com.like.OnLikeListener
import org.rekhta.user.ItemContentBindingModel_
import org.rekhta.user.ItemLoaderBindingModel_
import org.rekhta.data.model.content.Content
import java.util.*

/**
 * Epoxy needs a controller to build and add models to it.
 * See https://github.com/airbnb/epoxy for more info.
 */
class SherListController(var contentClickListener: ContentClickListener) : TypedEpoxyController<List<Content>>() {
    override fun buildModels(contentList: List<Content>?) {
        if (contentList != null && contentList.isNotEmpty()) {
            for (content in contentList) {
                ItemContentBindingModel_()
                        .id(content.i + content.getPoetName())
                        .content(content)
                        .clickListener { model, parentView, clickedView, position ->
                            contentClickListener.onContentItemClicked(content)
                        }
                        .onLikeListener(object : OnLikeListener {
                            override fun liked(likeButton: LikeButton?) {
                                if (likeButton != null) {
                                    contentClickListener.onLikeClicked(content, likeButton, true)
                                }
                            }

                            override fun unLiked(likeButton: LikeButton?) {
                                if (likeButton != null) {
                                    contentClickListener.onLikeClicked(content, likeButton, false)
                                }
                            }
                        })
                        .addTo(this)
            }
            ItemLoaderBindingModel_()
                    .id(UUID.randomUUID().toString())
                    .addTo(this)
        }
    }

    fun addData(poets: List<Content>) {
        val newList = ArrayList<Content>()
        currentData?.let { newList.addAll(it) }
        newList.addAll(poets)
        setData(newList)
    }

    fun replaceData(poets: List<Content>) {
        setData(poets)
    }

    interface ContentClickListener {
        fun onContentItemClicked(content: Content)
        fun onLikeClicked(content: Content, clickedView: LikeButton, isLiked: Boolean)
    }
}
