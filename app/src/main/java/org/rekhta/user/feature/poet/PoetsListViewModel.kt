// package org.rekhta.user.feature.poet
//
// import org.rekhta.data.interactor.PagingInteractor
// import org.rekhta.data.util.AppCoroutineDispatchers
// import org.rekhta.data.util.AppRxSchedulers
// import org.rekhta.data.util.NetworkDetector
// import org.rekhta.data.util.Logger
// import kotlinx.coroutines.experimental.withContext
// import org.rekhta.user.feature.paging.PagedListViewModel
// import org.rekhta.data.model.poet.Poet
//
// /**
// * Created by VipulKumar on 05/09/18.
// */
// class PoetsListViewModel constructor(
//        schedulers: AppRxSchedulers,
//        dispatchers: AppCoroutineDispatchers,
//        private val interactor: PoetsListInteractor,
//        networkDetector: NetworkDetector,
//        logger: Logger
// ) : PagedListViewModel<Poet>(
//        schedulers,
//        dispatchers,
//        interactor.dataSourceFactory(),
//        networkDetector,
//        logger
// ) {
//
//    override suspend fun callLoadMore() {
//        withContext(interactor.dispatcher) {
//            interactor(PoetsListInteractor.ExecuteParams(PoetsListInteractor.Page.NEXT_PAGE))
//        }
//    }
//
//    override suspend fun callRefresh() {
//        withContext(interactor.dispatcher) {
//            interactor(PoetsListInteractor.ExecuteParams(PoetsListInteractor.Page.REFRESH))
//        }
//    }
// }
