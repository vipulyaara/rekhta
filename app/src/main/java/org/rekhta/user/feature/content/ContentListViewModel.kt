package org.rekhta.user.feature.content

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import org.rekhta.data.repo.content.ContentListTrigger
import org.kodein.di.generic.instance
import org.rekhta.data.kodeinInstance
import org.rekhta.user.RekhtaApplication
import org.rekhta.data.repo.content.ContentRepository
import org.rekhta.user.feature.common.BaseViewModel

/**
 * Authored by vipulkumar on 13/12/17.
 */

class ContentListViewModel : BaseViewModel() {

    private val contentRepository: ContentRepository by kodeinInstance.instance()
    private val contentListTrigger = MutableLiveData<ContentListTrigger>()
    var isFromProfile = false

    private val repoResult =
            Transformations.map(contentListTrigger) { contentRepository.loadContent(it) }

    val contentList = Transformations.switchMap(repoResult) { it.pagedList }!!
    val networkState = Transformations.switchMap(repoResult) { it.networkState }!!
    val refreshState = Transformations.switchMap(repoResult) { it.refreshState }!!

    fun setContentTrigger(contentListTrigger: ContentListTrigger) {
        this.contentListTrigger.value = contentListTrigger
    }
}
