package org.rekhta.user.feature.profile

import android.os.Build
import android.os.Bundle
import android.transition.TransitionInflater
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.NavHostFragment
import org.rekhta.data.Environment
import org.rekhta.data.model.Resource
import org.rekhta.data.model.poet.PoetProfile
import org.rekhta.user.R
import org.rekhta.user.databinding.FragmentProfileBinding
import org.rekhta.user.feature.common.BaseActivity
import org.rekhta.user.feature.common.DataBindingFragment
import org.rekhta.user.feature.poet.PoetViewModel
import org.rekhta.user.feature.tabs.TabbedFragment
import org.rekhta.user.util.DataUtil.prepareTabParcelableForProfile

/**
 * Authored by vipulkumar on 13/12/17.
 */

class PoetProfileFragment : DataBindingFragment<FragmentProfileBinding>(R.layout.fragment_profile) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val poetDetailViewModel = ViewModelProviders.of(activity!!)
                .get(PoetViewModel::class.java)
        (activity as BaseActivity).getToolbar().title = ""
        setupTabsFragment()
        fetchPoetProfile(poetDetailViewModel)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            sharedElementEnterTransition = TransitionInflater
                    .from(context).inflateTransition(android.R.transition.move)
        }
    }

    private fun setupTabsFragment() {
        childFragmentManager
                .beginTransaction()
                .replace(R.id.fragmentContainer, TabbedFragment
                        .newInstance(prepareTabParcelableForProfile(
                                arguments?.getString("poetId") ?: "",
                                arguments?.getInt("ghazalCount") ?: 0,
                                arguments?.getInt("nazmCount") ?: 0))).commit()
    }

    private fun fetchPoetProfile(poetDetailViewModel: PoetViewModel) {
        poetDetailViewModel.setPoetId(arguments?.getString("poetId") ?: "")
        poetDetailViewModel.poetProfileResponse?.observe(this, Observer {
            if (it != null) {
                onDataLoaded(it)
            }
        })

        Environment.language.observe(this, Observer {
            onDataLoaded(poetDetailViewModel.poetProfileResponse?.value)
        })
    }

    private fun onDataLoaded(resource: Resource<PoetProfile>?) {
        binding.poetProfile = resource?.data
        binding.imageUrl = arguments?.getString("poetProfileImageUrl")
        if (resource?.data != null) {
//            toolbarTitle = resource.data?.nameInEnglish ?: ""
//            app_bar_layout.setCollapsedTitle((activity as BaseActivity).getToolbar(), toolbarTitle)
        }
    }

    override fun onPause() {
        super.onPause()
//        app_bar_layout.setCollapsedTitle((activity as BaseActivity).getToolbar(), "")
    }

    override fun onResume() {
        super.onResume()
//        app_bar_layout.setCollapsedTitle((activity as BaseActivity).getToolbar(), toolbarTitle)
    }

    override fun onLanguageChanged() {
    }

    companion object {
        fun newInstance(
                poetId: String?,
                poetName: String?,
                poetProfileImageUrl: String?,
                ghazalCount: Int,
                nazmCount: Int
        ): PoetProfileFragment {
            val args = Bundle()
            val fragment = PoetProfileFragment()
            args.putString("poetId", poetId)
            args.putString("poetName", poetName)
            args.putString("poetProfileImageUrl", poetProfileImageUrl)
            args.putInt("ghazalCount", ghazalCount)
            args.putInt("nazmCount", nazmCount)
            fragment.arguments = args
            return fragment
        }
    }
}
