package org.rekhta.user.feature.content

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagedList
import androidx.recyclerview.widget.DefaultItemAnimator
import com.like.LikeButton
import org.rekhta.user.data.AutoClearedValue
import org.rekhta.user.util.FeedbackUtil
import org.rekhta.data.model.Resource
import org.rekhta.user.R
import org.rekhta.user.databinding.FragmentContentListBinding
import org.rekhta.data.model.content.Content
import org.rekhta.data.repo.content.ContentListTrigger
import org.rekhta.user.feature.common.BaseFragment
import org.rekhta.user.feature.home.HomeActivity

/**
 * Authored by vipulkumar on 13/12/17.
 */

class ContentListFragment : BaseFragment() {
    private lateinit var binding: AutoClearedValue<FragmentContentListBinding>
    private lateinit var contentListController: AutoClearedValue<ContentListController>

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val dataBinding = DataBindingUtil
                .inflate<FragmentContentListBinding>(inflater,
                        R.layout.fragment_content_list, container, false)
        binding = AutoClearedValue(this, dataBinding)
        return dataBinding.root
    }

    private lateinit var contentListViewModel: ContentListViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViewModels()
        initUi()
        fetchData()
    }

    private fun initViewModels() {
        contentListViewModel = ViewModelProviders.of(this)
                .get(ContentListViewModel::class.java)
        contentListViewModel.isFromProfile = arguments?.getString("targetId")?.isEmpty() == true
        contentListViewModel.contentList.observe(this, Observer {
            onDataLoaded(it)
        })
    }

    private fun initUi() {
        val contentListController = ContentListController(contentListViewModel,
                object : ContentListController.ContentClickListener {
                    override fun onLikeClicked(
                            content: Content,
                            clickedView: LikeButton,
                            isLiked: Boolean
                    ) {
                        if (isLiked) {
                            FeedbackUtil.showMessage(context, "Added to favorites")
                        } else {
                            FeedbackUtil.showMessage(context, "Removed from favorites")
                        }
                    }

                    override fun onContentItemClicked(content: Content) {
                        (activity as HomeActivity).launchContentDetailFragment(content.i)
                    }
                })
        this.contentListController = AutoClearedValue(this, contentListController)
        binding.get()?.recyclerView?.adapter = this.contentListController.get()?.adapter
        binding.get()?.recyclerView?.itemAnimator = DefaultItemAnimator()
    }

    private fun fetchData() {
        contentListViewModel
                .setContentTrigger(ContentListTrigger(arguments
                        ?.getString("poetId") ?: "",
                        arguments?.getString("contentTypeId") ?: "",
                        arguments?.getString("targetId") ?: ""))
    }

    override fun onLanguageChanged() {
        onDataLoaded(contentListViewModel.contentList.value)
    }

    private fun onDataLoaded(resource: PagedList<Content>?) {
        binding.get()?.resource = Resource.success(resource)
        binding.get()?.viewModel = contentListViewModel
        if (resource != null) {
            contentListController.get()?.replaceData(resource)
        }
    }

    companion object {
        fun newInstance(
                poetId: String?,
                contentTypeId: String?,
                targetId: String?
        ): ContentListFragment {
            val args = Bundle()
            val fragment = ContentListFragment()
            args.putSerializable("poetId", poetId)
            args.putSerializable("contentTypeId", contentTypeId)
            args.putSerializable("targetId", targetId)
            fragment.arguments = args
            return fragment
        }
    }
}
