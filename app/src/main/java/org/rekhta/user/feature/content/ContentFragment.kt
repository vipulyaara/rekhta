package org.rekhta.user.feature.content

import android.os.Bundle
import android.view.View
import org.rekhta.data.model.tab.TabParcelable
import org.rekhta.data.util.inTransaction
import org.rekhta.user.R
import org.rekhta.user.databinding.FragmentContentBinding
import org.rekhta.user.feature.common.DataBindingFragment
import org.rekhta.user.feature.tabs.TabbedFragment
import org.rekhta.user.util.DataUtil

/**
 * Created by VipulKumar on 24/09/18.
 */
class ContentFragment
    : DataBindingFragment<FragmentContentBinding>(R.layout.fragment_content) {
    override fun onLanguageChanged() {
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUi()
    }

    private fun initUi() {
        childFragmentManager.inTransaction {
            replace(R.id.fragmentContainer,
                    TabbedFragment.newInstance(
                            arguments?.getSerializable("tab") as TabParcelable))
        }
    }

    companion object {
        fun newInstance(tabData: TabParcelable
        ): ContentListFragment {
            val args = Bundle()
            val fragment = ContentListFragment()
            args.putSerializable("tab", tabData)
            fragment.arguments = args
            return fragment
        }
    }
}
