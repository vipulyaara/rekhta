package org.rekhta.user.feature.content

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.bottomsheet.BottomSheetDialog
import org.rekhta.user.data.AutoClearedValue
import org.rekhta.data.model.Resource
import org.rekhta.user.R
import org.rekhta.user.databinding.FragmentContentDetailBinding
import org.rekhta.data.model.contentDetail.ContentDetailResponse
import org.rekhta.data.model.contentDetail.Para
import org.rekhta.data.model.contentDetail.Word
import org.rekhta.user.feature.common.BaseActivity
import org.rekhta.user.ui.widget.detailview.ContentDetailView
import org.rekhta.data.Environment
import org.rekhta.user.util.FeedbackUtil
import org.rekhta.data.model.Status
import org.rekhta.user.databinding.ViewWordMeaningBinding
import org.rekhta.data.model.dictionary.WordMeaningResponse
import org.rekhta.data.repo.content.ContentDetailTrigger
import org.rekhta.user.feature.common.BaseFragment
import org.rekhta.user.feature.home.HomeActivity

/**
 * Authored by vipulkumar on 13/12/17.
 */

class ContentDetailFragment : BaseFragment() {
    private lateinit var binding: AutoClearedValue<FragmentContentDetailBinding>
    private lateinit var bottomSheetDialog: BottomSheetDialog
    private var dialogBinding: ViewWordMeaningBinding? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val dataBinding = DataBindingUtil
                .inflate<FragmentContentDetailBinding>(inflater, R.layout.fragment_content_detail, container, false)
        binding = AutoClearedValue(this, dataBinding)
        return dataBinding.root
    }

    private lateinit var contentDetailViewModel: ContentDetailViewModel
    private val wordClickListener = object : ContentDetailView.WordClickListener {
        override fun onWordClicked(word: Word?) {
            showMeaning(word)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViewModels()
        initUi()
        fetchData()
    }

    private fun initUi() {
        (activity as BaseActivity).getToolbar().title = ""
        binding.get()?.contentContentDetail?.tvPoetName?.setOnClickListener {
            val detail = contentDetailViewModel.contentDetail.value?.data
            if (detail != null) {
                if (activity is HomeActivity) {
//                    (activity as HomeActivity).launchPoetProfileFragment(detail.poet)
                }
            }
        }
        initBottomSheet()
    }

    private fun initBottomSheet() {
        bottomSheetDialog = BottomSheetDialog(context!!)
        dialogBinding = DataBindingUtil.inflate(LayoutInflater.from(context),
                R.layout.view_word_meaning, null, false)
        dialogBinding?.ivClose?.setOnClickListener {
            bottomSheetDialog.dismiss()
        }
        bottomSheetDialog.setContentView(dialogBinding?.root)
    }

    private fun initViewModels() {
        contentDetailViewModel = ViewModelProviders.of(this).get(ContentDetailViewModel::class.java)

        contentDetailViewModel.contentDetail.observe(this, Observer {
            onDataLoaded(it)
        })
        contentDetailViewModel.wordMeaning.observe(this, Observer {
            onWordMeaningLoaded(it)
        })
    }

    private fun fetchData() {
        contentDetailViewModel.setContentTrigger(ContentDetailTrigger(
                Environment.getLanguage(context).code, arguments?.getString("contentId") ?: ""
        ))
    }

    override fun onLanguageChanged() {
        fetchData()
    }

    private fun onWordMeaningLoaded(it: Resource<WordMeaningResponse>?) {
        when (it?.status) {
            Status.LOADING_MORE -> {
            }
            Status.ERROR -> {
                FeedbackUtil.showMessage(context, "Some error occurred")
            }
            Status.SUCCESS -> {
                dialogBinding?.word = it.data
                bottomSheetDialog.show()
            }
        }
    }

    private fun onDataLoaded(resource: Resource<ContentDetailResponse>?) {
        binding.get()?.resource = resource
        if (resource != null) {
            binding.get()?.contentDetail = resource.data
            if (resource.data?.getContentToRender() != null) {
                binding.get()?.contentContentDetail
                        ?.contentDetailView?.wordClickListener = wordClickListener
                binding.get()?.contentContentDetail
                        ?.contentDetailView?.addLines((resource.data
                        ?.getContentToRender()?.para as ArrayList<Para>?)!!, resource.data?.ra ?: 1)
            }
        }
    }

    private fun showMeaning(word: Word?) {
        word?.m?.let { contentDetailViewModel.setWordId(word.m!!) }
    }

    companion object {
        fun newInstance(contentId: String?): ContentDetailFragment {
            val args = Bundle()
            val fragment = ContentDetailFragment()
            args.putSerializable("contentId", contentId)
            fragment.arguments = args
            return fragment
        }
    }
}
