package org.rekhta.user.feature.content

import androidx.lifecycle.*

import org.rekhta.data.livedata.AbsentLiveData
import org.rekhta.data.model.Resource
import org.kodein.di.generic.instance
import org.rekhta.data.kodeinInstance
import org.rekhta.user.RekhtaApplication

import org.rekhta.data.model.contentDetail.ContentDetailResponse
import org.rekhta.data.model.dictionary.WordMeaningResponse
import org.rekhta.data.repo.content.ContentDetailTrigger
import org.rekhta.data.repo.content.ContentRepository
import org.rekhta.user.feature.common.BaseViewModel

/**
 * Authored by vipulkumar on 13/12/17.
 */

class ContentDetailViewModel : BaseViewModel() {

    private val contentRepository: ContentRepository by kodeinInstance.instance()
    private val contentDetailTrigger = MutableLiveData<ContentDetailTrigger>()
    private val wordId = MutableLiveData<String>()
    val contentDetail: LiveData<Resource<ContentDetailResponse>>
    val wordMeaning: LiveData<Resource<WordMeaningResponse>>

    init {
        contentDetail = Transformations.switchMap(contentDetailTrigger) { contentTrigger ->
            if (contentTrigger != null) {
                contentRepository.loadContentDetail(contentTrigger)
            } else {
                AbsentLiveData.create<Resource<ContentDetailResponse>>()
            }
        }

        wordMeaning = Transformations.switchMap(wordId) { wordId ->
            if (wordId != null) {
                contentRepository.loadWordMeaning(wordId)
            } else {
                AbsentLiveData.create<Resource<WordMeaningResponse>>()
            }
        }
    }

    fun setWordId(id: String) {
        wordId.value = id
    }

    fun setContentTrigger(contentTrigger: ContentDetailTrigger) {
        this.contentDetailTrigger.value = contentTrigger
    }
}
