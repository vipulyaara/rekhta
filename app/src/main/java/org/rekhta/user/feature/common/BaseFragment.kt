package org.rekhta.user.feature.common

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer

import org.rekhta.data.Environment

/**
 * Authored by vipulkumar on 18/09/17.
 */

abstract class BaseFragment : Fragment() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Environment.language.observe(this, Observer {
            onLanguageChanged()
        })
    }

    abstract fun onLanguageChanged()
}
