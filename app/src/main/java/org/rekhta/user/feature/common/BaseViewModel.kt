package org.rekhta.user.feature.common

import androidx.lifecycle.ViewModel

/**
 * Authored by vipulkumar on 09/11/17.
 */

open class BaseViewModel : ViewModel()
