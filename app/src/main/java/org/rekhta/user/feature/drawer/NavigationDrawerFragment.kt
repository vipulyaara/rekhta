package org.rekhta.user.feature.drawer

import android.annotation.SuppressLint
import android.os.Bundle
import android.preference.PreferenceManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.NavHostFragment
import org.rekhta.user.R
import org.rekhta.user.feature.DictionaryFragment
import org.rekhta.user.feature.audio.AudioFragment
import org.rekhta.user.feature.content.ContentFragment
import org.rekhta.user.feature.home.HomeActivity
import org.rekhta.user.feature.profile.UserProfileFragment
import org.rekhta.user.feature.tabs.TabbedFragment
import org.rekhta.user.ui.widget.LanguageView
import org.rekhta.user.util.DataUtil

class NavigationDrawerFragment : BaseNavigationDrawerFragment() {
    private var fragmentToLaunch: Fragment? = null

    @SuppressLint("InflateParams")
    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        navigationDrawerLayout = inflater.inflate(R.layout.drawer_layout,
                container, false) as ViewGroup

        val profileLayout = navigationDrawerLayout!!.findViewById<View>(R.id.profile_layout)
        profileLayout.setOnClickListener {
            fragmentToLaunch = UserProfileFragment.newInstance()
            closeDrawer()
        }

        val poetsLayout = navigationDrawerLayout!!.findViewById<View>(R.id.poet_layout)
        poetsLayout.setOnClickListener {
            fragmentToLaunch = ContentFragment.newInstance(DataUtil.prepareTabParcelableForPoetList())
            closeDrawer()
        }

        val ghazalLayout = navigationDrawerLayout!!.findViewById<View>(R.id.ghazal_layout)
        ghazalLayout.setOnClickListener {
            fragmentToLaunch = ContentFragment.newInstance(DataUtil.prepareTabParcelableForGhazals())
            closeDrawer()
        }

        val nazmLayout = navigationDrawerLayout!!.findViewById<View>(R.id.nazm_layout)
        nazmLayout.setOnClickListener {
            fragmentToLaunch = ContentFragment.newInstance(DataUtil.prepareTabParcelableForNazms())
            closeDrawer()
        }

        val dictionaryLayout = navigationDrawerLayout!!.findViewById<View>(R.id.dictionary_layout)
        dictionaryLayout.setOnClickListener {
            fragmentToLaunch = DictionaryFragment.newInstance()
            closeDrawer()
        }

        val languageWidget: LanguageView = navigationDrawerLayout!!.findViewById(R.id.language_widget)
        languageWidget.setAfterLanguageSelectedListener(object : LanguageView.AfterLanguageSelectedListener {
            override fun afterLanguageSelected() {
                closeDrawer()
            }
        })

        val audioLayout: ViewGroup = navigationDrawerLayout!!.findViewById(R.id.audio_layout)
        audioLayout.setOnClickListener {
            fragmentToLaunch = AudioFragment.newInstance()
            closeDrawer()
        }
        return navigationDrawerLayout
    }

    fun setUp(fragmentId: Int, drawerLayout: DrawerLayout) {
        mFragmentContainerView = activity?.findViewById(fragmentId)
        mDrawerLayout = drawerLayout

        mDrawerLayout?.setDrawerShadow(R.drawable.ic_menu_black,
                GravityCompat.START)
        mDrawerToggle = object : ActionBarDrawerToggle(activity, /* host Activity */
                mDrawerLayout, /* DrawerLayout object */
                R.string.navigation_drawer_open, R.string.navigation_drawer_open) {
            override fun onDrawerClosed(drawerView: View) {
                super.onDrawerClosed(drawerView)
                makeStatusBarColored()
                if (!isAdded) {
                    return
                }
                if (fragmentToLaunch != null) {
                    (activity as HomeActivity).launchFragment(fragmentToLaunch!!)
                    fragmentToLaunch = null
                }
                activity?.supportInvalidateOptionsMenu()
            }

            override fun onDrawerOpened(drawerView: View) {
                super.onDrawerOpened(drawerView)
                makeStatusBarTransparent()
                if (!isAdded) {
                    return
                }
                if (!mUserLearnedDrawer) {
                    // The user manually opened the com.readies.readies.drawer; store this flag to
                    // prevent auto-showing
                    // the navigation com.readies.readies.drawer automatically in the future.
                    mUserLearnedDrawer = true
                    val sp = PreferenceManager
                            .getDefaultSharedPreferences(activity)
                    sp.edit().putBoolean(PREF_USER_LEARNED_DRAWER, true)
                            .apply()
                }
                activity!!.supportInvalidateOptionsMenu()
            }
        }
        // If the user hasn't 'learned' about the com.readies.readies.drawer, open it to introduce
        // them to the com.readies.readies.drawer,
        // per the navigation com.readies.readies.drawer design guidelines.
        if (!mUserLearnedDrawer && !mFromSavedInstanceState) {
            mDrawerLayout?.openDrawer(mFragmentContainerView!!)
        }
        // Defer code dependent on restoration of previous instance state.
        mDrawerLayout?.post { mDrawerToggle?.syncState() }
        mDrawerLayout?.setDrawerListener(mDrawerToggle)
    }

    private fun closeDrawerWithDelay() {
    }

    private fun makeStatusBarTransparent() {
//        try {
//            //int currentapiVersion = android.os.Build.VERSION.SDK_INT;
//            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
//                // Do something for lollipop and above versions
//                val window = activity?.window
//                // clear FLAG_TRANSLUCENT_STATUS flag:
//                window?.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
//                // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
//                window?.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
//                // finally change the color to any color with transparency
//                window?.setStatusBarColor(resources.getColor(R.color.colorAccentDark))
//            }
//
//        } catch (e: Exception) {
//            // Log
//        }
    }

    private fun makeStatusBarColored() {
//        try {
//            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
//                // Do something for lollipop and above versions
//                val window = activity?.window
//                // clear FLAG_TRANSLUCENT_STATUS flag:
//                window?.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
//                // add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
//                window?.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
//                // finally change the color again to dark
//                window?.setStatusBarColor(resources.getColor(R.color.colorPrimaryDark))
//            }
//        } catch (e: Exception) {
//            // Log
//        }
//
    }
}
