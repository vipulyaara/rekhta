package org.rekhta.user.feature.audio

import androidx.lifecycle.Observer
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import org.rekhta.user.data.AutoClearedValue
import org.rekhta.data.model.Resource
import org.rekhta.user.R
import org.rekhta.user.databinding.FragmentContentListBinding
import org.rekhta.data.model.audio.Audio
import org.rekhta.data.repo.audio.AudioListTrigger
import org.rekhta.user.feature.common.BaseFragment
import org.rekhta.data.Environment

/**
 * Authored by vipulkumar on 13/12/17.
 */

class AudioListFragment : BaseFragment() {

    private lateinit var binding: AutoClearedValue<FragmentContentListBinding>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val dataBinding = DataBindingUtil
                .inflate<FragmentContentListBinding>(inflater, R.layout.fragment_content_list, container, false)
        binding = AutoClearedValue(this, dataBinding)
        return dataBinding.root
    }

    private lateinit var audioViewModel: AudioViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        audioViewModel = ViewModelProviders.of(this).get(AudioViewModel::class.java)
        initUi()
        fetchData()
    }

    private lateinit var audioListController: AutoClearedValue<AudioListController>

    private fun initUi() {
        val audioListController = AudioListController(object : AudioListController.AudioClickListener {
            override fun onContentItemClicked(audio: Audio) {
            }
        })

        this.audioListController = AutoClearedValue(this, audioListController)
        binding.get()?.recyclerView?.adapter = this.audioListController.get()?.adapter
        binding.get()?.recyclerView?.itemAnimator = null
    }

    private fun fetchData() {
        audioViewModel.setContentTrigger(AudioListTrigger(arguments
                ?.getString("poetId") ?: "", Environment.getLanguage(context!!).code))
        audioViewModel.audioList.observe(this, Observer {
            if (it != null) {
                onDataLoaded(it)
            }
        })
    }

    override fun onLanguageChanged() {
        onDataLoaded(audioViewModel.audioList.value)
    }

    private fun onDataLoaded(resource: Resource<List<Audio>>?) {
        binding.get()?.resource = resource
        if (resource?.data != null) {
            audioListController.get()?.replaceData(resource.data!!)
        }
    }

    companion object {
        fun newInstance(poetId: String?): AudioListFragment {
            val args = Bundle()
            val fragment = AudioListFragment()
            args.putSerializable("poetId", poetId)
            fragment.arguments = args
            return fragment
        }
    }
}
