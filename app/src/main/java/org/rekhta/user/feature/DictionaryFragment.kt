package org.rekhta.user.feature

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import org.rekhta.user.R
import org.rekhta.user.feature.common.BaseActivity
import org.rekhta.user.feature.common.BaseFragment

class DictionaryFragment : BaseFragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_dictionary, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as BaseActivity).getToolbar().title = "Dictionary"
    }

    override fun onLanguageChanged() {
    }

    companion object {
        fun newInstance(): DictionaryFragment {
            return DictionaryFragment()
        }
    }
}
