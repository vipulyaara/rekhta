package org.rekhta.user.feature.home

import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.airbnb.epoxy.TypedEpoxyController
import org.rekhta.data.util.ConnectionUtil
import org.rekhta.data.model.Resource
import kotlinx.android.synthetic.main.fragment_home.*
import org.kodein.di.generic.instance
import org.rekhta.data.data.AndroidNetworkDetector
import org.rekhta.user.ItemHomeCardBindingModel_
import org.rekhta.user.databinding.FragmentHomeBinding
import org.rekhta.data.model.homepage.Card
import org.rekhta.data.model.homepage.HomepageResponse
import org.rekhta.data.extensions.observeK
import org.rekhta.data.kodeinInstance
import org.rekhta.data.langCode
import org.rekhta.data.repo.home.HomeTrigger
import org.rekhta.user.R
import org.rekhta.user.extensions.letEmpty
import org.rekhta.user.feature.common.DataBindingFragment
import org.rekhta.user.itemHomeCard

class HomeFragment
    : DataBindingFragment<FragmentHomeBinding>(
        R.layout.fragment_home
) {
    private val androidNetworkDetector: AndroidNetworkDetector by kodeinInstance.instance()

    override fun onViewCreated(
            view: View,
            savedInstanceState: Bundle?
    ) {
        super.onViewCreated(view, savedInstanceState)
        initUi()
        initViewModel()
        fetchData()
    }

    private fun initUi() {
        initOfflineView()
        initCards()
    }

    private lateinit var homepageViewModel: HomepageViewModel

    private fun initViewModel() {
        homepageViewModel = ViewModelProviders.of(this)
                .get(HomepageViewModel::class.java)
        homepageViewModel.homepageResponse
                .observeK(this) { resource ->
                    resource?.let { onDataLoaded(it) }
                }
    }

    override fun onLanguageChanged() {
        homepageViewModel.homepageResponse.value?.let { onDataLoaded(it) }
    }

    private fun onDataLoaded(it: Resource<HomepageResponse>) {
        binding.resource = it
        it.data?.let { homepageResponse ->
            homepageResponse.topPoets?.letEmpty {
                binding.poetSliderView.setPoetList(it)
            }

            homepageResponse.featured?.letEmpty {
                binding.featured = it[0]
            }

            homepageResponse.cards.letEmpty {
                cardController.setData(it)
            }

            homepageResponse.video?.let {
                binding.video = it
            }

            homepageResponse.imgShayari?.let {
                binding.imageShayri = it[0].shortUrlUr ?: ""
            }

            homepageResponse.wordOfTheDay?.let {
                binding.apply {
                    wordOfTheDay = it
                    line = it.getContentToRender()?.para?.get(0)?.line?.get(0)
                }
            }
        }
    }

    private lateinit var cardController: CardListController

    private fun initCards() {
        val spanCount = 2
        cardController = CardListController().apply {
            setSpanCount(spanCount)
        }

        val gridLayoutManager = GridLayoutManager(context, spanCount)
                .apply {
                    spanSizeLookup = cardController.spanSizeLookup
                }

        rvCards.apply {
            layoutManager = gridLayoutManager
            isNestedScrollingEnabled = false
            adapter = cardController.adapter
        }
    }

    private fun fetchData() {
        homepageViewModel.setTrigger(HomeTrigger("", langCode(context)))
    }

    private fun initOfflineView() {
        // TODO: Observe connectivity

        if (!ConnectionUtil.isNetworkAvailable(context!!)) {
            offlineView.visibility = View.VISIBLE
        } else {
            offlineView.visibility = View.GONE
        }
    }

    inner class CardListController : TypedEpoxyController<List<Card>>() {

        override fun buildModels(cards: List<Card>?) {
            cards?.forEach { card ->
                itemHomeCard {
                    id(card.cardId)
                    clickListener { model, parentView, clickedView, position -> }
                    card(card)
                }
            }
        }
    }

    companion object {
        internal fun newInstance() = HomeFragment()
    }
}
