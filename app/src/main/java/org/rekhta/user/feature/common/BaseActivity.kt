package org.rekhta.user.feature.common

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import org.rekhta.data.Environment

/**
 * Authored by vipulkumar on 18/09/17.
 */

@SuppressLint("Registered")
open class BaseActivity : AppCompatActivity() {

    protected lateinit var appToolbar: Toolbar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setLanguage()
    }

    fun setToolbar(toolbar: Toolbar) {
        this.appToolbar = toolbar
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = ""
    }

    fun getToolbar(): Toolbar {
        return appToolbar
    }

    fun replaceFragment(id: Int, fragment: Fragment) {
        supportFragmentManager.beginTransaction()
                .replace(id, fragment)
                .commit()
    }

    fun addFragment(id: Int, fragment: Fragment) {
        supportFragmentManager.beginTransaction()
                .add(id, fragment)
                .commit()
    }

    fun addFragment(id: Int, fragment: Fragment, animationIn: Int, animationOut: Int) {
        supportFragmentManager.beginTransaction()
                .add(id, fragment)
                .setCustomAnimations(animationIn, animationOut)
                .commit()
    }

    fun replaceFragment(id: Int, fragment: Fragment, backStack: String) {
        supportFragmentManager.beginTransaction()
                .replace(id, fragment)
                .addToBackStack(backStack)
                .commit()
    }

    fun removeFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
                .remove(fragment)
                .commit()
    }

    private fun setLanguage() {
        Environment.language.value = Environment.getLanguage(this)
    }
}
