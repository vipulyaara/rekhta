package org.rekhta.user.feature.profile

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.databinding.DataBindingUtil
import android.os.Build
import android.os.Bundle
import android.transition.TransitionInflater
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import org.rekhta.user.data.AutoClearedValue
import org.rekhta.user.R
import org.rekhta.user.databinding.FragmentPoetProfileDescBinding
import org.rekhta.user.feature.common.BaseFragment
import org.rekhta.user.feature.poet.PoetViewModel

/**
 * Authored by vipulkumar on 13/12/17.
 */

class PoetProfileDescFragment : BaseFragment() {
    private lateinit var binding: AutoClearedValue<FragmentPoetProfileDescBinding>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val dataBinding = DataBindingUtil
                .inflate<FragmentPoetProfileDescBinding>(inflater, R.layout.fragment_poet_profile_desc, container, false)
        binding = AutoClearedValue(this, dataBinding)
        return dataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initUi()
    }

    private fun initUi() {
        val poetDetailViewModel = ViewModelProviders.of(activity!!).get(PoetViewModel::class.java)
        poetDetailViewModel.poetProfileResponse?.observe(this, Observer {
            binding.get()?.viewModel = poetDetailViewModel
        })
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            sharedElementEnterTransition = TransitionInflater
                    .from(context).inflateTransition(android.R.transition.move)
        }
    }

    override fun onLanguageChanged() {
    }

    companion object {
        fun newInstance(): PoetProfileDescFragment {
            val args = Bundle()
            val fragment = PoetProfileDescFragment()
            fragment.arguments = args
            return fragment
        }
    }
}
