package org.rekhta.user.feature.poet

import android.text.Spanned
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import org.kodein.di.generic.instance
import org.rekhta.data.kodeinInstance
import org.rekhta.data.livedata.AbsentLiveData
import org.rekhta.data.model.Resource
import org.rekhta.data.model.poet.PoetProfile
import org.rekhta.user.RekhtaApplication
import org.rekhta.user.feature.common.BaseViewModel
import org.rekhta.data.repo.poet.PoetRepository
import org.rekhta.data.util.StringUtil

/**
 * Authored by vipulkumar on 13/12/17.
 */

class PoetViewModel : BaseViewModel() {

    private val poetId = MutableLiveData<String>()
    private val poetRepository: PoetRepository by kodeinInstance.instance()
    private val targetId = MutableLiveData<String>()

    private val repoResult =
            Transformations.map(targetId) { poetRepository.loadPoets(it) }

    val poets = Transformations.switchMap(repoResult) { it.pagedList }!!
    val networkState = Transformations.switchMap(repoResult) { it.networkState }!!
    val refreshState = Transformations.switchMap(repoResult) { it.refreshState }!!

    fun fetchPoets(targetId: String) {
        this.targetId.value = targetId
    }

    val poetProfileResponse: LiveData<Resource<PoetProfile>>?

    val fullDescription: Spanned?
        get() = if (poetProfileResponse != null && poetProfileResponse.value != null &&
                poetProfileResponse.value?.data != null)
            StringUtil.getSpannableFromHtml(poetProfileResponse.value?.data?.descInEnglish)
        else
            null

    init {
        poetProfileResponse = Transformations.switchMap(poetId) { input ->
            if (input != null) {
                poetRepository.loadPoetProfile(input)
            } else {
                AbsentLiveData.create()
            }
        }
    }

    fun setPoetId(poetId: String) {
        this.poetId.value = poetId
    }
}
