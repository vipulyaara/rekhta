package org.rekhta.user.feature.search

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.airbnb.epoxy.TypedEpoxyController
import org.rekhta.user.data.AutoClearedValue
import org.rekhta.data.model.Resource
import org.rekhta.user.ItemDictionaryBindingModel_
import org.rekhta.user.R
import org.rekhta.user.databinding.FragmentSearchBinding
import org.rekhta.data.model.search.Dictionary
import org.rekhta.data.model.search.SearchResponse
import org.rekhta.user.feature.common.BaseActivity
import org.rekhta.user.feature.common.BaseFragment
import org.rekhta.data.Environment
import org.rekhta.data.model.search.SearchTrigger

/**
 * Authored by vipulkumar on 16/12/17.
 */

class SearchFragment : BaseFragment() {
    private lateinit var binding: AutoClearedValue<FragmentSearchBinding>
    lateinit var searchViewModel: SearchViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val dataBinding = DataBindingUtil
                .inflate<FragmentSearchBinding>(inflater, R.layout.fragment_search, container, false)
        binding = AutoClearedValue(this, dataBinding)
        return dataBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        searchViewModel = ViewModelProviders.of(this).get(SearchViewModel::class.java)
        initUi()
        searchViewModel.searchResponse.observe(this, Observer {
            if (it != null) {
                onDataLoaded(it)
            }
        })
    }

    private lateinit var dictionaryController: DictionaryController

    private fun initUi() {
        (activity as BaseActivity).getToolbar().title = ""
        dictionaryController = DictionaryController()
        binding.get()?.rvDictionary?.adapter = dictionaryController.adapter
    }

    private fun onDataLoaded(it: Resource<SearchResponse>) {
        binding.get()?.resource = it
        if (it.data?.poets != null) {
            binding.get()?.poetSliderView?.setPoetList(it.data?.poets)
            dictionaryController.setData(it.data?.dictionary)
        }
    }

    override fun onLanguageChanged() {
    }

    fun fetchData(keyword: String) {
        val trigger = SearchTrigger(keyword, Environment.getLanguage(context!!).code)
        searchViewModel.setTrigger(trigger)
    }

    companion object {
        fun newInstance(): SearchFragment {
            val args = Bundle()
            val fragment = SearchFragment()
            fragment.arguments = args
            return fragment
        }
    }

    class DictionaryController : TypedEpoxyController<List<Dictionary>>() {
        override fun buildModels(contentList: List<Dictionary>?) {
            if (contentList != null && contentList.isNotEmpty()) {
                for (content in contentList) {
                    ItemDictionaryBindingModel_()
                            .id(content.id)
                            .dictionary(content)
                            .addTo(this)
                }
            }
        }
    }
}
