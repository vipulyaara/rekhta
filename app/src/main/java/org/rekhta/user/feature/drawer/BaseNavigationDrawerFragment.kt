package org.rekhta.user.feature.drawer

import android.annotation.SuppressLint
import android.app.Activity
import android.content.res.Configuration
import android.os.Bundle
import android.os.Handler
import android.preference.PreferenceManager
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment

abstract class BaseNavigationDrawerFragment : Fragment() {

    // Remember the position of the selected item.
    private val STATE_SELECTED_POSITION = "selected_navigation_drawer_position"

    // Per the design guidelines, you should show the com.readies.readies.drawer on launch until the
    // user manually expands it. This shared preference tracks this.
    protected val PREF_USER_LEARNED_DRAWER = "navigation_drawer_learned"

    // Audio pointer to the current callbacks instance (the Activity).
    private var mCallbacks: NavigationDrawerCallbacks? = null

    // Handler to wait for the com.readies.readies.drawer to close before new fragment is drawn
    private val mDrawerHandler = Handler()

    // Helper component that ties the action bar to the navigation com.readies.readies.drawer.
    var mDrawerToggle: ActionBarDrawerToggle? = null
    private val drawerCloseTime = 300

    var mDrawerLayout: DrawerLayout? = null
    protected var navigationDrawerLayout: ViewGroup? = null
    protected var mDrawerListView: ListView? = null
    var mFragmentContainerView: View? = null

    var mCurrentSelectedPosition = 1
    protected var mFromSavedInstanceState: Boolean = false
    protected var mUserLearnedDrawer: Boolean = false

    val isDrawerOpen: Boolean
        get() = mDrawerLayout != null && mDrawerLayout!!.isDrawerOpen(mFragmentContainerView!!)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Read in the flag indicating whether or not the user has demonstrated
        // awareness of the com.readies.readies.drawer
        val sp = PreferenceManager
                .getDefaultSharedPreferences(activity)
        mUserLearnedDrawer = sp.getBoolean(PREF_USER_LEARNED_DRAWER, false)

        if (savedInstanceState != null) {
            mCurrentSelectedPosition = savedInstanceState
                    .getInt(STATE_SELECTED_POSITION)
            mFromSavedInstanceState = true
        }

        // Select either the default item (1) (com.readies.readies.drawer header is on position 0) or the last selected item.
        selectItem(mCurrentSelectedPosition, true)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        // Indicate that this fragment would like to influence the set of
        // actions in the action bar.
        setHasOptionsMenu(true)
    }

    @SuppressLint("InflateParams")
    abstract override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View?

    fun closeDrawer() {
        mDrawerLayout!!.closeDrawer(GravityCompat.START)
    }

    fun openDrawer() {
        mDrawerLayout!!.openDrawer(GravityCompat.START)
    }

    // public abstract void setUp(int fragmentId, DrawerLayout drawerLayout);

    fun selectItem(position: Int, isFromDrawer: Boolean) {
        mCurrentSelectedPosition = position
        if (mDrawerListView != null) {
            mDrawerListView!!.setItemChecked(position, true)
        }
        if (mDrawerLayout != null) {
            mDrawerLayout!!.closeDrawer(mFragmentContainerView!!)
        }
        if (mCallbacks != null) {
            // If the method is called on com.readies.readies.drawer item click, wait until com.readies.readies.drawer is closed to call new fragment
            // This ensures that com.readies.readies.drawer is closed smoothly
            if (isFromDrawer) {
                mDrawerHandler.postDelayed({ mCallbacks!!.onNavigationDrawerItemSelected(position) }, drawerCloseTime.toLong())
            } else
                mCallbacks!!.onNavigationDrawerItemSelected(position)
        }
    }

    override fun onAttach(activity: Activity?) {
        super.onAttach(activity)
        try {
            mCallbacks = activity as NavigationDrawerCallbacks?
        } catch (e: ClassCastException) {
            // Log Fabric exception
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt(STATE_SELECTED_POSITION, mCurrentSelectedPosition)
    }

    override fun onConfigurationChanged(newConfig: Configuration?) {
        super.onConfigurationChanged(newConfig)
        // Forward the new configuration the com.readies.readies.drawer toggle component.
        mDrawerToggle!!.onConfigurationChanged(newConfig)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return mDrawerToggle!!.onOptionsItemSelected(item) || super.onOptionsItemSelected(item)
    }

    // Callbacks interface that all activities using this fragment must implement
    interface NavigationDrawerCallbacks {
        // Called when an item in the navigation com.readies.readies.drawer is selected.
        fun onNavigationDrawerItemSelected(position: Int)
    }
}
