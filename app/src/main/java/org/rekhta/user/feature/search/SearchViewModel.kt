package org.rekhta.user.feature.search

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations

import org.rekhta.data.livedata.AbsentLiveData
import org.rekhta.data.model.Resource
import org.kodein.di.generic.instance
import org.rekhta.data.kodeinInstance
import org.rekhta.user.RekhtaApplication
import org.rekhta.data.model.search.SearchResponse
import org.rekhta.data.model.search.SearchTrigger
import org.rekhta.data.repo.search.SearchRepository
import org.rekhta.user.feature.common.BaseViewModel

/**
 * Authored by vipulkumar on 13/12/17.
 */

class SearchViewModel : BaseViewModel() {

    private val searchRepository: SearchRepository by kodeinInstance.instance()
    val trigger = MutableLiveData<SearchTrigger>()

    val searchResponse: LiveData<Resource<SearchResponse>>

    init {
        searchResponse = Transformations.switchMap(trigger) { input ->
            if (input != null) {
                searchRepository.loadSearchResponse(input)
            } else {
                AbsentLiveData.create()
            }
        }
    }

    fun setTrigger(trigger: SearchTrigger) {
        this.trigger.value = trigger
    }
}
