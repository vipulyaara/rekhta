package org.rekhta.user.feature.tabs

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import kotlinx.android.synthetic.main.fragment_tab.*
import kotlinx.android.synthetic.main.view_tab_text_indicator.*
import org.rekhta.data.model.tab.BaseTabData
import org.rekhta.data.model.tab.TabParcelable
import org.rekhta.user.R
import org.rekhta.user.databinding.FragmentTabBinding
import org.rekhta.user.feature.common.DataBindingFragment
import org.rekhta.user.util.DataUtil

/**
 * Authored by vipulkumar on 13/12/17.
 */

open class TabbedFragment : DataBindingFragment<FragmentTabBinding>(
        R.layout.fragment_tab
) {
    internal lateinit var tabData: BaseTabData
    private lateinit var tabParcelable: TabParcelable


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        tabParcelable = arguments?.getSerializable("tab") as TabParcelable
    }

    override fun onViewCreated(
            view: View,
            savedInstanceState: Bundle?
    ) {
        super.onViewCreated(view, savedInstanceState)
        initUi()
    }

    private fun initUi() {
        tabData = getTabData()
        setUpTabs()
    }

    private fun setUpTabs() {
        viewPager.apply {
            adapter = MasterTabAdapter(childFragmentManager)
            currentItem = tabParcelable.currentPage
            offscreenPageLimit = getTabOffscreenLimit()
        }

        tabs.addView(LayoutInflater.from(activity)
                .inflate(R.layout.view_tab_text_indicator, tabs, false))

        smartTabLayout.apply {
            setCustomTabView { container, position, _ ->
                val itemView = LayoutInflater.from(activity)
                        .inflate(R.layout.view_tab_text_item, container, false)
                val tvTitle: TextView = itemView.findViewById(R.id.tv_title)
                tvTitle.text = tabData.titles[position]
                itemView
            }

            setViewPager(viewPager)
        }
    }

    private fun getTabOffscreenLimit(): Int {
        return tabData.titles.size.let { if (it < 5) it else 5 }
    }

    private fun getTabData(): BaseTabData {
        return when (tabParcelable.tabId) {
            BaseTabData.TAB_POET_LIST -> DataUtil.preparePoetsTabData()
            BaseTabData.TAB_PROFILE -> DataUtil
                    .prepareProfileTabData(tabParcelable.poetId,
                            tabParcelable.ghazalCount, tabParcelable.nazmCount)
            BaseTabData.TAB_GHAZALS -> DataUtil.prepareGhazalTabData()
            BaseTabData.TAB_NAZMS -> DataUtil.prepareNazmTabData()
            else -> return BaseTabData()
        }
    }

    override fun onLanguageChanged() {
    }

    companion object {
        fun newInstance(tabParcelable: TabParcelable): TabbedFragment {
            val args = Bundle()
            val fragment = TabbedFragment()
            args.putSerializable("tab", tabParcelable)
            fragment.arguments = args
            return fragment
        }
    }

    internal inner class MasterTabAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {
        override fun getCount(): Int {
            return tabData.titles.size
        }

        override fun getItem(position: Int): Fragment? {
            return tabData.fragments[position]
        }
    }
}
