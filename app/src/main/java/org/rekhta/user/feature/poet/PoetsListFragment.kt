package org.rekhta.user.feature.poet

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import org.rekhta.data.model.Resource
import org.rekhta.user.R
import org.rekhta.user.databinding.FragmentPoetListBinding
import org.rekhta.data.model.poet.Poet
import org.rekhta.data.repo.pagination.NetworkState
import org.rekhta.user.feature.poet.PoetListController.PoetCallback
import org.rekhta.user.feature.common.BaseFragment
import org.rekhta.data.Environment
import org.rekhta.user.data.AutoClearedValue

/**
 * Authored by vipulkumar on 13/12/17.
 */

class PoetsListFragment : BaseFragment() {

    private lateinit var binding: AutoClearedValue<FragmentPoetListBinding>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val dataBinding = DataBindingUtil
                .inflate<FragmentPoetListBinding>(inflater, R.layout.fragment_poet_list, container, false)
        binding = AutoClearedValue(this, dataBinding)
        return dataBinding.root
    }

    private lateinit var poetViewModel: PoetViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        poetViewModel = ViewModelProviders.of(this).get(PoetViewModel::class.java)
        initUi()
        fetchData(poetViewModel)
    }

    private lateinit var poetListController: AutoClearedValue<PoetListController>

    private fun initUi() {
        val poetListController = PoetListController(object : PoetCallback {
            override fun onPoetItemClicked(poet: Poet, imageView: ImageView?) {
//                (activity as HomeActivity).launchPoetProfileFragment(poet)
            }
        })
        this.poetListController = AutoClearedValue(this, poetListController)
        binding.get()?.recyclerView?.adapter = this.poetListController.get()?.adapter
    }

    private fun fetchData(poetViewModel: PoetViewModel) {
        poetViewModel.fetchPoets(arguments!!.getString("targetId"))
        poetViewModel.poets.observe(this, Observer {
            if (it != null) {
                onDataLoaded(poetViewModel)
            }
        })
        poetViewModel.networkState.observe(this, Observer {
            if (it != null) {
                onNetworkStateChanged(it)
            }
        })
        Environment.language.observe(this, Observer {
            onDataLoaded(poetViewModel)
        })
    }

    private fun onNetworkStateChanged(it: NetworkState) {
    }

    private fun onDataLoaded(poetViewModel: PoetViewModel) {
        val resource = poetViewModel.poets.value
        binding.get()?.resource = Resource.success(resource)
        binding.get()?.viewModel = poetViewModel
        if (resource != null) {
            poetListController.get()?.replaceData(resource)
        }
    }

    override fun onLanguageChanged() {
        onDataLoaded(poetViewModel)
    }

    companion object {
        fun newInstance(targetId: String?): PoetsListFragment {
            val args = Bundle()
            val fragment = PoetsListFragment()
            args.putString("targetId", targetId)
            fragment.arguments = args
            return fragment
        }
    }
}
