package org.rekhta.user.feature.home

import android.os.Bundle
import android.view.View
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.activity_home.*
import org.rekhta.data.model.search.Poet
import org.rekhta.user.R
import org.rekhta.user.R.layout.drawer_layout
import org.rekhta.user.feature.content.ContentDetailFragment
import org.rekhta.user.feature.profile.PoetProfileFragment
import org.rekhta.user.feature.search.SearchFragment
import org.rekhta.user.feature.common.BaseActivity
import org.rekhta.user.feature.drawer.NavigationDrawerFragment
import org.rekhta.user.feature.profile.UserProfileFragment
import org.rekhta.user.ui.widget.RekhtaSearchView

class HomeActivity : BaseActivity() {
    var searchFragment: SearchFragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        initUi()
    }

    private fun initUi() {
        setupToolbar()
        setUpNavigationDrawer()
        replaceFragment(R.id.fragmentContainer, HomeFragment.newInstance())
        manageSearchView()
    }

    override fun onBackPressed() {
        if (search_view.isSearchShown) {
            search_view.hideSearch()
        } else {
            super.onBackPressed()
        }
    }

    fun hideSerach() {
        search_view.hideSearch()
    }

    fun showSearch() {
        search_view.showSearch()
    }

    private lateinit var navigationDrawerFragment: NavigationDrawerFragment

    private fun setUpNavigationDrawer() {
        navigationDrawerFragment = supportFragmentManager
                .findFragmentById(R.id.navigation_drawer) as NavigationDrawerFragment

        // Set up the drawer
        navigationDrawerFragment.setUp(R.id.navigation_drawer,
                drawer_layout as DrawerLayout)
    }

    private fun setupToolbar() {
        setToolbar(toolbar_home)
        appToolbar.setNavigationIcon(R.drawable.ic_menu_black)
    }

    fun launchFragment(fragment: Fragment) {
        replaceFragment(R.id.fragmentContainer, fragment, "")
    }

    fun launchPoetProfileFragment(poet: org.rekhta.data.model.poet.Poet) {
        supportFragmentManager.beginTransaction()
                .addToBackStack("")
                .add(R.id.fragmentContainer,
                        PoetProfileFragment.newInstance(poet.id,
                                poet.nameInEnglish,
                                poet.getFormattedImageUrl(),
                                poet.ghazalCount, poet.nazmCount))
                .commit()
    }

    fun launchPoetProfileFragment(poet: Poet) {
        supportFragmentManager.beginTransaction()
                .addToBackStack("")
                .replace(R.id.fragmentContainer,
                        PoetProfileFragment.newInstance(poet.entityId,
                                poet.name,
                                poet.getFormattedImageUrl(),
                                2, 2))
                .commit()
    }

    fun launchPoetProfileFragment(poet: org.rekhta.data.model.contentDetail.Poet?) {
        supportFragmentManager.beginTransaction()
                .addToBackStack("")
                .replace(R.id.fragmentContainer,
                        PoetProfileFragment.newInstance(poet?.cs, poet?.pn, poet?.iu, 2, 2))
                .commit()
    }

    fun launchContentDetailFragment(contentId: String?) {
        supportFragmentManager.beginTransaction()
                .addToBackStack("")
                .add(R.id.fragmentContainer,
                        ContentDetailFragment.newInstance(contentId))
                .commit()
    }

    fun launchUserProfileFragment() {
        supportFragmentManager.beginTransaction()
                .addToBackStack("")
                .add(R.id.fragmentContainer,
                        UserProfileFragment.newInstance())
                .commit()
    }

    private fun manageSearchView() {
        if (searchFragment == null) {
            searchFragment = SearchFragment.newInstance()
        }
        search_view.setSearchToggleLitener(object : RekhtaSearchView.SearchToggleListener {
            override fun onSearchOpened() {
                addFragment(R.id.fragmentContainer, searchFragment!!, R.anim.abc_slide_in_bottom, R.anim.abc_slide_out_bottom)
            }

            override fun onSearchClosed() {
                removeFragment(searchFragment!!)
            }

        })
        search_view.setSearchTextChangeListener(object : RekhtaSearchView.SearchTextChangeListener {
            override fun onSearchTextChanged(text: String) {

            }

            override fun onSearchTextSubmit(text: String) {
                searchFragment!!.fetchData(text)
            }
        })
    }
}
