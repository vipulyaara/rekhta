package org.rekhta.user.feature.audio

import com.airbnb.epoxy.TypedEpoxyController
import org.rekhta.user.ItemAudioBindingModel_
import org.rekhta.user.ItemLoaderBindingModel_
import org.rekhta.data.model.audio.Audio
import java.util.*

/**
 * Epoxy needs a controller to build and add models to it.
 * See https://github.com/airbnb/epoxy for more info.
 */
class AudioListController(var audioClickListener: AudioClickListener) : TypedEpoxyController<List<Audio>>() {

    override fun buildModels(audioList: List<Audio>?) {
        if (audioList != null && audioList.isNotEmpty()) {
            for (audio in audioList) {
                ItemAudioBindingModel_()
                        .id(audio.i)
                        .audio(audio)
                        .clickListener { model, parentView, clickedView, position ->
                            audioClickListener.onContentItemClicked(audio)
                        }
                        .addTo(this)
            }
            ItemLoaderBindingModel_()
                    .id(UUID.randomUUID().toString())
                    .addTo(this)
        }
    }

    fun addData(list: List<Audio>) {
        val newList = ArrayList<Audio>()
        currentData?.let { newList.addAll(it) }
        newList.addAll(list)
        setData(newList)
    }

    fun replaceData(list: List<Audio>) {
        setData(list)
    }

    interface AudioClickListener {
        fun onContentItemClicked(audio: Audio)
    }
}
