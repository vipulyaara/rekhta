package org.rekhta.user.feature.poet

import android.widget.ImageView
import androidx.paging.PagedList
import com.airbnb.epoxy.paging.PagingEpoxyController
import org.rekhta.user.ItemLoaderBindingModel_
import org.rekhta.user.ItemPoetBindingModel_
import org.rekhta.data.model.poet.Poet
import java.util.*

/**
 * Epoxy needs a controller to build and add models to it.
 * See https://github.com/airbnb/epoxy for more info.
 */
class PoetListController(private var poetCallback: PoetCallback?) : PagingEpoxyController<Poet>() {

    override fun buildModels(poets: List<Poet>) {
        if (poets != null && poets.isNotEmpty()) {
            for (poet in poets) {
                if (poet != null) {
                    ItemPoetBindingModel_()
                            .id(poet.id + poet.getPoetName())
                            .poet(poet)
                            .clickListener { model, parentView, clickedView, position ->
                                poetCallback?.onPoetItemClicked(poet, null)
                            }
                            .addTo(this)
                }
            }
            ItemLoaderBindingModel_()
                    .id(UUID.randomUUID().toString())
                    .addTo(this)
        }
    }

    fun replaceData(poets: PagedList<Poet>) {
        setList(poets)
    }

    interface PoetCallback {
        fun onPoetItemClicked(poet: Poet, imageView: ImageView?)
    }
}
