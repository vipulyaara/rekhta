package org.rekhta.user.feature.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations

import org.rekhta.data.livedata.AbsentLiveData
import org.rekhta.data.model.Resource
import org.kodein.di.generic.instance
import org.rekhta.data.kodeinInstance
import org.rekhta.user.RekhtaApplication

import org.rekhta.data.model.homepage.HomepageResponse
import org.rekhta.data.repo.home.HomeTrigger
import org.rekhta.data.repo.home.HomepageRepository
import org.rekhta.user.feature.common.BaseViewModel

/**
 * Authored by vipulkumar on 13/12/17.
 */

class HomepageViewModel : BaseViewModel() {

    private val homepageRepository: HomepageRepository
            by kodeinInstance.instance()
    private val trigger = MutableLiveData<HomeTrigger>()
    val homepageResponse: LiveData<Resource<HomepageResponse>>

    init {
        homepageResponse = Transformations.switchMap(trigger) { input ->
            if (input != null) {
                 homepageRepository.loadHomepageResponse(input)
            } else {
                 AbsentLiveData.create<Resource<HomepageResponse>>()
            }
        }
    }

    fun setTrigger(trigger: HomeTrigger) {
        this.trigger.value = trigger
    }
}
