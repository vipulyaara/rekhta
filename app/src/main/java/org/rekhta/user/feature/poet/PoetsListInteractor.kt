// package org.rekhta.user.feature.poet
//
// import androidx.paging.DataSource
// import org.rekhta.data.extensions.emptyFlowableList
// import org.rekhta.data.interactor.PagingInteractor
// import org.rekhta.data.interactor.SubjectInteractor
// import org.rekhta.data.util.AppCoroutineDispatchers
// import org.rekhta.data.util.AppRxSchedulers
// import io.reactivex.Flowable
// import kotlinx.coroutines.experimental.CoroutineDispatcher
// import org.kodein.di.generic.instance
// import org.rekhta.user.RekhtaApplication.Companion.kodeinInstance
// import org.rekhta.data.model.poet.Poet
//
// /**
// * Created by VipulKumar on 08/09/18.
// */
// class PoetsListInteractor : PagingInteractor<Poet>,
//        SubjectInteractor<Unit, PoetsListInteractor.ExecuteParams, List<Poet>>() {
//
//    private val dispatchers: AppCoroutineDispatchers by kodeinInstance.instance()
//    private val schedulers: AppRxSchedulers by kodeinInstance.instance()
// //    private val popularShowsRepository: PoetsListRepository by kodeinInstance.instance()
//
//    override val dispatcher: CoroutineDispatcher = dispatchers.io
//
//    init {
//        // We don't have params, so lets set Unit to kick off the observable
//        setParams(Unit)
//    }
//
//    override fun dataSourceFactory(): DataSource.Factory<Int, Poet> {
//        return popularShowsRepository.observeForPaging()
//    }
//
//    override fun createObservable(params: Unit): Flowable<List<Poet>> {
//        return popularShowsRepository.observeForFlowable()
//                .startWith(emptyFlowableList())
//                .subscribeOn(schedulers.io)
//    }
//
//    override suspend fun execute(params: Unit, executeParams: ExecuteParams) {
//        when (executeParams.page) {
//            Page.NEXT_PAGE -> popularShowsRepository.loadNextPage()
//            Page.REFRESH -> popularShowsRepository.refresh()
//        }
//    }
//
//    data class ExecuteParams(val page: Page)
//
//    enum class Page {
//        NEXT_PAGE, REFRESH
//    }
// }
