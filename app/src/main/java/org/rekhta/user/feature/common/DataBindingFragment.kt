package org.rekhta.user.feature.common

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import org.rekhta.user.databinding.FragmentHomeBinding

/**
 * Created by VipulKumar on 21/09/18.
 */
abstract class DataBindingFragment<Binding : ViewDataBinding>(
        private val layoutRes: Int
) : BaseFragment() {

    protected lateinit var binding: Binding

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil
                .inflate(inflater, layoutRes, container, false)
        binding.setLifecycleOwner(this)
        return binding.root
    }
}

