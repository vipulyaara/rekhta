package org.rekhta.user.feature.home

/**
 * Created by VipulKumar on 08/09/18.
 */
interface HomeNavigator {
    fun showPoetList()
    fun showPoetDetail()
    fun showContentList()
    fun showContentDetail()
    fun showSearch()
    fun showDictionary()
}
