package org.rekhta.user.extensions

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding

/**
 * Created by VipulKumar on 21/09/18.
 */

/** Inflate a [ViewGroup] */
fun ViewGroup.inflate(layoutId: Int, attachToRoot: Boolean = false): View {
    return LayoutInflater.from(context).inflate(layoutId, this, attachToRoot)
}

/** Inflate a [ViewGroup] with DataBinding */
fun ViewGroup.inflateWithDataBinding(layoutId: Int, attachToRoot: Boolean = false)
        : ViewDataBinding {
    return DataBindingUtil
            .inflate(LayoutInflater.from(context),
                    layoutId, this, attachToRoot)
}

fun ViewGroup.getChild(tag: String?): View? {
    return findViewWithTag(tag)
}

fun View.getString(stringId: Int): String? {
    return resources.getString(stringId)
}

val View.marginLeft: Int
    get() = (layoutParams as? ViewGroup.MarginLayoutParams)?.leftMargin ?: 0

val View.marginTop: Int
    get() = (layoutParams as? ViewGroup.MarginLayoutParams)?.topMargin ?: 0

val View.marginRight: Int
    get() = (layoutParams as? ViewGroup.MarginLayoutParams)?.rightMargin ?: 0

val View.marginBottom: Int
    get() = (layoutParams as? ViewGroup.MarginLayoutParams)?.bottomMargin ?: 0

val View.marginStart: Int
    get() = (layoutParams as? ViewGroup.MarginLayoutParams)?.marginStart ?: 0

val View.marginEnd: Int
    get() = (layoutParams as? ViewGroup.MarginLayoutParams)?.marginEnd ?: 0
