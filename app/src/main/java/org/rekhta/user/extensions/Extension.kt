package org.rekhta.user.extensions

import android.content.Context
import android.view.View
import android.view.ViewGroup
import org.rekhta.data.util.isNotNullOrEmpty
import org.rekhta.data.util.isNullOrEmpty


/**
 * Created by VipulKumar on 2/7/18.
 * helpful extension functions for presentation layer.
 */

/** Get children of a [ViewGroup] */
val ViewGroup.children: List<*>
    get() = (0 until childCount).map { getChildAt(it) }

fun Int.dp(context: Context) = this * context.resources.displayMetrics.density

fun View.dp(size: Int) = size * context.resources.displayMetrics.density

fun View.dpFromDimen(dimenRes: Int) = context.resources.getDimensionPixelSize(dimenRes)

fun Context.dpFromDimen(dimenRes: Int) = resources.getDimensionPixelSize(dimenRes)

fun Context.dp(size: Int) = size * resources.displayMetrics.density

fun <T : Collection<*>> T?.letEmpty(f: (it: T) -> Unit) {
    if (this != null && this.isNotEmpty()) f(this)
}


