package org.rekhta.user.ui.widget

import android.app.Activity
import android.content.Context
import android.graphics.drawable.Drawable
import android.text.Editable
import android.text.TextWatcher
import android.util.AttributeSet
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.ImageView
import android.widget.RelativeLayout
import androidx.vectordrawable.graphics.drawable.Animatable2Compat
import androidx.vectordrawable.graphics.drawable.AnimatedVectorDrawableCompat

import com.github.florent37.expectanim.ExpectAnim

import com.github.florent37.expectanim.core.Expectations.leftOfParent
import com.github.florent37.expectanim.core.Expectations.rightOfParent
import org.rekhta.data.util.AnimationUtil
import org.rekhta.data.util.DeviceUtil
import org.rekhta.data.util.StringUtil
import org.rekhta.user.R

/**
 * Custom searchView with animation. The UI of this view is made to accommodate the UI on Hotels SRP screen
 * You can add customizations to the UI if required.
 */

class RekhtaSearchView : RelativeLayout {
    private var etSearch: EditText? = null
    private var ivClear: ImageView? = null
    private var searchLayout: ViewGroup? = null
    private var ivSearch: ImageView? = null
    var isSearchShown: Boolean = false
    var searchToggleListener: SearchToggleListener? = null

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init()
    }

    private fun init() {
        addView()
    }

    private fun addView() {
        val view = LayoutInflater.from(context).inflate(R.layout.widget_search_view, this, true)
        this.ivSearch = view.findViewById(R.id.iv_search)
        this.searchLayout = view.findViewById(R.id.searchLayout)
        this.ivClear = view.findViewById(R.id.iv_clear)
        this.etSearch = view.findViewById(R.id.et_search)

        ivClear?.visibility = View.GONE

        searchLayout?.visibility = View.GONE
        ivSearch?.setOnClickListener {
            if (isSearchShown) {
                hideSearch()
            } else {
                showSearch()
            }
        }

        ivClear?.setOnClickListener {
            if (!StringUtil.isEmpty(etSearch?.text.toString())) {
                clearSearch()
            } else {
                hideSearch()
            }
        }
    }

    private fun clearSearch() {
        etSearch?.setText("")
    }

    fun setSearchToggleLitener(searchToggleListener: SearchToggleListener) {
        this.searchToggleListener = searchToggleListener
    }

    fun setSearchTextChangeListener(searchTextChangeListener: SearchTextChangeListener) {
        etSearch?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (s.isNotEmpty()) {
                    ivClear?.visibility = View.VISIBLE
                } else {
                    ivClear?.visibility = View.GONE
                }
                searchTextChangeListener.onSearchTextChanged(s.toString())
            }

            override fun afterTextChanged(s: Editable) {
            }
        })

        etSearch?.setOnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH ||
                    actionId == EditorInfo.IME_ACTION_DONE ||
                    event.action == KeyEvent.ACTION_DOWN && event.keyCode == KeyEvent.KEYCODE_ENTER) {
                searchTextChangeListener.onSearchTextSubmit(v.text.toString())
                DeviceUtil.hideSoftKeyboard(etSearch!!)
                true
            }
            false
        }
    }

    fun hideSearch() {
        if (isSearchShown) {
            if (searchToggleListener != null) {
                searchToggleListener?.onSearchClosed()
            }
            DeviceUtil.hideSoftKeyboard(etSearch!!)
            clearSearch()
            if (DeviceUtil.isPostLollipop) {
                val avd = AnimatedVectorDrawableCompat.create(context!!,
                        R.drawable.avd_back_to_search)
                ivSearch?.setImageDrawable(avd)
                avd?.start()
            } else {
                ivSearch?.setImageResource(R.drawable.ic_search_white_24dp)
            }

            AnimationUtil.animationOut(searchLayout!!, R.anim.fade_out, 0, true, context)
            ExpectAnim().expect(ivSearch)
                    .toBe(rightOfParent().withMarginDp(12f))
                    .toAnimation()
                    .setDuration(400)
                    .start()

            DeviceUtil.hideSoftKeyboard(this)
            isSearchShown = false
        }
    }

    fun showSearch() {
        if (!isSearchShown) {
            if (DeviceUtil.isPostLollipop) {
                val avd = AnimatedVectorDrawableCompat.create(context!!,
                        R.drawable.avd_search_to_back)

                ivSearch?.setImageDrawable(avd)
                if (avd != null) {
                    avd.registerAnimationCallback(object : Animatable2Compat.AnimationCallback() {

                        override fun onAnimationEnd(drawable: Drawable?) {
                            super.onAnimationEnd(drawable)
                            avd.unregisterAnimationCallback(this)
                        }
                    })
                    avd.start()
                }
            } else {
                ivSearch?.setImageResource(R.drawable.ic_clear_white_24dp)
            }

            AnimationUtil.animationIn(searchLayout, R.anim.fade_in, 0, context)
            ExpectAnim().expect(ivSearch)
                    .toBe(leftOfParent().withMarginDp(12f))
                    .toAnimation()
                    .setDuration(400)
                    .start()

            etSearch?.requestFocus()
            if (context is Activity) {
                DeviceUtil.showSoftKeyboard((context as Activity).window
                        .decorView.findViewById(android.R.id.content))
            }
            if (searchToggleListener != null) {
                searchToggleListener?.onSearchOpened()
            }
            isSearchShown = true
        }
    }

    interface SearchToggleListener {
        fun onSearchOpened()

        fun onSearchClosed()
    }

    interface SearchTextChangeListener {
        fun onSearchTextChanged(text: String)

        fun onSearchTextSubmit(text: String)
    }
}
