package org.rekhta.user.ui.widget

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.view_language.view.*
import org.rekhta.user.R
import org.rekhta.data.model.Language
import org.rekhta.data.Environment

/**
 * Authored by vipulkumar on 15/12/17.
 */

class LanguageView : LinearLayout {

    private lateinit var englishView: ViewGroup
    private lateinit var hindiView: ViewGroup
    private lateinit var urduView: ViewGroup
    private var views: HashMap<Language, ViewGroup> = HashMap()
    private var textViews: HashMap<Language, TextView> = HashMap()
    private var afterLanguageSelectedListener: AfterLanguageSelectedListener? = null

    private lateinit var localContext: Context

    constructor(context: Context) : super(context) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int, defStyleRes: Int)
            : super(context, attrs, defStyleAttr, defStyleRes) {
        init(context)
    }

    private fun init(context: Context) {
        localContext = context
        initViews()
        selectLanguage(Environment.getLanguage(context)) // Default
    }

    private fun initViews() {
        val rootView = LayoutInflater.from(localContext).inflate(R.layout.view_language, this, true)
        englishView = rootView.findViewById(R.id.english_layout)
        hindiView = rootView.findViewById(R.id.hindi_layout)
        urduView = rootView.findViewById(R.id.urdu_layout)

        views.put(Language.ENGLISH, englishView)
        views.put(Language.HINDI, hindiView)
        views.put(Language.URDU, urduView)

        textViews.put(Language.ENGLISH, rootView.tv_english)
        textViews.put(Language.HINDI, rootView.tv_hindi)
        textViews.put(Language.URDU, rootView.tv_urdu)

        views.forEach { i -> i.value.setOnClickListener { selectLanguage(i.key) } }
    }

    private fun selectLanguage(language: Language) {
        Environment.setLanguage(context, language)

        views.entries.forEach {
            if (language == it.key) {
                selectView(it.value)
            }
        }
        afterLanguageSelectedListener?.afterLanguageSelected()
    }

    private fun selectView(view: View) {
        views.entries.forEach {
            if (view == it.value) {
                it.value.setBackgroundResource(R.drawable.shape_language_selected)
                textViews[it.key]?.setTextColor(ContextCompat.getColor(context, R.color.background))
            } else {
                it.value.setBackgroundResource(R.drawable.shape_language_normal)
                textViews[it.key]?.setTextColor(ContextCompat.getColor(context, R.color.text_body))
            }
        }
    }

    fun setAfterLanguageSelectedListener(afterLanguageSelectedListener: AfterLanguageSelectedListener) {
        this.afterLanguageSelectedListener = afterLanguageSelectedListener
    }

    public interface AfterLanguageSelectedListener {
        fun afterLanguageSelected()
    }
}
