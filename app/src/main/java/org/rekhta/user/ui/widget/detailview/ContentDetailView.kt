package org.rekhta.user.ui.widget.detailview

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import org.rekhta.user.R
import org.rekhta.data.model.contentDetail.Para
import org.rekhta.data.model.contentDetail.Word

/**
 * Authored by vipulkumar on 27/12/17.
 */

class ContentDetailView : LinearLayout {
    var wordClickListener: WordClickListener? = null

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init()
    }

    private fun init() {
        orientation = LinearLayout.VERTICAL
    }

    fun addLines(paras: ArrayList<Para>, alignment: Int) {
        removeAllViews()
        paras.forEach {
            it.line?.forEach {
                val lineView = LineView(context)
                lineView.wordClickListener = wordClickListener
                lineView.addWords(it, alignment)
                addView(lineView)
                addView(LayoutInflater.from(context).inflate(R.layout.view_line_sep, null, false))
            }
            addView(LayoutInflater.from(context).inflate(R.layout.view_para_sep, null, false))
        }
    }

    interface WordClickListener {
        fun onWordClicked(word: Word?)
    }
}
