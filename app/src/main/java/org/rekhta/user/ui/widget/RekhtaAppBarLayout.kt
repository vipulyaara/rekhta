package org.rekhta.user.ui.widget

import android.content.Context
import androidx.appcompat.widget.Toolbar
import android.util.AttributeSet
import android.util.Log
import androidx.coordinatorlayout.widget.CoordinatorLayout
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.appbar.CollapsingToolbarLayout
import org.rekhta.user.R

/**
 * AppBarLayout that provides custom animation for title.
 */

class RekhtaAppBarLayout : AppBarLayout {
    private var attrs: AttributeSet? = null
    private var mCollapsedTitle: String? = null
    private var isTitleAnimationEnabled: Boolean = false

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrset: AttributeSet) : super(context, attrset) {
        this.attrs = attrset
        init()
    }

    private fun init() {
        getAttributes()
        customTitleAnimation()
    }

    private fun customTitleAnimation() {
        if (parent is CoordinatorLayout && mCollapsedTitle != null && isTitleAnimationEnabled) {
            if (getChildAt(0) is CollapsingToolbarLayout) {
                val collapsingToolbarLayout = getChildAt(0) as CollapsingToolbarLayout
                addOnOffsetChangedListener(OnOffsetChangedListener { appBarLayout: AppBarLayout, verticalOffset: Int ->
                    Log.e("Offset", "" + verticalOffset)
                    Log.e("Range", "" + appBarLayout.totalScrollRange)
                    when {
                        Math.abs(verticalOffset) >= appBarLayout.totalScrollRange - 10 -> collapsingToolbarLayout.title = mCollapsedTitle
                        verticalOffset == 0 -> collapsingToolbarLayout.title = ""
                        else -> collapsingToolbarLayout.title = ""
                    }
                })
            }
        }
    }

    private fun customTitleAnimation(toolbar: Toolbar?) {
        if (parent is CoordinatorLayout && mCollapsedTitle != null && isTitleAnimationEnabled) {
            if (toolbar != null) {
                addOnOffsetChangedListener(OnOffsetChangedListener { appBarLayout: AppBarLayout, verticalOffset: Int ->
                    Log.e("Offset", "" + verticalOffset)
                    Log.e("Range", "" + appBarLayout.totalScrollRange)
                    when {
                        Math.abs(verticalOffset) >= appBarLayout.totalScrollRange - 10 -> toolbar.title = mCollapsedTitle
                        verticalOffset == 0 -> toolbar.title = ""
                        else -> toolbar.title = ""
                    }
                })
            }
        }
    }

    fun setCollapsedTitle(title: String?) {
        mCollapsedTitle = title
        customTitleAnimation()
    }

    fun setCollapsedTitle(toolbar: Toolbar, title: String) {
        mCollapsedTitle = title
        customTitleAnimation(toolbar)
    }

    private fun getAttributes() {
        val a = context?.theme?.obtainStyledAttributes(
                attrs,
                R.styleable.RekhtaAppBarLayout,
                0, 0)

        try {
            mCollapsedTitle = a?.getString(R.styleable.RekhtaAppBarLayout_collapsedTitle)
            isTitleAnimationEnabled = a?.getBoolean(R.styleable.RekhtaAppBarLayout_titleAnimation, true) == true
        } finally {
            a?.recycle()
        }
    }
}
