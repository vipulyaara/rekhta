package org.rekhta.user.ui.widget.detailview

import android.content.Context
import android.util.AttributeSet
import android.view.Gravity
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import org.rekhta.user.R
import org.rekhta.data.model.contentDetail.Word

/**
 * Created by VipulKumar on 1/26/18.
 *
 */

class WordView : LinearLayout {
    var currentWord: Word? = null
    var wordClickListener: ContentDetailView.WordClickListener? = null

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init()
    }

    private fun init() {
        val wordText = LayoutInflater.from(context).inflate(R.layout.view_word, null, false)
        gravity = Gravity.END
        addView(wordText)
    }

    fun setWord(word: Word, alignment: Int) {
        currentWord = word
        val textView = getChildAt(0)
        textView?.let {
            (textView as TextView).text = word.w
        }
        addOnClickListeners()

//        if (alignment == 1) {
//            makeFullWidthLine()
//        }
    }

    private fun addOnClickListeners() {
        setOnClickListener { wordClickListener?.onWordClicked(currentWord) }
    }

    private fun makeFullWidthLine() {
        for (i in 0 until childCount) {
            val wordView = getChildAt(i)
            val layoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT)
            layoutParams.gravity = Gravity.END
//            when (i) {
// //                0 -> layoutParams.gravity = Gravity.START
// //                childCount - 1 -> layoutParams.gravity = Gravity.END
//                else -> layoutParams.gravity = Gravity.CENTER
//            }
            wordView.layoutParams = layoutParams
        }
    }
}
