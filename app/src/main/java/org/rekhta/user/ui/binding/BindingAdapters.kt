package org.rekhta.user.ui.binding

import androidx.databinding.BindingAdapter
import android.view.View
import android.widget.ImageView
import android.widget.TextView

import org.rekhta.user.util.ImageLoader
import org.rekhta.user.ui.widget.RekhtaAppBarLayout

import org.rekhta.data.model.contentDetail.Line
import org.rekhta.user.ui.widget.detailview.LineView
import org.rekhta.user.util.WidgetUtil

/**
 * Data Binding adapters specific to the app.
 */
@BindingAdapter("visibleGone")
fun showHide(view: View, show: Boolean) {
    view.visibility = if (show) View.VISIBLE else View.GONE
}

@BindingAdapter("collapsedTitle")
fun collapsedTitle(view: RekhtaAppBarLayout, title: String?) {
    view.setCollapsedTitle(title)
}

@BindingAdapter("visibleInvisible")
fun visibleInvisible(view: View, show: Boolean) {
    view.visibility = if (show) View.VISIBLE else View.INVISIBLE
}

@BindingAdapter("line")
fun addWords(view: LineView, line: Line?) {
    view.addWords(line, 1)
}

@BindingAdapter("strikeOff")
fun strikeOff(textView: TextView, strikeOff: Boolean) {
    if (strikeOff) {
        WidgetUtil.enableStrikeOffToTextView(textView)
    }
}

@BindingAdapter("imageUrl")
fun bindImage(imageView: ImageView?, url: String?) {
    if (imageView != null && url != null) {
        ImageLoader.loadImage(imageView.context, url, imageView)
    }
}
