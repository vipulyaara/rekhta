package org.rekhta.user.ui.widget

import android.content.Context
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import android.util.AttributeSet
import android.widget.ProgressBar

import com.github.ybq.android.spinkit.style.ThreeBounce
import org.rekhta.user.R

/**
 * Authored by vipulkumar on 16/11/17.
 */

class RekhtaProgressBar : ProgressBar {
    /**
     * Create a new progress bar with range 0...100 and initial progress of 0.
     *
     * @param context the application environment
     */
    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init()
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int, defStyleRes: Int)
            : super(context, attrs, defStyleAttr, defStyleRes) {
        init()
    }

    private fun init() {
        val cubeGrid = ThreeBounce()
        cubeGrid.color = ContextCompat.getColor(context, R.color.app_blue)
        indeterminateDrawable = cubeGrid
    }
}
