package org.rekhta.user.ui.widget.detailview

import android.content.Context
import android.util.AttributeSet
import android.view.Gravity
import android.view.ViewGroup
import android.widget.LinearLayout
import org.rekhta.user.R
import org.rekhta.data.model.contentDetail.Line
import org.rekhta.data.util.inflate

/**
 * Created by VipulKumar on 1/26/18.
 *
 */

class LineView : LinearLayout {
    var wordClickListener: ContentDetailView.WordClickListener? = null

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init()
    }

    private fun init() {
        orientation = LinearLayout.HORIZONTAL
    }

    fun addWords(line: Line?, alignment: Int = 1) {
        val words = line?.word
        words?.forEach {
            val wordView = WordView(context)
            val spaceView = inflate(R.layout.view_space)
            wordView.wordClickListener = wordClickListener
            wordView.setWord(it, alignment)
            addView(wordView)
            if (alignment != 1) {
                addView(spaceView)
            }
        }
        if (alignment == 1) {
            makeFullWidthLine()
        }
    }

    private fun makeFullWidthLine() {
        for (i in 0 until childCount) {
            val wordView = getChildAt(i)
            if (wordView is WordView) {
                val layoutParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT, 0.2f)
                when (i) {
                    0 -> wordView.gravity = Gravity.START
                    childCount - 1 -> wordView.gravity = Gravity.END
                    else -> wordView.gravity = Gravity.CENTER
                }
                wordView.layoutParams = layoutParams
            }
        }
    }
}
