package org.rekhta.user.ui.widget

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.View
import androidx.vectordrawable.graphics.drawable.Animatable2Compat
import androidx.vectordrawable.graphics.drawable.AnimatedVectorDrawableCompat
import org.rekhta.user.R

/**
 * Authored by vipulkumar on 25/09/17.
 */

class RekhtaHorizontalProgressBar : View {
    private var avd: AnimatedVectorDrawableCompat? = null

    /**
     * Simple constructor to use when creating a view from code.
     *
     * @param context The Context the view is running in, through which it can
     * access the current theme, resources, etc.
     */
    constructor(context: Context) : super(context) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(context)
    }

    private fun init(context: Context) {
        avd = AnimatedVectorDrawableCompat.create(context,
                R.drawable.avd_progress_bar_horizontal)
        avd?.registerAnimationCallback(object : Animatable2Compat.AnimationCallback() {
            override fun onAnimationEnd(drawable: Drawable?) {
                avd?.start()
            }
        })
        background = avd
        avd?.start()
    }

    override fun setVisibility(visibility: Int) {
        super.setVisibility(visibility)
        when (visibility) {
            View.VISIBLE -> avd?.start()
            View.GONE -> avd?.stop()
            INVISIBLE -> avd?.stop()
        }
    }
}
