package org.rekhta.user.ui.widget

import android.content.Context
import android.os.Bundle
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.airbnb.epoxy.TypedEpoxyController
import org.rekhta.data.model.search.Poet
import org.rekhta.user.ItemPoetSliderBindingModel_
import org.rekhta.user.R
import org.rekhta.user.extensions.letEmpty
import org.rekhta.user.feature.home.HomeActivity
import org.rekhta.user.util.DataUtil

/**
 * Authored by vipulkumar on 16/12/17.
 */

class PoetSliderView : LinearLayout {
    private var localContext: Context? = null
    private var poets: List<Poet>? = null
    lateinit var poetListSliderController: PoetSliderListController

    constructor(context: Context) : super(context) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init(context)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init(context)
    }

    private fun init(context: Context) {
        this.localContext = context
        addViews()
    }

    fun setPoetList(poets: List<Poet>?) {
        this.poets = poets
        poetListSliderController.setData(poets)
    }

    private fun addViews() {
        val view = LayoutInflater.from(context).inflate(R.layout.view_poet_slider, this, true)
        val recyclerView = view.findViewById<RecyclerView>(R.id.recycler_view)
        poetListSliderController = PoetSliderListController(object : PoetCallback {
            override fun onPoetItemClicked(poet: Poet, imageView: ImageView?) {
                (localContext as HomeActivity).hideSerach()
                (localContext as HomeActivity).launchPoetProfileFragment(poet)
            }
        })
        recyclerView.adapter = poetListSliderController.adapter
    }

    class PoetSliderListController(private var poetCallback: PoetCallback?) : TypedEpoxyController<List<Poet>>() {
        private val ids = arrayListOf<String?>()

        override fun buildModels(poets: List<Poet>?) {
            poets.letEmpty { poets ->
                for (poet in poets) {
                    if (poets.indexOf(poet) != 2) {
                        val itemPoetBindingModel = ItemPoetSliderBindingModel_()
                        itemPoetBindingModel
                                .id(poet.entityId + poet.name)
                                .clickListener { model, parentView, clickedView, position ->
                                    poetCallback?.onPoetItemClicked(poet, null)
                                }
                                .poet(poet)
                                .addTo(this)
                        ids.add(poet.entityId)
                    }
                }
            }
        }
    }

    interface PoetCallback {
        fun onPoetItemClicked(poet: Poet, imageView: ImageView?)
    }
}
