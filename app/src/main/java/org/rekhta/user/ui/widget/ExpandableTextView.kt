package org.rekhta.user.ui.widget

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.TimeInterpolator
import android.animation.ValueAnimator
import android.content.Context
import androidx.core.content.ContextCompat
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.view.animation.AccelerateDecelerateInterpolator
import android.widget.TextView
import androidx.appcompat.widget.AppCompatTextView
import org.rekhta.data.util.StringUtil
import org.rekhta.user.R

import java.util.ArrayList

/**
 * TextView that shows a fixed no of lines at first with a "show more" option.
 * Expands and collapses on click.
 */
class ExpandableTextView @JvmOverloads
constructor(context: Context, attrs: AttributeSet? = null,
            defStyleAttr: Int = 0) : AppCompatTextView(context, attrs, defStyleAttr) {
    private var mContext: Context? = null
    private var onExpandListeners: MutableList<OnExpandListener>? = null
    /**
     * Returns the current [TimeInterpolator] for expanding.
     *
     * @return the current interpolator, null by default.
     */
    /**
     * Sets a [TimeInterpolator] for expanding.
     *
     * @param expandInterpolator the interpolator
     */
    var expandInterpolator: TimeInterpolator? = null
    /**
     * Returns the current [TimeInterpolator] for collapsing.
     *
     * @return the current interpolator, null by default.
     */
    /**
     * Sets a [TimeInterpolator] for collpasing.
     *
     * @param collapseInterpolator the interpolator
     */
    var collapseInterpolator: TimeInterpolator? = null
    private var animationDuration: Long = 0
    private var animating: Boolean = false
    /**
     * Is this [ExpandableTextView] expanded or not?
     *
     * @return true if expanded, false if collapsed.
     */
    var isExpanded: Boolean = false
        private set
    private var collapsedHeight: Int = 0
    private var firstSetText = true
    private var hotelDesc: String? = null

    init {
        init(context, attrs, defStyleAttr)
    }

    private fun init(context: Context, attrs: AttributeSet?, defStyleAttr: Int) {
        // read attributes
        val attributes = context.obtainStyledAttributes(attrs, R.styleable.ExpandableTextView,
                defStyleAttr, 0)
        this.animationDuration = attributes.getInt(R.styleable.ExpandableTextView_animation_duration, 200).toLong()
        attributes.recycle()

        // keep the original value of maxLines
        this.maxLines = this.getMaxLines()
        this.mContext = context

        // create bucket of OnExpandListener instances
        this.onExpandListeners = ArrayList()

        // create default interpolator
        this.expandInterpolator = AccelerateDecelerateInterpolator()
        this.collapseInterpolator = AccelerateDecelerateInterpolator()

        this.onExpandListeners?.add(object : OnExpandListener {
            override fun onExpand(view: ExpandableTextView) {
                setTextToCollapse()
            }

            override fun onCollapse(view: ExpandableTextView) {
                setTextToExpand()
            }
        })
    }

    override fun setText(text: CharSequence, type: TextView.BufferType) {
        super.setText(text, type)

        if (firstSetText && !StringUtil.isEmpty(text)) {
            hotelDesc = text.toString()

            val viewTreeObserver = viewTreeObserver
            viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
                override fun onGlobalLayout() {
                    getViewTreeObserver().removeOnGlobalLayoutListener(this)
                    setTextToExpand()
                }
            })
        }
        firstSetText = true
    }

    fun setTextToCollapse() {
        val showLess = "show less"
        val text = "$hotelDesc $showLess"
        firstSetText = false
        setText(addClickablePartTextViewResizable(text, showLess),
                TextView.BufferType.SPANNABLE)
    }

    fun setTextToExpand() {
        try {
            val lineEndIndex = this@ExpandableTextView.layout.getLineEnd(maxLines - 1)
            val showMore = "... show more"
            val text = (hotelDesc?.subSequence(0, lineEndIndex - showMore.length + 1).toString() +
                    " " + showMore)
            firstSetText = false
            setText(addClickablePartTextViewResizable(text, showMore),
                    TextView.BufferType.SPANNABLE)
        } catch (ex: IndexOutOfBoundsException) {
            // Do nothing
        }
    }

    private fun addClickablePartTextViewResizable(
            string: String,
            spannableText: String
    ): SpannableStringBuilder {
        val ssb = SpannableStringBuilder(string)
        if (string.contains(spannableText)) {
            ssb.setSpan(
                    ForegroundColorSpan(ContextCompat.getColor(mContext!!, R.color.colorAccent)),
                    string.indexOf(spannableText), string.indexOf(spannableText) + spannableText.length, 0)
        }
        return ssb
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        var heightMeasureSpec = heightMeasureSpec
        // if this TextView is collapsed and maxLines = 0,
        // than make its height equals to zero
        if (this.maxLines == 0 && !this.isExpanded && !this.animating) {
            heightMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.EXACTLY)
        }

        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
    }

    /**
     * Toggle the expanded state of this [ExpandableTextView].
     *
     * @return true if toggled, false otherwise.
     */
    fun toggle(): Boolean {
        return if (this.isExpanded)
            this.collapse()
        else
            this.expand()
    }

    /**
     * Expand this [ExpandableTextView].
     *
     * @return true if expanded, false otherwise.
     */
    fun expand(): Boolean {
        if (!this.isExpanded && !this.animating && this.maxLines >= 0) {
            // notify listener
            this.notifyOnExpand()

            // measure collapsed height
            this.measure(View.MeasureSpec.makeMeasureSpec(this.measuredWidth, View.MeasureSpec.EXACTLY),
                    View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED))

            this.collapsedHeight = this.measuredHeight

            // indicate that we are now animating
            this.animating = true

            // set maxLines to MAX Integer, so we can calculate the expanded height
            this.setMaxLines(Integer.MAX_VALUE)

            // measure expanded height
            this.measure(View.MeasureSpec.makeMeasureSpec(this.measuredWidth, View.MeasureSpec.EXACTLY),
                    View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED))

            val expandedHeight = this.measuredHeight

            // animate from collapsed height to expanded height
            val valueAnimator = ValueAnimator.ofInt(this.collapsedHeight, expandedHeight)
            valueAnimator.addUpdateListener { animation -> this@ExpandableTextView.height = animation.animatedValue as Int }

            // wait for the animation to end
            valueAnimator.addListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    // reset min & max height (previously set with setHeight() method)
                    this@ExpandableTextView.maxHeight = Integer.MAX_VALUE
                    this@ExpandableTextView.minHeight = 0

                    // if fully expanded, set height to WRAP_CONTENT, because when rotating the device
                    // the height calculated with this ValueAnimator isn't correct anymore
                    val layoutParams = this@ExpandableTextView.layoutParams
                    layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT
                    this@ExpandableTextView.layoutParams = layoutParams

                    // keep track of current status
                    this@ExpandableTextView.isExpanded = true
                    this@ExpandableTextView.animating = false
                }
            })

            // set interpolator
            valueAnimator.interpolator = this.expandInterpolator

            // start the animation
            valueAnimator
                    .setDuration(this.animationDuration)
                    .start()

            return true
        }
        return false
    }

    /**
     * Collapse this [TextView].
     *
     * @return true if collapsed, false otherwise.
     */
    fun collapse(): Boolean {
        if (this.isExpanded && !this.animating && this.maxLines >= 0) {
            // notify listener
            this.notifyOnCollapse()

            // measure expanded height
            val expandedHeight = this.measuredHeight

            // indicate that we are now animating
            this.animating = true

            // animate from expanded height to collapsed height
            val valueAnimator = ValueAnimator.ofInt(expandedHeight, this.collapsedHeight)
            valueAnimator.addUpdateListener { animation -> this@ExpandableTextView.height = animation.animatedValue as Int }

            // wait for the animation to end
            valueAnimator.addListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    // keep track of current status
                    this@ExpandableTextView.isExpanded = false
                    this@ExpandableTextView.animating = false

                    // set maxLines back to original value
                    this@ExpandableTextView.setMaxLines(this@ExpandableTextView.maxLines)

                    // if fully collapsed, set height back to WRAP_CONTENT, because when rotating the device
                    // the height previously calculated with this ValueAnimator isn't correct anymore
                    val layoutParams = this@ExpandableTextView.layoutParams
                    layoutParams.height = ViewGroup.LayoutParams.WRAP_CONTENT
                    this@ExpandableTextView.layoutParams = layoutParams
                }
            })

            // set interpolator
            valueAnimator.interpolator = this.collapseInterpolator

            // start the animation
            valueAnimator
                    .setDuration(this.animationDuration)
                    .start()

            return true
        }

        return false
    }

    // endregion

    // region public getters and setters

    /**
     * Sets the duration of the expand / collapse animation.
     *
     * @param animationDuration duration in milliseconds.
     */
    fun setAnimationDuration(animationDuration: Long) {
        this.animationDuration = animationDuration
    }

    /**
     * Adds a listener which receives updates about this [ExpandableTextView].
     *
     * @param onExpandListener the listener.
     */
    fun addOnExpandListener(onExpandListener: OnExpandListener) {
        this.onExpandListeners?.add(onExpandListener)
    }

    /**
     * Removes a listener which receives updates about this [ExpandableTextView].
     *
     * @param onExpandListener the listener.
     */
    fun removeOnExpandListener(onExpandListener: OnExpandListener) {
        this.onExpandListeners?.remove(onExpandListener)
    }

    /**
     * Sets a [TimeInterpolator] for expanding and collapsing.
     *
     * @param interpolator the interpolator
     */
    fun setInterpolator(interpolator: TimeInterpolator) {
        this.expandInterpolator = interpolator
        this.collapseInterpolator = interpolator
    }

    // endregion

    /**
     * This method will notify the listener about this view being expanded.
     */
    private fun notifyOnCollapse() {
        for (onExpandListener in this.onExpandListeners!!) {
            onExpandListener.onCollapse(this)
        }
    }

    /**
     * This method will notify the listener about this view being collapsed.
     */
    private fun notifyOnExpand() {
        for (onExpandListener in this.onExpandListeners!!) {
            onExpandListener.onExpand(this)
        }
    }

    // region public interfaces

    /**
     * Interface definition for a callback to be invoked when
     * a [ExpandableTextView] is expanded or collapsed.
     */
    interface OnExpandListener {
        /**
         * The [ExpandableTextView] is being expanded.
         *
         * @param view the textview
         */
        fun onExpand(view: ExpandableTextView)

        /**
         * The [ExpandableTextView] is being collapsed.
         *
         * @param view the textview
         */
        fun onCollapse(view: ExpandableTextView)
    }

    /**
     * Simple implementation of the [OnExpandListener] interface with stub
     * implementations of each method. Extend this if you do not intend to override
     * every method of [OnExpandListener].
     */
    class SimpleOnExpandListener : OnExpandListener {
        override fun onExpand(view: ExpandableTextView) {
            // empty implementation
        }

        override fun onCollapse(view: ExpandableTextView) {
            // empty implementation
        }
    }
}
