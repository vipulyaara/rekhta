@EpoxyDataBindingLayouts({R.layout.item_poet,
        R.layout.item_loader,
        R.layout.item_content,
        R.layout.item_poet_slider,
        R.layout.item_dictionary,
        R.layout.item_home_card,
        R.layout.item_audio,
        R.layout.item_content_nazm,
        R.layout.item_content_sher})
package org.rekhta.user.ui.epoxy;

import com.airbnb.epoxy.EpoxyDataBindingLayouts;

import org.rekhta.user.R;
